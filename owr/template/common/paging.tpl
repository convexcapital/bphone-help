{if $thePaging}
<div class="dataTables_paginate paging_simple_numbers">
    <ul class="pagination">
        <li class="{if $thePaging->getCurrPage()-1<=0}disabled{/if}"><a href="#" class="paging" data-page="{if $thePaging->getCurrPage()-1<=0}#{else}{$thePaging->getCurrPage()-1}{/if}">Previous</a></li>
    {if $thePaging->getLastPage()>7}
    	{if $thePaging->getCurrPage()>=$thePaging->getLastPage()}
    	<li class="hidden-xs"><a href="#" class="paging" data-page="{$thePaging->getCurrPage()-4}">{($thePaging->getCurrPage()-4)|number_format:0}</a></li>
    	{/if}
    	{if $thePaging->getCurrPage()+1>=$thePaging->getLastPage()}
    	<li class="hidden-xs"><a href="#" class="paging" data-page="{$thePaging->getCurrPage()-3}">{($thePaging->getCurrPage()-3)|number_format:0}</a></li>
    	{/if}
    	{if $thePaging->getCurrPage()-2>=1}
    	<li class="hidden-xs"><a href="#" class="paging" data-page="{$thePaging->getCurrPage()-2}">{($thePaging->getCurrPage()-2)|number_format:0}</a></li>
    	{/if}
    	{if $thePaging->getCurrPage()-1>=1}
    	<li class=""><a href="#" class="paging" data-page="{$thePaging->getCurrPage()-1}">{($thePaging->getCurrPage()-1)|number_format:0}</a></li>
    	{/if}

    	<li class="active"><a href="#" class="paging" data-page="{$thePaging->getCurrPage()}">{$thePaging->getCurrPage()|number_format:0}</a></li>

    	{if $thePaging->getCurrPage()+1<=$thePaging->getLastPage()}
    	<li class=""><a href="#" class="paging" data-page="{$thePaging->getCurrPage()+1}">{($thePaging->getCurrPage()+1)|number_format:0}</a></li>
    	{/if}
    	{if $thePaging->getCurrPage()+2<=$thePaging->getLastPage()}
    	<li class="hidden-xs"><a href="#" class="paging" data-page="{$thePaging->getCurrPage()+2}">{($thePaging->getCurrPage()+2)|number_format:0}</a></li>
    	{/if}
    	{if $thePaging->getCurrPage()-2<=0}
    	<li class="hidden-xs"><a href="#" class="paging" data-page="{$thePaging->getCurrPage()+3}">{($thePaging->getCurrPage()+3)|number_format:0}</a></li>
    	{/if}
    	{if $thePaging->getCurrPage()-1<=0}
    	<li class="hidden-xs"><a href="#" class="paging" data-page="{$thePaging->getCurrPage()+4}">{($thePaging->getCurrPage()+4)|number_format:0}</a></li>
    	{/if}
    {else}
    {assign var="foo" value="1"}
    {while $foo <= $thePaging->getLastPage()}
        <li class="{if $smarty.request.page==$foo}active{/if}{if $smarty.request.page=='' && $foo=='1'}active{/if}"><a href="#" class="paging" data-page="{$foo}">{$foo|number_format:0}</a></li>
    	{assign var="foo" value=$foo+1}
    {/while}
    {/if}
        <li class="{if $thePaging->getCurrPage()+1>$thePaging->getLastPage()}disabled{/if}"><a href="#" class="paging" data-page="{if $thePaging->getCurrPage()+1>$thePaging->getLastPage()}#{else}{$thePaging->getCurrPage()+1}{/if}">Next</a></li>
    </ul>
</div>
{/if}
