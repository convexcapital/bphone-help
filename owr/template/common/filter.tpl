<!-- filter -->
<div class="filter">

	<div class="filter-content">
		 <div class="filter-content-title">
			add/remove filters
			<a href="#" class="filter-close"><i class="fa fa-close"></i></a>
		 </div>

		 <form id="tableFilters"  method="post" autocomplete="off">
			 <input type="hidden" id="tablePage" name="page" value="1"/>
			 <input type="hidden" id="sortField" name="field" value="{$arrParams.sort.field}"/>
			 <input type="hidden" id="sortOrder" name="order" value="{$arrParams.sort.order}"/>
			 <input type="hidden" id="size" name="size" value="{$arrParams.page.size}"/>

			 <!-- filter-box -->
			 {if $blnDates}
			 <div class="filter-box">
				<div class="filter-box-heading expand" id="expand-3">
					 <div class="checkbox filter-checkbox">
						<label>
						  <input type="checkbox" name="blnDates" checked>
							<span></span>
						</label>
					 </div>
					 <span><strong>Dates: {$arrParams.user_date_range.start_date|date_format:$smarty.CONST.DISPLAY_DATE_1} - {$arrParams.user_date_range.end_date|date_format:$smarty.CONST.DISPLAY_DATE_1}</strong></span>
				</div>
				 <div class="filter-box-content dates" id="expandable-3">

					<div class="row">
						<div class="col-md-6">
							<input type="text" class="input-date form-control" placeholder="Start Date"  id="start_date" value="{$arrParams.user_date_range.start_date|date_format:$smarty.CONST.DISPLAY_DATE_1}" name="start_date">
						</div>
						<div class="col-md-6">
							<input type="text" class="input-date form-control" placeholder="End Date"  id="end_date"  value="{$arrParams.user_date_range.end_date|date_format:$smarty.CONST.DISPLAY_DATE_1}" name="end_date">
						</div>
					</div><!-- ./row -->

					<div class="form-group">
						<select id="quick_date" class="form-control custom-select" style="margin-bottom:200px;">
				        	<option selected="selected" value="-1">Quick Date</option>
				            <option value="today">Today</option>
				            <option value="yesterday">Yesterday</option>
				            <option value="this_week">This Week</option>
				            <option value="last_week">Last Week</option>
				            <option value="month_to_date">Month to Date</option>
				            <option value="last_month_to_date">Last Month to Date</option>
				            <option value="last_month">Last Month</option>
				         </select>
			        </div>

				 </div>
			 </div>
			 {/if}
			 <!-- filter-box -->

			 <!-- filter-box -->
			 {if $blnDomains && sizeof($arrDomains) > 1}
			 <div class="filter-box">
				<div class="filter-box-heading expand" id="expand-6">
					 <div class="checkbox filter-checkbox">
						<label>
						  <input type="checkbox" name="blnDates" checked>
							<span></span>
						</label>
					 </div>
					 <span><strong>Domain(s)</strong></span>
				</div>
				 <div class="filter-box-content" id="expandable-6">
					 <div class="panel-box">
						 <div class="panel-box-header">
							 <div class="input-group">
								 <input type="text" class="form-control" placeholder="Filter Domains">
								 <span class="input-group-btn">
									 <button class="btn btn-addon" type="button"><i class="fa fa-search"></i></button>
								 </span>
							 </div><!-- /input-group -->
						 </div>
						 <div class="panel-box-content">
							 <div class="height-scroll">
								 <table class="table-default">
									 <tbody>
										 {foreach $arrDomains as $arrDomain}
										 <tr>
											 <td>
												 <div class="checkbox custom-checkbox">
													 <label>
													   <input type="checkbox" name="domains[]" value="{$arrDomain['domain_uuid']}" {if $arrDomain['domain_uuid']|in_array:$arrParams['domains']}checked{/if} ><span>{$arrDomain['domain_name']}</span>
													 </label>
												 </div>
											 </td>
										 </tr>
										 {/foreach}
									 </tbody>
								 </table>
							 </div>
							 <!-- ./height-scroll -->
						 </div>

					 </div>
					 <!-- ./panelbox -->
				 </div>
			 </div>
			 {/if}
			 <!-- filter-box -->

			 <!-- filter-box -->
			 {if $blnLocations}
			 <div class="filter-box" id="locations">
				<div class="filter-box-heading expand" id="expand-5">
					 <div class="checkbox filter-checkbox">
						<label>
						  <input type="checkbox" name="blnLocations" {if $arrParams['locations']}checked{/if}>
							<span></span>
						</label>
					 </div>
					 <span><strong>Location(s)</strong></span>
				</div>
				<div class="filter-box-content" id="expandable-5">
					 <div class="panel-box">
						 <div class="panel-box-header">
							 <div class="input-group">
								 <input type="text" class="form-control search" placeholder="Filter Locations">
								 <span class="input-group-btn">
									 <button class="btn btn-addon" type="button"><i class="fa fa-search"></i></button>
								 </span>
							 </div><!-- /input-group -->
						 </div>
						 <div class="panel-box-content">
							 <div class="height-scroll">
								 <table class="table-default">
									 <tbody class="list">
										 {foreach $arrLocations as $objLocation}
										 <tr>
											 <td>
												 <div class="checkbox custom-checkbox">
													 <label>
													   <input type="checkbox" name="locations[]" value="{$objLocation->location_uuid}" {if $objLocation->location_uuid|in_array:$arrParams.locations}checked{/if}><span class="name">{$objLocation->number}</span>
													 </label>
												 </div>
											 </td>
										 </tr>
										 {/foreach}
									 </tbody>
								 </table>
							 </div>
							 <!-- ./height-scroll -->
						 </div>

					 </div>
					 <!-- ./panelbox -->
				 </div>
			 </div>
			 {/if}
			 <!-- filter-box -->

			 <!-- filter-box -->
			 {if $blnAgents}
			 <div class="filter-box" id="agents">
			    <div class="filter-box-heading expand" id="expand-0">
			 		<div class="checkbox filter-checkbox">
			 		   <label>
			 			 <input type="checkbox" name="blnAgents" {if $arrParams['extensions']}checked{/if}>
			 			   <span></span>
			 		   </label>
			 		</div>
			 		<span><strong>Agent(s)</strong></span>
			    </div>
			    <div class="filter-box-content" id="expandable-0">
			 		<div class="panel-box">
			 			<div class="panel-box-header">
			 				<div class="input-group">
			 					<input type="text" class="form-control search" placeholder="Filter Agents">
			 					<span class="input-group-btn">
			 						<button class="btn btn-addon" type="button"><i class="fa fa-search"></i></button>
			 					</span>
			 				</div><!-- /input-group -->
			 			</div>
			 			<div class="panel-box-content">
			 				<div class="height-scroll">
			 					<table class="table-default">
			 						<tbody class="list">
			 							{foreach $arrCCAgents as $key => $val}
			 							<tr>
			 								<td>
			 									<div class="checkbox custom-checkbox">
			 										<label>
			 										  <input type="checkbox" name="agents[]" value="{$key}" {if $key|in_array:$arrParams.cc_agent}checked{/if}><span class="name">{$val}</span>
			 										</label>
			 									</div>
			 								</td>
			 							</tr>
			 							{/foreach}
			 						</tbody>
			 					</table>
			 				</div>
			 				<!-- ./height-scroll -->
			 			</div>

			 		</div>
			 		<!-- ./panelbox -->
			 	</div>
			 </div>
			 {/if}
			 <!-- filter-box -->

			 <!-- filter-box -->
			 {if $blnSkills}
			 <div class="filter-box" id="skills">
				<div class="filter-box-heading expand" id="expand-1">
					 <div class="checkbox filter-checkbox">
						<label>
						  <input type="checkbox" name="blnSkills" {if $arrParams['cc_queue']}checked{/if}>
							<span></span>
						</label>
					 </div>
					 <span><strong>Skill(s)</strong></span>
				</div>
				 <div class="filter-box-content" id="expandable-1">
					 <div class="panel-box">
						 <div class="panel-box-header">
							 <div class="input-group">
								 <input type="text" class="form-control search" placeholder="Filter Skills">
								 <span class="input-group-btn">
									 <button class="btn btn-addon" type="button"><i class="fa fa-search"></i></button>
								 </span>
							 </div><!-- /input-group -->
						 </div>
						 <div class="panel-box-content">
							 <div class="height-scroll">
								 <table class="table-default">
									 <tbody class="list">
										 {foreach $arrSkills as $objSkill}
										 <tr>
											 <td>
												 <div class="checkbox custom-checkbox">
												 	<label>
													 	<input type="checkbox" name="skills[]" value="{$objSkill['id']}" {if $objSkill['id']|in_array:$arrParams['cc_queue']}checked{/if} ><span class="name">{$objSkill['name']}</span>
													</label>
												 </div>
											 </td>
										 </tr>
										 {/foreach}
									 </tbody>
								 </table>
							 </div>
							 <!-- ./height-scroll -->
						 </div>

					 </div>
					 <!-- ./panelbox -->
				 </div>
			 </div>
			 {/if}
			 <!-- filter-box -->


			 <!-- filter-box -->
			 {if $blnNumbers}
			 <div class="filter-box">
				<div class="filter-box-heading expand" id="expand-10">
					<div class="checkbox filter-checkbox">
					   <label>
						 <input type="checkbox" name="blnDirection" {if $arrParams['phone']['type']}checked{/if}>
						   <span></span>
					   </label>
					</div>
					<span><strong>Direction</strong></span>
				</div>
				<div class="filter-box-content" id="expandable-10">
				   <div class="form-group">
					   <select name="phone_type" class="form-control custom-select" >
						   <option value="">--- PLEASE SELECT ---</option>
					   		<option value="inbound" {if $arrParams['phone']['type'] == "inbound"}selected{/if}>Inbound</option>
							<option value="outbound" {if $arrParams['phone']['type'] == "outbound"}selected{/if}>Outbound</option>
					   </select>
				   </div>
				</div>
			 </div>


			 <div class="filter-box">
			    <div class="filter-box-heading expand" id="expand-4">
			 		<div class="checkbox filter-checkbox">
			 		   <label>
			 			 <input type="checkbox" name="blnNumber" {if $arrParams['phone']['number']}checked{/if}>
			 			   <span></span>
			 		   </label>
			 		</div>
			 		<span><strong>Number</strong></span>
			    </div>
			 	<div class="filter-box-content" id="expandable-4">
			 	   <div class="form-group">
			 		   <input type="text" class="form-control" name="phone_number" placeholder="(614) 378-6542" {if $arrParams.phone.number}value="{$arrParams.phone.number}"{/if}>
			 	   </div>
			 	</div>
			 </div>
			 {/if}
			 <!-- filter-box -->

			 <!-- filter-box -->
			 {if $blnAccountCode}
			 <div class="filter-box">
			    <div class="filter-box-heading expand" id="expand-7">
			 		<div class="checkbox filter-checkbox">
			 		   <label>
			 			 <input type="checkbox">
			 			   <span></span>
			 		   </label>
			 		</div>
			 		<span><strong>Account Code</strong></span>
			    </div>
			 	<div class="filter-box-content" id="expandable-7">
			 	   <div class="form-group">
			 		   <input type="text" class="form-control" name="accountcode" placeholder="" {if $arrParams.accountcode}value="{$arrParams.accountcode}"{/if}>
			 	   </div>
			 	</div>
			 </div>
			 {/if}
			 <!-- filter-box -->

			 <!-- filter-box -->
			 {if $blnCallGroup}
			 <div class="filter-box">
			    <div class="filter-box-heading expand" id="expand-8">
			 		<div class="checkbox filter-checkbox">
			 		   <label>
			 			 <input type="checkbox">
			 			   <span></span>
			 		   </label>
			 		</div>
			 		<span><strong>Call Group</strong></span>
			    </div>
			 	<div class="filter-box-content" id="expandable-8">
			 	   <div class="form-group">
			 		   <input type="text" class="form-control" name="call_group" placeholder="" value="{$arrParams['call_group']}">
			 	   </div>
			 	</div>
			 </div>
			 {/if}
			 <!-- filter-box -->

			 <!-- filter-box -->
			 {if $blnExtensions}
			 <div class="filter-box" id="agents">
			    <div class="filter-box-heading expand" id="expand-0">
			 		<div class="checkbox filter-checkbox">
			 		   <label>
			 			 <input type="checkbox" name="blnAgents" {if $arrParams['extensions']}checked{/if}>
			 			   <span></span>
			 		   </label>
			 		</div>
			 		<span><strong>Extension(s)</strong></span>
			    </div>
			    <div class="filter-box-content" id="expandable-0">
			 		<div class="panel-box">
			 			<div class="panel-box-header">
			 				<div class="input-group">
			 					<input type="text" class="form-control search" placeholder="Filter Extensions">
			 					<span class="input-group-btn">
			 						<button class="btn btn-addon" type="button"><i class="fa fa-search"></i></button>
			 					</span>
			 				</div><!-- /input-group -->
			 			</div>
			 			<div class="panel-box-content">
			 				<div class="height-scroll">
			 					<table class="table-default">
			 						<tbody class="list">
			 							{foreach $arrExtensions as $key => $arrVal}
			 							<tr>
			 								<td>
			 									<div class="checkbox custom-checkbox">
			 										<label>
			 										  <input type="checkbox" name="agents[]" value="{$key}" {if $key|in_array:$arrParams.extensions}checked{/if}><span class="name">{$arrVal['extension']}</span>
			 										</label>
			 									</div>
			 								</td>
			 							</tr>
			 							{/foreach}
			 						</tbody>
			 					</table>
			 				</div>
			 				<!-- ./height-scroll -->
			 			</div>

			 		</div>
			 		<!-- ./panelbox -->
			 	</div>
			 </div>
			 {/if}
			 <!-- filter-box -->

			 <!-- filter-action -->
			 <div class="filter-action">
				<button class="btn btn-green" id="close-filters">Apply Filters <i class="fa fa-plus"></i></button>
				<button class="btn btn-red" id="reset-filters" type="button">Reset All Filters</button>
			 </div>
			 <!-- ./filter-section -->

		</div>
			<!-- ./filter-content -->

		<div class="filter-back"></div>

	</form>

</div>
<!-- ./filter -->

<script>

	var options = {
		valueNames: ['name']
	};

	{if $arrLocations}
		new List('locations',options);
	{/if}

	{if $arrSkills}
		new List('skills',options);
	{/if}

	{if $arrCCAgents}
		new List('agents',options);
	{/if}

</script>
