<!-- This is a new comment for testing -->

<!DOCTYPE html>

<!-- This is a completely different comment -->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Business Phone Portal</title>

	<!-- Reset CSS -->
	<link href="/assets/themes/romel/css/reset.css" rel="stylesheet">

	<!-- Bootstrap -->
	<link href="/assets/themes/romel/css/bootstrap.min.css" rel="stylesheet">

	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

	<!-- font-awesome -->
	<link href="/assets/themes/romel/css/font-awesome/font-awesome.css" rel="stylesheet">

	<!-- style -->
	<link href="/assets/themes/romel/css/style.css" rel="stylesheet">
	<link href="/assets/themes/romel/css/filters.css" rel="stylesheet">
	<link href="/assets/themes/romel/css/style-override.css" rel="stylesheet">

	<!-- Sweet Alerts -->
	<link href="/assets/plugins/sweetalerts/css/sweetalert.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- List JS -->
	<script src="/assets/plugins/listjs/js/list.min.js"></script>

    {block name='css'}{/block}
</head>

<body>
	<!-- wrapper -->
	<div class="wrapper">

		<!-- sidebar -->
		<div class="sidebar">
			<div class="side-header">
				<a href="#"><img src="/assets/themes/romel/images/logo.png" alt="logo"></a>
				<!--  only show 800px below -->
				<span class="mobile-toggle" role="button"><i class="fa fa-navicon"></i></span>
			</div>

			<!--  only display in 320px below -->
			<div class="mobile-search">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search for...">
					<span class="input-group-btn">
						<button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
			</div>

			<ul class="sidebar-menu">
				<li class="title">navigation</li>
				<li class='{if $strNavigationPrimary == "dashboard"}active{/if}'>
					<a href="/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
				</li>
				<li class='{if $strNavigationPrimary == "numbers"}active{/if}'>
					<a href="/manage_numbers.php"><i class="fa fa-phone fa-fw"></i> Numbers</a>
				</li>
				<li class='{if $strNavigationPrimary == "locations"}active{/if}'>
					<a href="/manage_locations.php"><i class="fa fa-home fa-fw"></i> Locations</a>
				</li>
				<li class='{if $strNavigationPrimary == "tickets"}active{/if}'>
					<a href="/manage_tickets.php"><i class="fa fa-newspaper-o fa-fw"></i> Tickets</a>
				</li>
				<li class="has-sub {if $strNavigationPrimary == 'call_center'}active{/if}">
					<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Call Center<span class="fa arrow"></span></a>
					<ul class="sub-menu">
						<li class="{if $strNavigationSecondary == 'abandon_summary'}active{/if}">
							<a href="/report_cc_abandon.php">Abandon Summary</a>
						</li>
						<li class="{if $strNavigationSecondary == 'agent_overview'}active{/if}">
							<a href="/agent_overview.php">Agent Overview</a>
						</li>
						<li class="{if $strNavigationSecondary == 'agent_statistics'}active{/if}">
							<a href="/report_agent_statistics.php">Agent Statistics</a>
						</li>
						<li class="{if $strNavigationSecondary == 'cc_location_summary'}active{/if}">
							<a href="/report_cc_location_summary.php">Location Summary</a>
						</li>
						<li class="{if $strNavigationSecondary == 'production_report'}active{/if}">
							<a href="/report_production.php">Production Report</a>
						</li>
					</ul>
				</li>
				<li class="has-sub {if $strNavigationPrimary == 'reporting'}active{/if}">
					<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Reporting<span class="fa arrow"></span></a>
					<ul class="sub-menu">
						<li class="{if $strNavigationSecondary == 'call_detail_records'}active{/if}">
							<a href="/report_cdr_list.php">Call Detail Records</a>
						</li>
						<li class="{if $strNavigationSecondary == 'location_summary'}active{/if}">
							<a href="/report_cdr_summary.php">Location Summary</a>
						</li>
						<li class="{if $strNavigationSecondary == 'extension_statistics'}active{/if}">
							<a href="/report_extension_statistics.php">Extension Statistics</a>
						</li>
					</ul>
				</li>
				<li class="{if $strNavigationPrimary == 'users'}active{/if}">
					<a href="/manage_users.php"><i class="fa fa-group fa-fw"></i> Users</a>
				</li>
				{if Tag::exists('super', User::getTags())}
				<li class="has-sub {if $strNavigationPrimary == 'admin'}active{/if}">
					<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Admin<span class="fa arrow"></span></a>
					<ul class="sub-menu">
						<li class="{if $strNavigationSecondary == 'view_session'}active{/if}">
							<a href="/view_session.php">View Session</a>
						</li>
						<li class="{if $strNavigationSecondary == 'report_billing'}active{/if}">
							<a href="/report_billing.php">Billing - Seconds</a>
						</li>
						<li class="{if $strNavigationSecondary == 'report_billing_extensions'}active{/if}">
							<a href="/report_billing_extensions.php">Billing - Extensions</a>
						</li>
						</li>
						<li class="{if $strNavigationSecondary == 'locate_number'}active{/if}">
							<a href="/locate_number.php">Locate Number</a>
						</li>
					</ul>
				</li>
				{/if}
				<li>
					<a href="/index.php?action=logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
				</li>

			</ul>

		</div>
		<!-- ./sidebar -->

		<div class="fixed-header fixed-header-wrapped">
			<div class="top-header clearfix">

				<div class="row">
					<div class="col-sm-6 col-xs-11">
						<span class="sidebar-toggle" role="button"> <i class="fa fa-navicon"></i> </span>

						<!-- top-breadcrumbs -->
						<ul class="top-breadcrumbs">
							{foreach from=$arrPageTitle key=key item=val name=arrPageTitle}
								<li><a href="{$val}" >{$key}</a> </li>
							{/foreach}
						</ul>
					</div>
					<div class="col-sm-6 col-xs-1 text-right">
						<div class="row">
							<div class="col-xs-12 hidden-xs hidden-sm">
								<div class="btn-group dropdown-select">
				                	{if Tag::exists('super', User::getTags())}
				                    	{html_options name=tenant id=tenant options=Company::getAllSelect() selected=Company::getID() class="btn-select pointer form-control"}
					                {/if}
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>

			<div class="bottom-header">

					<div class="left-section">
						<button type="button" data-toggle="tooltip" data-placement="bottom" title="View my Profile" class="user-btn"><span class="user"><img src="/assets/themes/romel/images/user.png" alt=""></span></button>
						<!-- <button type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="New Notifications"><span class="user-notif"><a href="#"><i class="fa fa-bell"></i></a> <span class="notif">4</span> </span></button> -->
					</div>
					<div class="right-section">
						<button type="button" class="btn-icon" data-toggle="tooltip" data-placement="bottom" title="Support:						(614) 379-6850" class="user-btn"><i class="fa fa-question-circle" aria-hidden="true"></i></button>
						<button class="btn-icon create-ticket" data-toggle="tooltip" data-placement="bottom" title="Create Ticket"><i class="fa fa-file-text-o"></i></button>
						<span class="pushed-buttons"></span>
						{if $arrSave['blnDelete']}
							<button type="button" href="#" class="btn-icon btn-delete" data-toggle="tooltip" data-placement="bottom" title="{$arrSave['delete_name']}"><i class="fa fa-trash"></i></button>
						{/if}
						{if $arrSave['blnSave']}
							<button class="btn btn-green btn-save">{$arrSave['save_name']} <i class="fa fa-save"></i></button>
						{/if}
						{if $blnFilter}
							<button id="filter" class="btn btn-blue big">Add Filters<i class="fa fa-sliders"></i></button>
						{/if}
					</div>

			</div>

		</div>

        <div id="page-wrapper">
            {block name='body'}{/block}
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!-- jQuery -->
<script src="/assets/themes/romel/js/jquery-3.1.1.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/assets/themes/romel/js/bootstrap.min.js"></script>
<script src="/assets/themes/romel/js/tether.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/assets/themes/romel/js/bootstrap.min.js"></script>
<script src="/assets/themes/romel/js/bootstrap-datepicker.js"></script>
<script src="/assets/themes/romel/js/bootstrap-dropdown-checkbox.js"></script>

<script src="/assets/themes/romel/js/main.js"></script>

<!-- Sweet Alerts JS -->
<script src="/assets/plugins/sweetalerts/js/sweetalert.min.js"></script>

<script src="/assets/plugins/chartjs/Chart.min.js"></script>

{block name='scripts'}{/block}

{if Tag::exists('super', User::getTags())}
<script>
	$(document).ready(function() {
		$('#tenant').on('change', function(){
            var id = $(this).val();
			$.post("/ajax/assume_company.php", { id: id }, function(result){
				location.reload();
			});
        });

		$('.user-btn').click(function(){
			window.location = "/edit_user.php?id={$smarty.session.user->user_id}";
		});

		$('.create-ticket').click(function(){
			window.location = "/edit_ticket.php";
		});

		$('#reset-filters').click(function(){
			$("#start_date").val('{"last monday"|date_format:"%m/%d/%Y"}');
			$("#end_date").val('{"today"|date_format:"%m/%d/%Y"}');
		});
	});

	$(document).on('change', '#quick_date', function (){

		var val = $("#quick_date option:selected").val();

		switch (val) {
			case "today":
			  $("#start_date").val('{$smarty.now|date_format:"%m/%d/%Y"}');
			  $("#end_date").val('{$smarty.now|date_format:"%m/%d/%Y"}');
			  break;
			case "yesterday":
			  $("#start_date").val('{"yesterday"|date_format:"%m/%d/%Y"}');
			  $("#end_date").val('{"yesterday"|date_format:"%m/%d/%Y"}');
			  break;
			case "this_week":
			  $("#start_date").val('{"last monday"|date_format:"%m/%d/%Y"}');
			  $("#end_date").val('{"today"|date_format:"%m/%d/%Y"}');
			  break;
			case "last_week":
			  $("#start_date").val('{"monday last week"|date_format:"%m/%d/%Y"}');
			  $("#end_date").val('{"last sunday"|date_format:"%m/%d/%Y"}');
			  break;
			case "month_to_date":
			  $("#start_date").val('{"today"|date_format:"%m/01/%Y"}');
			  $("#end_date").val('{"today"|date_format:"%m/%d/%Y"}');
			  break;
			case "last_month_to_date":
			  var last_month = '{"last month"|date_format:"%m"}';
			  $("#start_date").val('{"last month"|date_format:"%m/01/%Y"}');
			  $("#end_date").val(last_month+'{"today"|date_format:"/%d/%Y"}');
			  break;
			case "last_month":
			  $("#start_date").val('{"last month"|date_format:"%m/01/%Y"}');
			  $("#end_date").val('{"last day of last month"|date_format:"%m/%d/%Y"}');
			  break;
		}
	});

</script>
{/if}

</body>

</html>
