<html>
<head>
	<title>New Voicemail</title>
	{literal}
	<style type="text/css">
		table td {border-collapse: collapse;}
		@media only screen and (max-width: 480px) {
            table[class=full-table] { width: 95% !important; min-width: 300px; }
            *[class=full] { width: 100% !important; }
            *[class=inside] { width: 90% !important; margin: 0 auto; }
            *[class=no-padding] { padding:0px !important; }
		    *[class=mobile-only] { display: block !important; max-height: none !important; }
            *[class=display-block] { display: block !important; width: 100% !important; }
            *[class=display-block-center] { display: block !important; width: 100% !important; text-align:center !important; }
            *[class=hide-mobile] { display: none !important; }
            span[class=auto-link] a { color: #ffffff !important; }
            a[href^=tel], a[href^=sms] { text-decoration: none; color: blue; cursor: default; }
		}
		a{
			color: white;
		}
	</style>
	{/literal}
</head>
<body style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<table cellpadding="0" cellspacing="0" border="0" style="margin:0; padding:0; width:100% !important; line-height: 100% !important;background:#363636;" bgcolor="#4d4d4d" align="center">
	<tr>
		<td valign="top" align="center" style="padding:0 20px;" class="no-padding">
            <table cellspacing="0" cellpadding="0" border="0" width="600" style="width:600px;margin:0 auto" class="full-table">
                <tr>
                    <td align="center" style="padding:30px 0 10px; color:#ffffff; font-weight:400; font-size:12px; mso-line-height-rule:exactly; line-height:14px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
						{block name="title"}{/block}
                    </td>
                </tr>
            </table>
		</td>
	</tr>

    <tr>
		<td valign="top" align="center">
            <table cellspacing="0" cellpadding="0" border="0" width="600" style="width:600px;margin:0 auto; background:#ffffff;" class="full-table">
                <tr>
                    <td align="center">
                        <table cellspacing="0" cellpadding="0" border="0" width="600" style="width:600px;margin:0 auto; background:#ffffff;" class="full">
                            <tr>
                                <td align="center">
                                    <img src="https://s3.amazonaws.com/engage-business-phone/images/img-hero-desktop.jpg" width="600" style="width:600px; display:block;" alt="We're upgrading your canvas" class="full"/>
                                </td>
                            </tr><!--HERO-->
                        </table>
                    </td>
                </tr>
				<tr>
                    <td align="center">
                        <table cellspacing="0" cellpadding="0" border="0" width="600" style="width:600px;margin:0 auto; background:#ffffff;" class="full">
                            <tr>
                                <td align="center">
                                    {block name="feature-img"}{/block}
                                </td>
                            </tr><!--HERO-->
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding:40px 0;">
                        {block name="body"}{/block}
                    </td>
                </tr>
            </table>
		</td>
	</tr>
    <tr>
		<td valign="top" align="center">
            <table cellspacing="0" cellpadding="0" border="0" width="600" style="width:600px;margin:0 auto; background:#c4253c;" class="full-table">
                <tr>
                    <td align="center">
                        <table cellspacing="0" cellpadding="0" border="0" width="520" style="width:520px;margin:0 auto;" class="inside">
                            <tr>
                                <td align="center" style="padding:20px 0; color:#ffffff; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:18px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
                                    <strong>Contact your Businessphone.com representative</strong> if you have received this message in error.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
		</td>
	</tr>
    <tr>
		<td valign="top" align="center">
            <table cellspacing="0" cellpadding="0" border="0" width="520" style="width:540px;margin:0 auto" class="full-table">
                <tr>
                    <td align="center">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="width:100%;margin:0 auto" class="inside">
                            <tr>
                                <td align="center" style="padding:30px 0 10px;color:#ffffff; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:21px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
                                    <a href="http://businessphone.com/#Contact%C2%A0Us" style="color:#ffffff;">Contact Us</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding:10px 0 40px;color:#ffffff; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:21px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
                                    This email was sent by:<br />
                                    Businessphone.com<br />
                                    <span class="auto-link">278 N. 5th St. <br />
                                    Columbus, OH 43215 </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr><!--Footer end-->
</table>


</body>
</html>
