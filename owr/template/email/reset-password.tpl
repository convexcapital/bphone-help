{extends file='email/common/email.tpl'}

{block name="body"}
<table cellspacing="0" cellpadding="0" border="0" width="520" style="width:520px;margin:0 auto; background:#ffffff;" class="inside">
	 <tr>
		<td align="center" style="padding-bottom:10px; color:#4d4d4d; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:18px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
		   <strong> PASSWORD RESET</strong>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding-bottom:0px; color:#4d4d4d; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:18px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
			<br>
			<span>Please click the following link to complete your password reset request.</span><br>
			<a style="color: #15c;" href="{$strServer}/reset_password.php?id={$strHash}">{$strServer}/reset_password.php?id={$strHash}</a><br>
		</td>
	</tr>
</table>
{/block}
