{extends file='email/common/email.tpl'}

{block name="title"}
	Welcome to <a href="#" style="color:#ffffff; text-decoration:none;"><font face="Helvetica, Arial, sans-serif" color="#ffffff">businessphone.com</font></a>
{/block}

{block name="feature-img"}
	<img src="https://s3.amazonaws.com/engage-business-phone/images/img-hero-welcome.jpg" width="600" style="width:600px; display:block;" alt="We're upgrading your canvas" class="full"/>
{/block}

{block name="body"}
<table cellspacing="0" cellpadding="0" border="0" width="600" style="width:600px;margin:0 auto; background:#ffffff;" class="full-table">
    <tr>
        <td align="center" style="padding:40px 0;">
            <table cellspacing="0" cellpadding="0" border="0" width="520" style="width:520px;margin:0 auto; background:#ffffff;" class="inside">
                 <tr>
                    <td align="center" style="padding-bottom:10px; color:#4d4d4d; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:18px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
                       <strong> WELCOME TO THE <a href="#" style="color:#4d4d4d; text-decoration:none;"><font face="Helvetica, Arial, sans-serif" color="#4d4d4d">BUSINESSPHONE.COM</font></a></strong>
                    </td>
                </tr>
                 <tr>
                    <td align="center" style="padding-bottom:10px; padding-top:0px; color:#4d4d4d; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:18px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
                       Please follow the link below to access your dashboard. Make sure to have your user name and password (shown below) available when you log in.
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-bottom:0px; color:#4d4d4d; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:18px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
                       <a href="{$strServer}" style="color:#c4253c">Log in to businessphone.com</a>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-bottom:0px; padding-top:10px; color:#4d4d4d; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:18px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
                       <strong> Username: </strong> <a href="#" style="color:#4d4d4d; text-decoration:none;"><font face="Helvetica, Arial, sans-serif" color="#4d4d4d">{$strUsername}</font></a>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-bottom:0px; padding-top:10px; color:#4d4d4d; font-weight:400; font-size:14px; mso-line-height-rule:exactly; line-height:18px; font-family:Helvetica, Arial, sans-serif; text-align:left;">
                       <strong> Password: </strong> {$strPassword}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
{/block}
