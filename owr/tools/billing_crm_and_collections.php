<?php

$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../docroot';
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'rds.database.inc.php';
require_once INCLUDES_DIR . 'fusion.database.inc.php';

global $objFusionConnRead, $objRDSConnRead;

$strStartDate = "2017-02-01 05:00:00";
$strEndDate = "2017-03-01 04:59:59";

// BILLING FOR CRM
$results = $objFusionConnRead->select('v_call_center_agents', 'agent_contact', ['agent_contact[~]' => '@ccfi.bphone.io']);

for($i=0; $i<sizeof($results); ++$i)
{
	if(strpos($results[$i], "user/"))
	{
		$intExt = strstr(substr($results[$i], strpos($results[$i], "user/")+5), "@", true);
	}
	else
	{
		$intExt = strstr(substr($results[$i], strpos($results[$i], "sofia/internal/")+15), "@", true);
	}

	if($intExt != 1968) $arrExtensions[] = $intExt;
}

// select extension_uuid from v_extensions where extension = /1234@ extension
$arrResults = $objFusionConnRead->select('v_extensions', 'extension_uuid', ['extension' => $arrExtensions]);

// use extension_uuid's to get billsec's from ES cdr's
$arrCRM = $objRDSConnRead->select(
	'v_xml_cdr',
	['uuid', 'start_stamp', 'billsec'],
	[
		"AND" => [
			'extension_uuid' => $arrResults,
			'billsec[>]' => 0,
			'start_epoch[<>]' => [strtotime($strStartDate), strtotime($strEndDate)]
		],
		"ORDER" => "start_epoch"
	]
);

echo "From " . $arrCRM[0]['start_stamp']." To ". $arrCRM[end(array_keys($arrCRM))]['start_stamp'] ."\n";
$intTotal = 0;
for($i=0; $i<sizeof($arrCRM); ++$i)
{
	$intSec = $arrCRM[$i]['billsec'];

	if($intSec < 30) 						$intTotal += 30;
	if($intSec > 30 && ($intSec % 6) == 0) 	$intTotal += $intSec;
	if($intSec > 30 && ($intSec % 6) != 0) 	$intTotal += $intSec + (6 - ($intSec % 6));
}

echo "CRM: " . $intTotal . "\n";


// BILLING FOR CCFI COLLECTIONS
$arrResults = $objRDSConnRead->select('v_xml_cdr',
	['uuid', 'start_stamp', 'billsec'],
	[
		"AND" => [
			'domain_uuid' => '7f8ee51c-5563-4e94-afe9-936ec13058fa',
			'billsec[>]' => 0,
			'start_epoch[<>]' => [strtotime($strStartDate), strtotime($strEndDate)]
		],
		"ORDER" => "start_epoch"
	]
);

echo "From " . $arrResults[0]['start_stamp']." To ". $arrResults[end(array_keys($arrResults))]['start_stamp'] ."\n";
$intTotal = 0;
for($i=0; $i<sizeof($arrResults); ++$i)
{

	$intSec = $arrResults[$i]['billsec'];

	if($intSec < 30) 						$intTotal += 30;
	if($intSec > 30 && ($intSec % 6) == 0) 	$intTotal += $intSec;
	if($intSec > 30 && ($intSec % 6) != 0) 	$intTotal += $intSec + (6 - ($intSec % 6));
}

echo "Collections: " . $intTotal . "\n";
exit();
