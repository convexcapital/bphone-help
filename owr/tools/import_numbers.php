<?php
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../docroot/';
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'number/number.class.php';
require_once CLASS_DIR . 'country/country.class.php';

require_once INCLUDES_DIR . 'fusion.database.inc.php';

$strCompanyID = $argv[1];
if(empty($strCompanyID)) die('CompanyID Required [argument 1]');

$_SESSION['company']->company_id = $strCompanyID;

$arrDomainUUIDs = $objConn->select('companies_domain_uuids', 'domain_uuid', ['AND'=>[
	'company_id'=>$strCompanyID
]]);

$arrResult = $objFusionConnRead->select("v_dialplans", '*', ['AND'=>[
	'domain_uuid'=>$arrDomainUUIDs,
	'dialplan_context'=>'public',
	'dialplan_number[!]'=>''
]]);

foreach($arrResult as &$val)
{
	$objNumber = new Number();
	$objNumber->description = $val['dialplan_description'];
	$objNumber->country_iso2 = 'us';
	$objNumber->number = $val['dialplan_number'];
	$objNumber->save();
	unset($objNumber);
}
$objNumber = new Number();
