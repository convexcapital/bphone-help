<?php

$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../docroot';
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'rds.database.inc.php';
require_once CLASS_DIR . 'database/elasticsearch.class.php';

global $objRDSConnRead;

$objQuery->size = 0;
$objQuery->aggs->by_date->date_histogram = [
	"field"     => "start_epoch",
	"interval"  => "month",
	"format"    => "MM/yyyy"
];

$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery, '_search', 'GET', false);

for($i=0; $i<sizeof($results->aggregations->by_date->buckets); ++$i)
{
	echo $results->aggregations->by_date->buckets[$i]->key_as_string;
	echo " =ES= ";
	echo $results->aggregations->by_date->buckets[$i]->doc_count;
	echo "\n";

	
}

exit();
