<?php
set_time_limit(0);
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../../docroot';
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once CRON_DIR . 'cron-util.php';

if(@$argv[1] != 'force' && !shouldIRun()) die();

require_once INCLUDES_DIR . "rds.database.inc.php";
require_once INCLUDES_DIR . "fusion.database.inc.php";
require_once CLASS_DIR . 'database/elasticsearch.class.php';
require_once CLASS_DIR . 'domain/domain.class.php';

// get last imported cdr start_epoch
$objLast->size = 5;
$objLast->sort->end_epoch->order = 'desc';
$results = ElasticSearch::query(ES_INDEX."/cdr", $objLast, '_search', 'GET', false);

// start from hour since last end_epoch
$intEndEpoch = $results->hits->hits[0]->_source->end_epoch - 7200;
// $intEndEpoch = 1479405150; // start from here

// setup new database object
global $objRDSConnRead, $objFusionConnRead, $objMem;

// get number of results
$intTotal			= $objRDSConnRead->count('v_xml_cdr', ['end_epoch[>=]' => $intEndEpoch]);
$intChunkSize 		= 5000;
$intTotalLoops 		= ceil($intTotal/$intChunkSize);
$objImportElastic 	= new ImportElastic($intTotal);


// loop through them ceil ( number of recordings / 10000)
echo "starting: ".$intTotal." total\n";
for($x=0; $x<$intTotalLoops; ++$x)
{
	// query subset
	$intOffset = $x*$intChunkSize;

	$arrResults = $objRDSConnRead->select(
		// table
		'v_xml_cdr',
		// select
		"*",
		// where
		[
			'end_epoch[>=]' => $intEndEpoch,
			'ORDER' => ['end_epoch' => 'ASC'],
			'LIMIT' => [$intOffset, $intChunkSize]
		]
	);

	for($i=0; $i<sizeof($arrResults); ++$i)
	{
		$objAction->create = [
			"_index" => "bphone",
			"_type"  => "cdr",
			"_id"    => $arrResults[$i]['uuid']
		];

		// calculate tta if answer_epoch
		$arrResults[$i]['tta'] = 0; // default to 0
		if($arrResults[$i]['answer_epoch'] > 0)
		{
			$arrResults[$i]['tta'] = $arrResults[$i]['answer_epoch'] - $arrResults[$i]['start_epoch'];
		}

		// get location_uuid from json variables
		$objJSON = json_decode($arrResults[$i]['json']);
		$arrResults[$i]['location_uuid'] = $objJSON->variables->location_uuid;

		// get domain_uuid if its blank
		if(empty($arrResults[$i]['domain_uuid']) && !empty($objJSON->variables->dialed_domain))
		{
			// set domain_uuid based on dialed_domain
			// !!! used for cc_agents if they do not pick up the call
			$arrResults[$i]['domain_uuid'] = Domain::getUUIDFromFusion($objJSON->variables->dialed_domain);
		}

		// outbound Call center calls do not have cc_* info
		// use extension_uuid to get the cc_agent@domain string
		// also check if domain_uuid is empty so the following if statement pulls the rest of the data
		if(empty($arrResults[$i]['cc_agent']) && !empty($arrResults[$i]['extension_uuid']))
		{
			// check memcache
			$strKey = sha1("getCCAgentString-{$arrResults[$i]['extension_uuid']}");
			$arrCCAgent = $objMem->get($strKey);

			// get from fusion and add to memcache
			if(empty($arrCCAgent))
			{
				$arrCCAgent = $objFusionConnRead->get(
					'v_call_center_agents',
					[
						'agent_name',
						'agent_contact',
						'domain_uuid'
					],[
					"AND" => [
						"extension_uuid" => $arrResults[$i]['extension_uuid']
					]
				]);
			}

			// set to memcache
			if($arrCCAgent) $objMem->set($strKey, $arrCCAgent, null, 60*5);

			// get domain
			preg_match("!sip_invite_domain=([^@]+)}!", $arrCCAgent['agent_contact'], $arrDomainMatches);

			// set cc_agent name@domain
			$arrResults[$i]['cc_agent'] = $arrCCAgent['agent_name'] . "@" . $arrDomainMatches[1];

			// make sure domain_uuid is not empty
			if(empty($arrResults[$i]['domain_uuid']))
			{
				$arrResults[$i]['domain_uuid'] = $arrCCAgent['domain_uuid'];
			}
		}

		// get extension_uuid if blank and cdr is from call center agent
		// add cc_extension for reporting based on fusionpbx.v_call_center_agents.agent_contact
		// !!! used for cc_agents if they do not pick up the call
		if(!empty($arrResults[$i]['cc_agent']) && !empty($arrResults[$i]['domain_uuid']))
		{
			// check memcache
			$strKey = sha1("getExtensionUUIDccagent-{$arrResults[$i]['cc_agent']}");
			$arrCCAgent = $objMem->get($strKey);

			// get from fusion and add to memcache
			if(empty($arrCCAgent))
			{
				$arrExplode = explode("@", $arrResults[$i]['cc_agent']);
				$arrCCAgent = $objFusionConnRead->get(
					'v_call_center_agents(a)',
					[
						"[>]v_call_center_tiers(t)" => "agent_name"
					],
					[
						'agent_contact',
						'extension_uuid',
						'queue_name'
					],[
					"AND" => [
						"a.agent_name" => $arrExplode[0],
						"a.domain_uuid" => $arrResults[$i]['domain_uuid']
					]
				]);
			}

			// set to memcache
			if($arrCCAgent) $objMem->set($strKey, $arrCCAgent, null, 60*5);

			// get cc_extension from agent_contact == $arrMatches[1]
			preg_match("!sofia/internal/([^@]+)@!", $arrCCAgent['agent_contact'], $arrMatches);

			// if extension_uuid is missing
			if(empty($arrResults[$i]['extension_uuid']))
			{
				$arrResults[$i]['extension_uuid'] = $arrCCAgent['extension_uuid'];
			}

			// if cc_queue is missing
			if(empty($arrResults[$i]['cc_queue']))
			{
				// get domain
				preg_match("!sip_invite_domain=([^@]+)}!", $arrCCAgent['agent_contact'], $arrDomainMatches);

				// set cc_queue name
				$arrResults[$i]['cc_queue'] = $arrCCAgent['queue_name'] . "@" . $arrDomainMatches[1];
			}

			// set cc_extension
			$arrResults[$i]['cc_extension'] = $arrMatches[1];
		}

		// if direction is null, check if cc_agent is set and make sure its set to inbound call
		if(empty($arrResults[$i]['direction']) && $arrResults[$i]['cc_agent']) $arrResults[$i]['direction'] = "inbound";

		// get location_uuid from json variables
		$arrResults[$i]['location_uuid'] = $objJSON->variables->location_uuid;

		// set call_group
		$arrResults[$i]['call_group'] = urldecode($objJSON->variables->call_group);

		// get direction from json variables
		if(!empty($objJSON->variables->call_direction)) $arrResults[$i]['direction'] = $objJSON->variables->call_direction;

		// get other json variables
		$arrResults[$i]['cc_cause'] 		= $objJSON->variables->cc_cause;
		$arrResults[$i]['cc_cancel_reason'] = $objJSON->variables->cc_cancel_reason;

		// unset json variables
		unset($arrResults[$i]['json']);

		// unset last_app/Last_arg
		if(isset($arrResults[$i]['last_app'])) unset($arrResults[$i]['last_app']);
		if(isset($arrResults[$i]['last_arg'])) unset($arrResults[$i]['last_arg']);

		// check if the cdr has any tags
		$arrResults[$i]['acl_tags'] = $objConnRead->select(
			'cdr_tagged_numbers',
			[
				"[>]cdr_tags" => ["cdr_tag_uuid" => "uuid"]
			],
			'name',
			[
				"number" => [$arrResults[$i]['caller_id_number'], $arrResults[$i]['destination_number']]
			]
		);

		$objImportElastic->strBulk .= json_encode($objAction)."\n";
		$objImportElastic->strBulk .= json_encode($arrResults[$i])."\n";
		++$objImportElastic->intDone;
	}

	// send api calls
	$objImportElastic->send();
}

class ImportElastic
{
	public function __construct($intTotal)
	{
		$this->intDone 				= 0;
		$this->intTotal 			= $intTotal;
		$this->strBulk 				= "";
		$this->strScorecardsBulk 	= "";
	}

	public function send()
	{
		echo "curl call for parents  - ";
		$ch = curl_init();
		$strURL = "http://192.168.21.100/_bulk";

		curl_setopt($ch, CURLOPT_URL, $strURL);
		curl_setopt($ch, CURLOPT_PORT, 9200);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->strBulk);

		$result = curl_exec($ch);
		curl_close($ch);

		$result = json_decode($result);

		// error reporting
		if(!empty($result->errors))
		{
			for($y=0; $y<sizeof($result->items); ++$y)
			{
				if($result->items[$y]->create->status != 201)
				{
					echo "Failed: " . $result->items[$y]->create->error->reason . "\n";
				}
			}
		}
		// unset($result->items);
		// print_r($result);

		echo "done - sleeping (1) seconds\n"; sleep(1);

		// reset data variables we send to ES
		$this->strBulk 				= "";
		$this->strScorecardsBulk 	= "";

		// echo progress
		echo $this->intDone . " of " . $this->intTotal."\n";
	}
}
