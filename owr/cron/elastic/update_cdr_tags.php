<?php
set_time_limit(0);
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../../docroot';
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once CLASS_DIR . 'database/elasticsearch.class.php';

// setup new database object
global $objConnRead;

$objQuery->size = 10000;
$objQuery->fields = ["caller_id_number", "destination_number", "tags"];

// add the restriction query to the main query and return
$arrNumbers = $objConnRead->select("cdr_tagged_numbers", "number");
$arrShould[]->terms->caller_id_number = $arrNumbers;
$arrShould[]->terms->destination_number = $arrNumbers;
$objQuery->query->bool->must[]->bool->should = $arrShould;

$objResults = ElasticSearch::query(ES_INDEX."/cdr", $objQuery, "_search", "GET", false);

$intLoop = $objResults->hits->total;
if($objResults->hits->total >= 10000) $intLoop = 10000;
echo "Total: {$objResults->hits->total} -- processing {$intLoop}\n";

for($i=0; $i<sizeof($objResults->hits->hits); ++$i)
{
	$objDoc = $objResults->hits->hits[$i];

	// check if the cdr has any tags
	$arrCDRTags = $objConnRead->select(
		'cdr_tagged_numbers',
		[
			"[>]cdr_tags" => ["cdr_tag_uuid" => "uuid"]
		],
		'name',
		[
			"number" => array_merge($objDoc->fields->caller_id_number, $objDoc->fields->destination_number)
		]
	);

	// no tags for some reason
	if(empty($arrCDRTags)) continue;

	// check to see if the tags have not changed
	if(sort($objDoc->fields->acl_tags) == sort($arrCDRTags)) continue;

	// if doc already has cdr tags, merge and update
	if(!empty($objDoc->fields->acl_tags)) $arrCDRTags = array_merge($arrCDRTags, $objDoc->fields->acl_tags);

	$objUpdate->doc->acl_tags = $arrCDRTags;
	ElasticSearch::query(ES_INDEX."/cdr/{$objDoc->_id}", $objUpdate, "_update", "POST");

	echo "{$objDoc->_id}\n";
}

echo "Done\n";
