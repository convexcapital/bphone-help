<?php
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../docroot';
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once CRON_DIR . 'cron-util.php';

if(@$argv[1] != 'force' && !shouldIRun()) die();

require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'database.inc.php';
require_once CLASS_DIR . 'util/util.class.php';
require_once CLASS_DIR . 'company/company.class.php';
require_once CLASS_DIR . 'location/location.class.php';

global $blnDebug;
$blnDebug = true;

set_time_limit(43200);

$arrCompanies = Company::getAll();

for($x = 0; $x < sizeof($arrCompanies); ++$x)
{
    $objCompany = $arrCompanies[$x];

    $arrDomainUUIDs = Company::getAllDomainUUIDs($objCompany->company_id);

    if(empty($arrDomainUUIDs)) continue;

    for($y = 0; $y < sizeof($arrDomainUUIDs); ++$y)
    {
        $arrPost = [
            'domain_uuid'   =>  $arrDomainUUIDs[$y],
        ];

        $arrResponse = util::sendRequest('https://iqventures.bphone.io/app/locations/location_api.php', $arrPost);
        $arrLocations = json_decode($arrResponse['response']);

        if(empty($arrLocations)) continue;

        for($z = 0; $z < sizeof($arrLocations); ++$z)
        {
            $objLocation = $arrLocations[$z];

            $arrData = [
                'location_uuid'     =>  $objLocation->location_uuid,
                'company_id'        =>  $objCompany->company_id,
                'number'            =>  $objLocation->location_name,
                'address1'          =>  $objLocation->location_street,
                'address2'          =>  $objLocation->location_street_ext,
                'city'              =>  $objLocation->location_city,
                'state'             =>  $objLocation->location_state,
                'zip'               =>  $objLocation->location_postal_code,
                'phone1'            =>  $objLocation->default_emergency,
                'isp_name'          =>  $objLocation->provider_name,
                'isp_type'          =>  $objLocation->provider_type,
                'isp_phone1'        =>  $objLocation->provider_phone_number,
                'mb_down'           =>  $objLocation->location_speed_down,
                'mb_up'             =>  $objLocation->location_speed_up,
            ];

            $objExistingLocation = Location::getOneByUUID($objLocation->location_uuid);

            Location::save($objExistingLocation->location_id, $arrData);

            $arrEnabledLocations[] = $objLocation->location_uuid;
        }
    }

    Location::deleteNotInArray($objCompany->company_id, $arrEnabledLocations);
}

die('done');
