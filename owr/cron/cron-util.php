<?php
global $argv;
function shouldIRun($strInstanceID = '', $strCronGroup = 'cron-main')
{
	$strHostname = exec('hostname');
	// updated prod-web-01.... removed engagertc.net
	// removed dev-web-01.... no cron should be allowed to run from dev
	// removed engage-ftp-01.engagertc.net as per steven
	$arrAllowed = array(
		'prod-web-01',
		'prod-cron-01',
		'engage-ftp-02.engagertc.net'
	);

	if( ! in_array(strtolower($strHostname), $arrAllowed) ) return false;

	//http://programster.blogspot.com/2013/11/php-using-file-locks-to-prevent.html
	//must make global so php doesnt clean up until script is done exec
	global $globals;
	$strProcess = basename($_SERVER['argv'][0]);
    $LOCK_FILE = dirname(__FILE__) . "/{$strProcess}.txt";
    # This creates the file if it doesnt exist.
	$globals['lock_file'] = fopen($LOCK_FILE, 'w');
    $result = flock($globals['lock_file'], LOCK_NB | LOCK_EX); # LOCK_EX
	if(!$result) return false;

	return true;
}

if(extension_loaded('newrelic')){
	newrelic_set_appname("PROD - CRON - BPhone Portal");
}
