<?php

class Skill
{
	function __construct(){}

		public static function getAll($strCompanyID)
		{
			if (empty($strCompanyID)) {
	            $strCompanyID = Company::getID();
	        }

			$objQuery = new stdClass();

			$objQuery->aggs->skills->terms = [
				"field" => "cc_queue",
				"size" => 100
			];

			$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);

			$arrSkills = [];

			foreach($results->aggregations->skills->buckets as $objSkills)
			{
				$arrSkills[] = [
					"id"	=> $objSkills->key,
					"name" 	=> explode("@",$objSkills->key)[0]
				];
			}

			return $arrSkills;
		}

		public static function getAllTagsInputJSON($strCompanyID)
		{
	        if (empty($strCompanyID)) {
	            $strCompanyID = Company::getID();
	        }

			$objQuery = new stdClass();

			$objQuery->aggs->skills->terms = [
				"field" => "cc_queue",
				"size" => 100
			];

			$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);

			$arrSkills = [];

			foreach($results->aggregations->skills->buckets as $objSkills)
			{
				$arrSkills[] = [
					"id"	=> $objSkills->key,
					"name" 	=> explode("@",$objSkills->key)[0]
				];
			}

	        return json_encode($arrSkills);
		}

		public static function getSelectedTagsInputJSON($arrSkills)
		{
			if(!$arrSkills) return;

			$arrSkills = explode(',',$arrSkills);
			$arrSkillsNew = [];

			foreach($arrSkills as $strSkill)
			{
				$arrSkillsNew[] = [
					"id"	=> $strSkill,
					"name" 	=> explode("@",$strSkill)[0]
				];
			}

			return json_encode($arrSkillsNew);
		}

}
