<?php
require_once INCLUDES_DIR . "fusion.database.inc.php";

class Domain
{
	public static function getUUIDFromFusion($strDomainName='')
	{
		if(empty($strDomainName)) return;

		global $objFusionConnRead, $objMem;

		// check memcache
		$strKey = sha1("getUUIDFromFusion-{$strDomainName}");
		$strDomainUUID = $objMem->get($strKey);

		// get from fusion and add to memcache
		if(empty($strDomainUUID))
		{
			$strDomainUUID = $objFusionConnRead->get('v_domains', 'domain_uuid', [ 'domain_name' => $strDomainName ]);
			$objMem->set($strKey, $strDomainUUID, null, 60*5);
		}

		return $strDomainUUID;
	}
}
