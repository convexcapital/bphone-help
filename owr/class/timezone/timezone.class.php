<?php
require_once CLASS_DIR . "company/company.class.php";
class timezone
{
	public static function getListAssoc($strSmartyKey = '')
	{
		global $smarty;
		$zones = array();
		$zones['Pacific/Honolulu']     = 'Hawaii';
		$zones['America/Anchorage']    = 'Alaska';
		$zones['America/Los_Angeles']  = 'Pacific';
		$zones['America/Denver']       = 'Mountain';
		$zones['America/Chicago']     = 'Central';
		$zones['America/New_York']     = 'Eastern';
		$zones['America/Puerto_Rico']  = 'Atlantic';
		if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $zones);
		return $zones;
	}

	public static function convert_to_user_date($date, $strUserTZ = false, $strFormat = 'Y-m-d H:i:s')
	{
		if($date == '0000-00-00 00:00:00') return $date;

		$strServerTZ = 'GMT';
		if($strUserTZ == 'user') $strUserTZ = Company::getTimeZone();
		if(!$strUserTZ) $strUserTZ = 'America/New_York';

		$dateTime = new DateTime ($date, new DateTimeZone($strServerTZ));
		$dateTime->setTimezone(new DateTimeZone($strUserTZ));
		return $dateTime->format($strFormat);
	}

	public static function convert_to_server_date($date, $strUserTZ = false, $strFormat = 'Y-m-d H:i:s')
	{
		$strServerTZ = 'GMT';
		if($strUserTZ == 'user') $strUserTZ = Company::getTimeZone();
		if(!$strUserTZ) $strUserTZ = 'America/New_York';

		$dateTime = new DateTime ($date, new DateTimeZone($strUserTZ));
		$dateTime->setTimezone(new DateTimeZone($strServerTZ));
		return $dateTime->format($strFormat);
	}

	public static function parseDateTime($arrData = array(), $strTimezone = '')
	{
		if(empty($arrData['Date'])) return array('gmt'=>'', 'local'=>'');

		if(!empty($arrData['Hour']) && !empty($arrData['Minute']) && !empty($arrData['Meridian']))
		{
			$strAppt = "{$arrData['Date']} {$arrData['Hour']}:{$arrData['Minute']} {$arrData['Meridian']}";
		}
		else
		{
			$strAppt = "{$arrData['Date']} 00:00:00";
		}
		$arrData['gmt'] = timezone::convert_to_server_date(date('Y-m-d H:i:s', strtotime($strAppt)), $strTimezone);
		$arrData['local'] = date('Y-m-d H:i:s', strtotime($strAppt));
		return $arrData;
	}

	public static function parseDate($arrData = array(), $strTimezone = '')
	{
		if(!empty($arrData['Day']) && !empty($arrData['Month']) && !empty($arrData['Year']))
		{
			return "{$arrData['Year']}-{$arrData['Month']}-{$arrData['Day']}";
		}
		return '';
	}
}
