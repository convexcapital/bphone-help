<?php
class BphoneS3
{
	public function connect($strID = '', $strKey = '')
	{
		if(empty($strID) || empty($strKey))
		{
			$strID = AWS_CRED_ID;
			$strKey = AWS_CRED_KEY;
		}

		$this->client = new Aws\S3\S3Client([
		    'version'     => 'latest',
		    'region'      => 'us-east-1',
		    'credentials' => [
		        'key'    => $strID,
		        'secret' => $strKey,
		    ],
		]);
	}

	public function streamAudio($strAudioPath="")
	{
		if( ! $this->doesObjectExist("", "fusion-recordings".$strAudioPath)) return "false";
		$strBucket = AWS_CRED_BUCKET;

		// The Amazon S3 stream wrapper allows you to store and retrieve data from
		// Amazon S3 using built-in PHP functions like
		// file_get_contents, fopen, copy, rename, unlink, mkdir, rmdir, etc.
		$this->client->registerStreamWrapper();

		// Open a stream in read-only mode
		if (!($stream = fopen("s3://{$strBucket}/fusion-recordings".$strAudioPath, 'r'))) {
		    die('Could not open stream for reading');
		}

		// retrieve metadata from object without returning the object itself
		$arrHeaders = $this->client->headObject([
			"Bucket" => $strBucket,
			"Key" => "fusion-recordings".$strAudioPath
		])->toArray();

		// set header
		header("Content-Type: {$arrHeaders['ContentType']}");

		// Check if the stream has more data to read
		while (!feof($stream)) {
		    echo fread($stream, $arrHeaders['ContentLength']);
		}

		// Be sure to close the stream resource when you're done with it
		fclose($stream);
	}

	public function doesObjectExist($strBucket = '', $strName = '')
	{
		if(!is_object($this->client)) $this->connect();
		if(empty($strBucket)) $strBucket = AWS_CRED_BUCKET;

		return $this->client->doesObjectExist($strBucket, $strName);
	}
}
