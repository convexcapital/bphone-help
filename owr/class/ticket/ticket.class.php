<?php
require_once INCLUDES_DIR . "ost.database.inc.php";
require_once CLASS_DIR.'company/company.class.php';
require_once CLASS_DIR.'user/user.class.php';
require_once CLASS_DIR.'util/util.class.php';
require_once CLASS_DIR.'timezone/timezone.class.php';

class Ticket{

    public static function getCount($strCompanyID = '', $strSmartyKey = '')
    {
        global $smarty;
        global $objMem;

        $strCompanyID = self::getCompanyID();

        $strKey = sha1("ticket-count-{$strCompanyID}");
        $objMem->delete($strKey);
        $intCount = $objMem->get($strKey);

        if(empty($intCount))
        {
            $arrData = self::getAll('', $strCompanyID);
            $objMem->set($strKey, count($arrData), null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, count($arrData));

        return count($arrData);
    }

    public static function getAll($arrPost = '', $strSmartyKey = '')
    {
        global $smarty;
        global $objOSTConnRead;
        global $objMem;

        $strOSTCompanyID = self::getCompanyID();

        $strKey = sha1("tickets-{$strOSTCompanyID}");
        $objMem->delete($strKey);
		$arrData = $objMem->get($strKey);

        if(empty($arrData))
        {
            //base where
            $arrWhere = [
                'AND' => [
                    'u.org_id'      => $strOSTCompanyID,
                ],
                'GROUP' =>  't.ticket_id',
            ];

            //if locations are provided, use in query
            if($arrPost['locations'] != 'all' && !empty($arrPost['locations']))
            {
                $arrLocations = explode(',', $arrPost['locations']);

                for($i = 0; $i < sizeof($arrLocations); ++$i)
                {
                    $objLocation = Location::getOneByNumber($arrLocations[$i]);
                    $arrWhere['AND']['tl.location_id'][] = $objLocation->location_id;
                }
            }

            //if dates are provided, use in query
            if($arrPost['start_date'] && $arrPost['end_date'])
            {
                $strStartDate = timezone::convert_to_server_date("{$arrPost['start_date']} 00:00:00", Company::getTimeZone(), 'Y-m-d H:i:s');
                $strEndDate = timezone::convert_to_server_date("{$arrPost['end_date']} 23:59:59", Company::getTimeZone(), 'Y-m-d H:i:s');
                $arrWhere['AND']['t.created[<>]'] = [$strStartDate, $strEndDate];
            }

            //if status is provided, use in query
            if($arrPost['status'] && $arrPost['status'] != 'all')
            {
                $arrWhere['AND']['t.status_id'] = $arrPost['status'];
            }

            //if priority is provided, use in query
            if($arrPost['priority'] && $arrPost['priority'] != 'all')
            {
                $arrWhere['AND']['tc.priority'] = $arrPost['priority'];
            }


            //joins and fields
            $arrData = $objOSTConnRead->select('ticket(t)', [
                '[>]tickets_locations(tl)' => ['t.ticket_id' => 'ticket_id'],
                '[>]ticket__cdata(tc)' => ['t.ticket_id' => 'ticket_id'],
                '[>]user(u)' => ['t.user_id' => 'id'],
                '[>]user_email(ue)' => ['u.default_email_id' => 'id'],
            ], [
                't.ticket_id(id)',
                't.number',
                'ue.address',
                'u.name',
                't.user_id',
                't.created',
                't.lastupdate',
                't.closed',
                't.status_id',
                'tc.subject',
                'tc.priority',
            ], $arrWhere);

            for($i = 0; $i < sizeof($arrData); ++$i)
            {
                $arrData[$i] = (object)$arrData[$i];
            }

            $objMem->set($strKey, $arrData, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrData);

        return $arrData;
    }

    public static function getOne($strTicketNumber = '', $strSmartyKey = '')
    {
        if(empty($strTicketNumber)) return false;

        global $smarty;
        global $objOSTConnRead;
        global $objMem;

        $strCompanyID = self::getCompanyID();

        $strKey = sha1("ticket-{$strCompanyID}{$strTicketNumber}");
        $objMem->delete($strKey);
        $objData = $objMem->get($strKey);

        if(empty($objData->id))
        {
            $objData = (object)$objOSTConnRead->get('ticket(t)', [
                '[>]ticket__cdata(tc)' => ['t.ticket_id' => 'ticket_id'],
                '[>]user(u)' => ['t.user_id' => 'id'],
                '[>]user_email(ue)' => ['u.default_email_id' => 'id'],
            ], [
                't.ticket_id(id)',
                't.number',
                'ue.address',
                'u.name',
                't.user_id',
                't.created',
                't.lastupdate',
                't.closed',
                't.status_id',
                'tc.subject',
                'tc.priority',
            ], [
                'AND' => [
                    'u.org_id'      =>  $strCompanyID,
                    't.number'   =>  $strTicketNumber,
                ],
            ]);

            $objMem->set($strKey, $objData, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $objData);

        return $objData;
    }

    public static function getComments($strTicketID = '', $strSmartyKey = '')
    {
        if(empty($strTicketID)) return false;

        global $smarty;
        global $objOSTConnRead;
        global $objMem;

        $strCompanyID = self::getCompanyID();

        $strKey = sha1("ticket-comments-{$strCompanyID}{$strTicketID}");
        $objMem->delete($strKey);
        $arrData = $objMem->get($strKey);

        if(empty($arrData))
        {
            $arrData = $objOSTConnRead->select('thread(th)', [
                '[>]ticket(t)' => ['th.object_id' => 'ticket_id'],
                '[>]thread_entry(te)' => ['th.id' => 'thread_id'],
                '[>]user(u)' => ['t.user_id' => 'id'],
            ], [
                'te.user_id',
                'te.staff_id',
                'te.poster',
                'te.body',
                'te.ip_address',
                'te.created',
            ], [
                'AND' => [
                    'u.org_id'      =>  $strCompanyID,
                    't.number'      =>  $strTicketID,
                ],
            ]);

            for($i = 0; $i < sizeof($arrData); ++$i)
            {
                $arrData[$i] = (object)$arrData[$i];
                if($arrData[$i]->user_id)
                {
                    $arrData[$i]->icon = 'user';
                    $arrData[$i]->color = '';
                }elseif($arrData[$i]->staff_id)
                {
                    $arrData[$i]->icon = 'group';
                    $arrData[$i]->color = '';
                }
            }

            $objMem->set($strKey, $arrData, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrData);

        return $arrData;
    }

    public static function getEvents($strTicketID = '', $strSmartyKey = '')
    {
        if(empty($strTicketID)) return false;

        global $smarty;
        global $objOSTConnRead;
        global $objMem;

        $strKey = sha1("ticket-events-{$strTicketID}");
        $objMem->delete($strKey);
        $arrData = $objMem->get($strKey);

        if(empty($arrData))
        {
            $arrData = $objOSTConnRead->select('thread(th)', [
                '[>]ticket(t)' => ['th.object_id' => 'ticket_id'],
                '[>]thread_event(te)' => ['th.id' => 'thread_id'],
                '[>]staff(s)' => ['te.uid' => 'staff_id'],
                '[>]user(u)' => ['te.uid' => 'id'],
            ], [
                'te.staff_id',
                's.firstname',
                's.lastname',
                'u.name',
                'te.state',
                'te.data',
                'te.timestamp(created)',
            ], [
                'AND' => [
                    't.number'      =>  $strTicketID,
                ],
            ]);

            for($i = 0; $i < sizeof($arrData); ++$i)
            {
                $arrData[$i] = (object)$arrData[$i];

                $arrData[$i]->data = json_decode($arrData[$i]->data, 1);
                $arrData[$i]->poster = "{$arrData[$i]->firstname} {$arrData[$i]->lastname}";
                switch($arrData[$i]->state)
                {
                    case 'edited':
                        $arrData[$i]->icon = 'pencil';
                        $arrData[$i]->color = 'warning';
                    break;
                    case 'created':
                        $arrData[$i]->icon = 'check';
                        $arrData[$i]->color = 'success';
                    break;
                    case 'closed':
                        $arrData[$i]->icon = 'times';
                        $arrData[$i]->color = 'danger';
                    break;
                }
            }

            $objMem->set($strKey, $arrData, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrData);

        return $arrData;
    }

    public static function getEntriesAndEvents($strTicketID = '', $strSmartyKey = '')
    {
        if(empty($strTicketID)) return false;

        global $smarty;
        global $objMem;

        $strCompanyID = self::getCompanyID();

        $strKey = sha1("ticket-comments-events-{$strCompanyID}{$strTicketID}");
        $objMem->delete($strKey);
        $arrData = $objMem->get($strKey);

        if(empty($arrData))
        {
            $arrComments = self::getComments($strTicketID);
            $arrEvents = self::getEvents($strTicketID);

            $arrData = array_merge($arrComments, $arrEvents);

            function sortTimestamp($a = array(), $b = array())
            {
                if ($a->created == $b->created) {
                    return 0;
                }

                return ($a->created < $b->created) ? -1 : 1;
            }

            usort($arrData, "sortTimestamp");

            $objMem->set($strKey, $arrData, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrData);

        return $arrData;
    }

    public static function getID($strTicketNumber = '')
    {
        if(empty($strTicketNumber)) return false;

        global $objMem;
        global $objOSTConnRead;

        $strKey = sha1("ticket-id-{$strTicketNumber}");
        $objMem->delete($strKey);
        $objData = $objMem->get($strKey);

        if(empty($objData))
        {
            $objData = (object)$objOSTConnRead->get('ticket(t)', [
                't.ticket_id(id)',
            ], [
                'AND' => [
                    't.number'   =>  $strTicketNumber,
                ],
            ]);

            $objMem->set($strKey, $objData, null, 60*1);
        }

        return $objData->id;
    }

    public static function getThreadID($strTicketID = '')
    {
        if(empty($strTicketID)) return false;

        global $objMem;
        global $objOSTConnRead;

        $strKey = sha1("thread-id-for-ticket-{$strTicketID}");
        $objMem->delete($strKey);
        $objData = $objMem->get($strKey);

        if(empty($objData))
        {
            $objData = (object)$objOSTConnRead->get('thread(t)', [
                't.id',
            ], [
                'AND' => [
                    't.object_id'   =>  $strTicketID,
                    't.object_type' =>  'T',
                ],
            ]);

            $objMem->set($strKey, $objData, null, 60*1);
        }

        return $objData->id;
    }

    public static function getStatusByID($strID = '')
    {
        if(empty($strID)) return false;

        global $objMem;
        global $objOSTConnRead;

        $strKey = sha1("status-{$strID}");
        $objMem->delete($strKey);
        $objData = $objMem->get($strKey);

        if(empty($objData))
        {
            $objData = (object)$objOSTConnRead->get('ticket_status(ts)', [
                'ts.name',
            ], [
                'AND' => [
                    'ts.id'      =>  $strID,
                ],
            ]);

            $objMem->set($strKey, $objData, null, 60*1);
        }

        return $objData->name;
    }

    public static function getAllStatuses($strSmartyKey = '')
    {
        global $objMem;
        global $objOSTConnRead;
        global $smarty;

        $strCompanyID = Company::getID();

        $strKey = sha1("ticket-statuses-{$strCompanyID}");
        $objMem->delete($strKey);
        $arrData = $objMem->get($strKey);

        if(empty($arrData))
        {
            $arrData = $objOSTConnRead->select('ticket_status(ts)', [
                'ts.id',
                'ts.name',
            ], [
                'ts.state[!]'   =>  'deleted',
                'ORDER'         =>  'ts.sort',
            ]);

            for($i = 0; $i < sizeof($arrData); ++$i)
            {
                $arrData[$i] = (object)$arrData[$i];
            }

            $objMem->set($strKey, $arrData, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrData);

        return $arrData;
    }

    public static function getPriorityByID($strID = '')
    {
        if(empty($strID)) return false;

        global $objMem;
        global $objOSTConnRead;

        $strKey = sha1("priority-{$strID}");
        $objMem->delete($strKey);
        $objData = $objMem->get($strKey);

        if(empty($objData))
        {
            $objData = (object)$objOSTConnRead->get('ticket_priority(tp)', [
                'tp.priority_desc(name)',
            ], [
                'AND' => [
                    'tp.priority_id'      =>  $strID,
                ],
            ]);

            $objMem->set($strKey, $objData, null, 60*1);
        }

        return $objData->name;
    }

    public static function getAllPriorities($strSmartyKey = '')
    {
        global $objMem;
        global $objOSTConnRead;
        global $smarty;

        $strCompanyID = Company::getID();

        $strKey = sha1("ticket-priority-{$strCompanyID}");
        $objMem->delete($strKey);
        $arrData = $objMem->get($strKey);

        if(empty($arrData))
        {
            $arrData = $objOSTConnRead->select('ticket_priority(tp)', [
                'tp.priority_id(id)',
                'tp.priority_desc(name)',
            ], [
                'ORDER' =>  [
                    'tp.priority_urgency'   => 'DESC',
                ]
            ]);

            for($i = 0; $i < sizeof($arrData); ++$i)
            {
                $arrData[$i] = (object)$arrData[$i];
            }

            $objMem->set($strKey, $arrData, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrData);

        return $arrData;
    }

    public static function getFormFieldByID($strID = '')
    {
        if(empty($strID)) return false;

        global $objMem;
        global $objOSTConnRead;

        $strKey = sha1("form-field-{$strID}");
        $objMem->delete($strKey);
        $objData = $objMem->get($strKey);

        if(empty($objData))
        {
            $objData = (object)$objOSTConnRead->get('form_field(ff)', [
                'ff.label',
            ], [
                'AND' => [
                    'ff.id'      =>  $strID,
                ],
            ]);

            $objMem->set($strKey, $objData, null, 60*1);
        }

        return $objData->label;
    }

    public static function getAllHelpTopics($strSmartyKey = '')
    {
        global $objMem;
        global $objOSTConnRead;
        global $smarty;

        $strCompanyID = Company::getID();

        $strKey = sha1("help-topics-{$strCompanyID}");
        $objMem->delete($strKey);
        $arrData = $objMem->get($strKey);

        if(empty($arrData))
        {
            $arrData = $objOSTConnRead->select('help_topic(ht)', [
                'ht.topic_id(id)',
                'ht.topic',
            ], [
                'ht.isactive'   =>  '1',
                'ORDER'         =>  'ht.topic',
            ]);

            for($i = 0; $i < sizeof($arrData); ++$i)
            {
                $arrData[$i] = (object)$arrData[$i];
            }

            $objMem->set($strKey, $arrData, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrData);

        return $arrData;
    }

    public static function save($strTicketNumber = '', $arrData = array())
    {
        if(empty($arrData)) return false;

        global $objConn;
        global $objOSTConn;

        if(empty($strTicketNumber))
        {
            /*
            email: required Email address of the submitter
            name: required Name of the submitter
            subject: required Subject of the ticket
            message: required Initial message for the ticket thread
            alert: If unset, disable alerts to staff. Default is true
            autorespond: If unset, disable autoresponses. Default is true
            ip: IP address of the submitter
            phone: Phone number of the submitter
            phone_ext: Phone number extension -- can also be embedded in phone
            priorityId: Priority id for the new ticket to assume
            source: Source of the ticket, default is API
            topicId: Help topic id associated with the ticket
            attachments: An array of files to attach to the initial message. Each attachment must have some content and also the following fields:
                name: required name of the file to be attached. Multiple files with the same name are allowable
                type: Mime type of the file. Default is text/plain
                encoding: Set to base64 if content is base64 encoded
            */

            $arrPost = [
                'email'         =>  User::getEmail(),
                'name'          =>  User::getName(),
                'subject'       =>  Company::getName().': '.$arrData['subject'],
                'message'       =>  "{$arrData['comment']}<br><br><b>Affected Locations:</b> {$arrData['locations']}<br><b>Duration</b>: {$arrData['how_long']}",
                'alert'         =>  true,
                'autorespond'   =>  true,
                'ip'            =>  $_SERVER['REMOTE_ADDR'],
                'phone'         =>  User::getPhoneNumber(),
                'phone_ext'     =>  User::getPhoneExtension(),
                'priorityId'    =>  $arrData['priority'],
                'source'        =>  'API',
                'topicId'       =>  $arrData['category'],
            ];

            $arrHeaders = [
                'X-API-Key: 84FF359937190C78AC65F9C3EFF32B8C'
            ];

            $arrResponse = util::sendRequest('http://support.businessphone.com/api/tickets.json', json_encode($arrPost), 'POST', $arrHeaders);

            if($arrResponse['curl_errno'])
            {
                throw new exception($arrResponse['curl_error']);
            }

            $strTicketNumber = $arrResponse['response'];
            $strTicketID = self::getID($strTicketNumber);

            $arrLocations = explode(',', $arrData['locations']);

            for($i = 0; $i < sizeof($arrLocations); ++$i)
            {
                $strLocationNumber = $arrLocations[$i];
                $objLocation = Location::getOneByNumber($strLocationNumber);

                $arrPost = [
                    'ticket_id' => $strTicketID,
                    'location_id' => $objLocation->location_id,
                ];

                $objOSTConn->insert('tickets_locations', $arrPost);
            }

        }else{

            $strTicketID = self::getID($strTicketNumber);
            $strThreadID = self::getThreadID($strTicketID);

            if(!empty($arrData['comment']))
            {
                $arrPost = [
                    'pid'           => 0,
                    'thread_id'     => $strThreadID,
                    'staff_id'      => 0,
                    'user_id'       => self::getUserIDByEmail(User::getEmail()),
                    'type'          => 'M',
                    'flags'         => 1,
                    'poster'        => User::getName(),
                    'editor'        => NULL,
                    'editor_type'   => NULL,
                    'source'        => 'API',
                    'title'         => NULL,
                    'body'          => $arrData['comment'],
                    'format'        => 'html',
                    'ip_address'    => User::getIPAddress(),
                    'created'       => gmdate('Y-m-d H:i:s'),
                    'updated'       => '0000-00-00 00:00:00',
                    'body'          => $arrData['comment'],

                ];

                $objOSTConn->insert('thread_entry', $arrPost);
            }

            $objOSTConn->update('ticket', [
                'isanswered'    =>  0,
                'status_id'     =>  $arrData['status_id'],
            ], [
                'AND' => [
                    'ticket_id' => $strTicketID,
                ],
            ]);

            if(!empty($arrData['status_id']) && !empty($arrData['status_id_original']) && $arrData['status_id'] != $arrData['status_id_original'])
            {
                $arrData = [
                    'status'    =>  [
                        (int)$arrData['status_id'],
                        self::getStatusByID($arrData['status_id']),
                    ]
                ];

                $objOSTConn->insert('thread_event', [
                    'thread_id' =>  self::getThreadID($strTicketID),
                    'state'     =>  'edited',
                    'data'      =>  json_encode($arrData),
                    'username'  =>  User::getName(),
                    'timestamp' =>  gmdate('Y-m-d H:i:s'),
                    'uid'       =>  self::getUserID(),
                ]);
            }
        }

        return $strTicketNumber;
    }

    public static function getUserIDByEmail($strEmail = '')
    {
        global $objConn;
        global $objOSTConnRead;

        if(empty($strEmail)) return false;

        if(!empty(self::getUserID())) return self::getUserID();

        $objData = (object)$objOSTConnRead->get('user_email', [
            'user_id'
        ], [
			'AND' => [
		        'address' =>  $strEmail
		    ]
		]);

        $objConn->update('users', [
			'ost_id' =>  $objData->user_id
		], [
			'id'     =>	User::getID()
		]);

        return $objData->user_id;
    }

    public static function getCompanyIDByName($strCompanyName = '')
    {
        global $objConn;
        global $objOSTConnRead;

        if(empty($strCompanyName)) return false;

        if(!empty(self::getCompanyID())) return self::getCompanyID();

        $objData = (object)$objOSTConnRead->get('organization', [
            'id'
        ], [
			'AND' => [
		        'name' =>  $strCompanyName
		    ]
		]);

        $objConn->update('companies', [
			'ost_id' =>  $objData->id
		], [
			'id'     =>	Company::getID()
		]);

        return $objData->id;
    }

    public static function getCompanyID() { return $_SESSION['company']->ost_id; }
    public static function getUserID() { return $_SESSION['user']->ost_id; }
}
