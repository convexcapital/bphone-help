<?php

class Company
{
	function __construct(){}

	public static function getOne($strCompanyID = '', $strSmartyKey = '')
	{
		global $smarty;
		global $objConnRead;
		global $objMem;

		$strKey = sha1("company-{$strCompanyID}");
		$objMem->delete($strKey);
		$objData = $objMem->get($strKey);

		if(empty($objData->company_id))
		{
			/*
			OLD NON-MEDOO BASED
			if(!Tag::exists('super', $_SESSION['user']->tags))
			{
				for($i = 0; $i < sizeof($_SESSION['user']->tenants); ++$i)
				{
					$arrTenants[] = $objConnRead->quote($_SESSION['user']->tenants[$i]);
				}

				$sql_tenants = implode(',', $arrTenants);
				$sql_tenants = "AND c.company_id IN ({$sql_tenants})";
			}
			*/

			$objData = (object)$objConnRead->get('companies', '*', [
				'AND' => [
			        'company_id'    =>	$strCompanyID
			    ]
			]);

			$objData->domain_uuids = self::getAllDomainUUIDs($strCompanyID);

			$objMem->set($strKey, $objData, null, 60*5);
		}

		if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $objData);

		return $objData;
	}

	public static function getTag($strCompanyID = '')
    {
        global $objConnRead;

        $objData = (object)$objConnRead->get('companies', [
            'tag'
        ], [
            'AND' => [
                'company_id'	=>	$strCompanyID
            ]
        ]);

        return $objData->tag;
    }

	public static function getAll($strSmartyKey = '')
    {
        global $smarty;
        global $objConnRead;

        $arrData = $objConnRead->select('companies(c)', '*');

        for ($i = 0; $i < sizeof($arrData); ++$i) {
            $arrData[$i] = (object)$arrData[$i];
            $objData = $arrData[$i];
        }

        if (!empty($strSmartyKey) && is_object($smarty)) {
            $smarty->assign($strSmartyKey, $arrData);
        }

        return $arrData;
    }

	public static function getAllSelect($strSmartyKey = '')
    {
        global $smarty;
        global $objConnRead;
		global $objMem;

        $arrData = $objConnRead->select('companies(c)', '*',['ORDER' => ['c.name' => 'ASC']]);
		
		$strKey = sha1("company-select-{$strCompanyID}");
		$arrCompanies = $objMem->get($strKey);

		if(empty($arrCompanies))
		{
	        for ($i = 0; $i < sizeof($arrData); ++$i) {
	            $objData = (object)$arrData[$i];
				$arrCompanies[$objData->company_id] = $objData->name;
	        }

			$objMem->set($strKey, $arrCompanies, null, 60*5);
		}

        if (!empty($strSmartyKey) && is_object($smarty)) {
            $smarty->assign($strSmartyKey, $arrCompanies);
        }

        return $arrCompanies;
    }

	public static function getAllDomainUUIDs($strCompanyID = '', $strSmartyKey = '')
	{
		if(empty($strCompanyID)) return false;

        global $smarty;
        global $objConnRead;
        global $objMem;

		if(empty($strCompanyID))
		{
        	$strCompanyID = self::getID();
		}

        $strKey = sha1("company-domains-{$strCompanyID}");
        $objMem->delete($strKey);
        $arrDomainUUIDs = $objMem->get($strKey);

        if(empty($arrDomainUUIDs))
        {
            $arrData = $objConnRead->select('companies_domain_uuids(cdu)', [
                'cdu.domain_uuid'
            ], [
                'AND' => [
                    'cdu.company_id'      =>  $strCompanyID,
                ],
            ]);

            for($i = 0; $i < sizeof($arrData); ++$i)
            {
				$arrDomainUUIDs[] = $arrData[$i]['domain_uuid'];
            }

            $objMem->set($strKey, $arrData, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrDomainUUIDs);

        return $arrDomainUUIDs;
	}

	public static function getAllDomains($strCompanyID='')
	{
		global $smarty, $objConnRead, $objMem;

		// get company id if null
		if(empty($strCompanyID)) $strCompanyID = self::getID();

		// check Memcache
		$strKey = sha1("company-getalldomains-{$strCompanyID}");
        $arrDomains = $objMem->get($strKey);

		// get domains from db if memcache is empty
		if(empty($arrDomains)) $arrDomains = $objConnRead->select('companies_domain_uuids', '*', ['company_id' => $strCompanyID]);

		// set memcache
		$objMem->set($strKey, $arrDomains, null, 60*1);

        return $arrDomains;
	}

	/*
	public static function update($strCompanyID = '', $arrData = array())
	{
		global $smarty;
		global $objConn;
		global $objMem;

		try
		{
			$sql_company_id = $objConn->quote($strCompanyID);

			if(empty($strCompanyID))
			{
				$objConn->insert('companies', $arrData);
			}else{
				$objConn->update('companies', $arrData, "company_id = {$sql_company_id}");
			}

			if($objConn->errno)
			{
				throw new Exception($objConn->error);
			}

			$strKey = sha1("company-{$strCompanyID}");
			$objMem->delete($strKey);

			$smarty->assign('strSuccess', 'Company Updated!');
		}

		catch(Exception $e)
		{
			$smarty->assign('strError', $e->getMessage());
		}
	}
	*/

	public static function getID() { return $_SESSION['company']->company_id; }
	public static function getName() { return $_SESSION['company']->name; }
	public static function getTimeZone() { return $_SESSION['company']->time_zone; }
	public static function getDomainUUIDs() { return $_SESSION['company']->domain_uuids; }

	public static function getAllAccountCodeJSON($arrParams)
	{
    	if (empty($strCompanyID)) {
            $strCompanyID = self::getID();
        }

		$objQuery = new stdClass();

		$objQuery->aggs->accountcode->terms->field = "accountcode";

		$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);

		$arrAccountCodes = [];

		foreach($results->aggregations->accountcode->buckets as $objAccountCode)
		{
			$arrAccountCodes[] = [
				"id"	=> $objAccountCode->key,
				"name" 	=> $objAccountCode->key
			];
		}

        return json_encode($arrAccountCodes);
	}

	public static function getSelectedAccountCodeJSON($arrAccountCodes)
	{
		$arrAccountCodesNew = [];

		foreach($arrAccountCodes as $strAccountCodes)
		{
			$arrAccountCodesNew[] = [
				"id"	=> $strAccountCodes,
				"name" 	=> $strAccountCodes
			];
		}

		return json_encode($arrAccountCodesNew);
	}
}
