<?php
Class ElasticSearch {

    // the actual query that runs
    protected $objQuery;

    public static function query($strTable='', $objQuery=[], $strFunction='_search', $strMethod='GET', $blnRestrict=true)
    {
		// inject restrictions on bphone/cdr index queries
		if($strTable == "bphone/cdr" && $blnRestrict == true) $objQuery = self::addRestrictions($objQuery);

        $ch = curl_init();
        $strURL = "http://192.168.21.100/".$strTable."/".$strFunction;
        curl_setopt($ch, CURLOPT_URL, $strURL);
        curl_setopt($ch, CURLOPT_PORT, 9200);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $strMethod);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($objQuery));

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

	// query injection for restrictions on all queries
	private static function addRestrictions($objQuery=[])
	{
		global $objConnRead;
		$objParams = new Params();
		$arrUserTags = User::getTags();

		/*
		 * DOMAIN_UUID FILTER --- NEEDED TO LIMIT COMPANY
		 */
		$arrDomainUUIDs = $objParams->arrParams['domains'];
		for($i=0; $i<sizeof($arrDomainUUIDs); ++$i)
		{
			$arrShouldDomains[]->match->domain_uuid = $arrDomainUUIDs[$i];
		}
		$objQuery->query->bool->must[]->bool->should = $arrShouldDomains;

		// if super admin, no restrictions
		if(in_array("super", $arrUserTags)) return $objQuery;

		// pull cdr tags based on user tags
		$arrCDRTags = $objConnRead->select('cdr_tags', 'name', ['tag' => $arrUserTags]);

		// always display cdr's that do not have any tags
		$arrShould[]->term->acl_tags = "false";

		// add cdr_tags to query
		$arrShould[]->terms = [
			"acl_tags" => [$arrCDRTags]
		];

		// add the restriction query to the main query and return
		$objQuery->query->bool->must[]->bool->should = $arrShould;

		return $objQuery;
	}
}
