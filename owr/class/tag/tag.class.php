<?php

class Tag
{
	public static function getAllByUser($strUserID)
	{
		if(empty($strUserID)) return false;

		global $objConnRead;

		$arrData = $objConnRead->select('tags', [
			'tag'
		], [
		    'user_id'   =>	$strUserID
		]);

		for($i = 0; $i < sizeof($arrData); ++$i)
		{
			$objData = (object)$arrData[$i];

			$arrTags[] = $objData->tag;
		}

		return $arrTags;
	}

	public static function exists($strTag = '', $arrTags = array())
	{
		if(empty($strTag)) return false;

		$strTag = rtrim($strTag, '-');
		
		if(in_array($strTag, $arrTags)) return true;

		if(in_array('super', $arrTags)) return true;

		return false;
	}
}
