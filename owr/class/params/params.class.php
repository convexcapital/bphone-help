<?php
require_once INCLUDES_DIR . 'smarty.inc.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'user/user.class.php';

Class Params {

	public $arrTempParams = [];
	private $arrDefaults = [];

    public $arrParams = [
		'company'				=> "",		// integer 	- company_id

		'locations'				=> [],		// array 	- location uuid's

		'accountcodes'			=> [],		// array 	- account codes

		'phone' => [
			'type'				=> "",		// string 	- inbound/outbound/local
			'number'			=> "", 		// string 	- phone numbe or extension
		],

		'date_range' => [
			'start_date' 		=> "",		// string 	- datetime in UTC mm-dd-yyyy hh:ii:ss
			'end_date'	 		=> "",		// string 	- datetime in UTC mm-dd-yyyy hh:ii:ss
			'user_timezone' 	=> "",		// string 	- php timezone
		],

		'user_date_range' => [
			'start_date' 		=> "",		// string 	- converted back to user timezone
			'end_date'			=> "",		// string 	- converted back to user timezone
		],

		'page' => [
			'size' 				=> "",		// integer 	- result set size
			'from' 				=> "",		// integer 	- result set page
		],

		'sort' => [
			'field' 			=> "",		// string 	- column name to sort by
			'order' 			=> "",		// string 	- DESC or ASC
		],

		'extensions'			=> [],		// array 	- extension_uuid's

		'domains'				=> [], 		// array 	- domain_uuid's
		'call_group'			=> "",		// string 	- string based call_group
		'cc_queue'				=> [],		// array 	- array of skills (cc_queue)
		'cc_agent' 				=> [],		// array 	- array of agent@domain.com
	];

	private $arrParamSettings = [
		'company' 			=> ['persist'  => true, 	'setTemp' => false],
		'locations' 		=> ['persist'  => false, 	'setTemp' => true],
		'phone' 			=> ['persist'  => false, 	'setTemp' => true],
		'date_range' 		=> ['persist'  => true, 	'setTemp' => true],
		'user_date_range'	=> ['persist'  => true, 	'setTemp' => true],
		'page'				=> ['persist'  => false, 	'setTemp' => false],
		'sort'				=> ['persist'  => false, 	'setTemp' => true],
		'extensions'		=> ['persist'  => false, 	'setTemp' => true],
		'domains'			=> ['persist'  => false, 	'setTemp' => false],
		'call_group'		=> ['persist'  => false, 	'setTemp' => true],
		'cc_queue'			=> ['persist'  => false, 	'setTemp' => true],
		'cc_agent'			=> ['persist'  => false, 	'setTemp' => true],
	];

	public function __construct()
	{
		// RESET OBJECT
		if($_POST['resetParams']) unset($_SESSION['objParams']);

		// REINSTANTIATE OBJECT
		if($_SESSION['objParams'])
		{
			$objParams = unserialize($_SESSION['objParams']);
			$this->arrDefaults = $objParams->arrDefaults;
			$this->setParams($objParams->arrParams);
			$this->setTempParams($objParams->arrTempParams);
		}
		else
		{
		// CREATE OBJECT
			// do not create now if company_id is not defined
			if( ! Company::getID() ) return;

			// set default values
			$this->arrDefaults = [
				'date_range' => [
					'start_date' 		=> timezone::convert_to_server_date(gmdate("m/d/Y", strtotime("-7 days")) . ' 00:00:00', Company::getTimeZone()),
					'end_date'	 		=> timezone::convert_to_server_date(gmdate("m/d/Y", strtotime("-0 days")) . ' 23:59:59', Company::getTimeZone()),
					'user_timezone' 	=> Company::getTimeZone()
				],
				'page' => [
					'size' 				=> 50,
					'from' 				=> 1
				],
				'sort' => [
					'field' 			=> 'start_epoch',
					'order' 			=> 'desc'
				],
				'company' 				=> Company::getID(),
				'domains'				=> Company::getDomainUUIDs()
			];

			$this->setParams($this->arrDefaults);
		}
	}

    public function setParams($arrData=[])
	{
		if(!is_array($arrData)) return false;

		foreach($arrData as $key => $val)
		{
			// check if param is allowed
            if( ! array_key_exists($key, $this->arrParams) ) continue;

			// make sure a value exists as well
			// if($this->isEmpty($val)) continue;

			// set the param
			$this->arrParams[$key] = $val;

			// set the date_range back to user's timezone for the view
			if($key == 'date_range')
			{
				$this->setParams([
					'user_date_range' => [
						'start_date' 	=> timezone::convert_to_user_date($val['start_date'], $val['user_timezone']),
						'end_date'		=> timezone::convert_to_user_date($val['end_date'], $val['user_timezone'])
					]
				]);
			}
		}

		// trigger persistence
		$this->setToSession();
		$this->setSmartyVars();
	}

	public function setTempParams($arrData=[])
	{
		if(!is_array($arrData)) return false;

		foreach($arrData as $key => $val)
		{
			// check if param is allowed
            if( ! array_key_exists($key, $this->arrParams) ) continue;

			// make sure a value exists as well
			if($this->isEmpty($val)) continue;

			// make sure this param is allowed as a temp
			if(!$this->arrParamSettings[$key]['setTemp']) continue;

			// if temp key exists
			if($this->arrTempParams[$key])
			{
				// make sure value is not the same
				if($this->arrTempParams["id"][$key] == md5(serialize($val))) continue;
			}

			// set the param
			$this->arrTempParams[$key] = $val;
		}

		// trigger persistence
		$this->setToSession();
		$this->setSmartyVars();
	}

	public function getParam($strParam="")
	{
		// check if param is allowed
		if( ! array_key_exists($strParam, $this->arrParams) ) return false;

		// return Temp Param first if available
		if($this->arrTempParams[$strParam])
		{
			$param = $this->arrTempParams[$strParam];
		}
		else
		{
			$param = $this->arrParams[$strParam];
		}

		return $param;
	}

	public function getAllParams()
	{
		// array merge will overwite arrParams with temp params if available
		return array_merge($this->arrParams, $this->arrTempParams);
	}

	private function isEmpty($val)
	{
		if( is_null($val) || empty($val) ) return true;
		if(is_array($val))
		{
			foreach($val as $subkey => $subval)
			{
				if( is_null($subval) || empty($subval) ) return true;
			}
		}

		return false;
	}

	private function setToSession()
	{
		$objSession = new stdClass();

		// loop through params
		foreach($this->arrParams as $key => $val)
		{
			// if value is allowed to persist
			if($this->arrParamSettings[$key]['persist'])
			{
				$objSession->arrParams[$key] = $val;
			}
			// if its a default value
			else
			{
				if(array_key_exists($key, $this->arrDefaults)) $objSession->arrParams[$key] = $this->arrDefaults[$key];
			}
		}

		// loop through temp params
		foreach($this->arrTempParams as $key => $val)
		{
			// check if value is allowed to persist
			if(!$this->arrParamSettings[$key]['persist']) continue;

			// set param
			$objSession->arrTempParams[$key] = $val;
		}

		$objSession->arrDefaults = $this->arrDefaults;
		// set to session for persistence
		$_SESSION['objParams'] = serialize($objSession);
	}

	private function setSmartyVars()
	{
		global $smarty;
		$smarty->assign('arrParams', array_merge($this->arrParams, $this->arrTempParams), TRUE);
		$smarty->assign('arrTempParams', $this->arrTempParams, TRUE);
	}
}
