<?php
class Password
{
    /*
     * at least 1 uppercase character (A-Z)
     * at least 1 lowercase character (a-z)
     * at least 1 digit (0-9)
     * at least 1 special character (punctuation, spaces)
     * at least 8 characters
     * at most 128 characters
     * no more than 2 identical characters in a row (e.g. 111)
     *
     * temporary account lockout after 5 failed attempts (5 minutes)
     * after 10 failed attempts force reset password
     */

    public function __construct($arrPasswords=[], $arrHashes=[], $intCost=14)
    {
        // ---------- Config ------------
        $this->intMinLen      	= 8;
        $this->intMaxLen      	= 128;
        $this->intMaxHistory  	= 1;
        $this->intCost        	= $intCost;

		$this->blnLower 		= true;
		$this->blnUpper 		= true;
		$this->blnDigit 		= true;
		$this->blnSpecial 		= false;
		$this->blnRepeat		= true;
        // ------------------------------

        $this->strPassword    = $arrPasswords[0];
        $this->strPassword2   = $arrPasswords[1];
        $this->strHash        = $arrHashes[0];
        $this->arrHashes      = $arrHashes;
    }

    // verify complexity requirements and hash
    public function hash()
    {

        if($this->verifyRequirements())
        {
            return password_hash($this->strPassword, PASSWORD_DEFAULT, ['cost' => $this->intCost]);
        }

        return false;
    }

    // @TODO should at least check that passwords match
    public function hashNoValidation()
    {
        return password_hash($this->strPassword, PASSWORD_DEFAULT, ['cost' => $this->intCost]);
    }

    // update hash if crypto or cost changes
    public function rehash()
    {
        return password_hash($this->strPassword, PASSWORD_DEFAULT, ['cost' => $this->intCost]);
    }

    // check if hash needs updated due to crypto or cost change
    public function needsRehashed()
    {
        if(password_needs_rehash($this->strHash, PASSWORD_DEFAULT, ['cost' => $this->intCost]))
        {
            return true;
        }

        return false;
    }

    // match password against hash
    public function verifyHash()
    {
        return password_verify($this->strPassword, $this->strHash);
    }

    // complexity, runs before hash()
    private function verifyRequirements()
    {
        // make sure passwords match
        if($this->strPassword != $this->strPassword2) return false;
        // at least 8 characters and max of 128 characters
        if(strlen($this->strPassword) < $this->intMinLen || strlen($this->strPassword) > $this->intMaxLen) return false;

        // no more than 2 identical characters in a row (e.g. 111)
		if($blnRepeat)
		{
			if((preg_match('/(\S)\1{2,}/', $this->strPassword) === 1)) return false;
		}

		$strPregMatch  = "";
		if($this->blnLower) 	$strPregMatch 		.= "(?=.*[a-z])";
		if($this->blnUpper) 	$strPregMatch 		.= "(?=.*[A-Z])";
		if($this->blnDigit) 	$strPregMatch 		.= "(?=.*\d)";
		if($this->blnSpecial) 	$strPregMatch 		.= "(?=.*[_\W])";

        if(preg_match("/^" . $strPregMatch . ".+$/", $this->strPassword) !== 1) return false;

        // everything checks out
        return true;
    }

    // check password against history of passwords
    public function checkPasswordHistory()
    {
        foreach($this->arrHashes as $strHash)
        {
            //compare new password with history of passwords
            //if found, return true and require new password
            if(password_verify($this->strPassword, $strHash)) return false;
        }

		return true;
    }
}
