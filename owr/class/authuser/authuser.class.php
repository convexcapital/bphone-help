<?php
require_once CLASS_DIR . 'authuser/password.class.php';
require_once CLASS_DIR . 'phpmailer/phpmailer.class.php';

Class AuthUser
{

    /*
     * TODO : when a manager resets password, maybe set password_num to 0 and have 0 in password_history be the manager slot for any/all users
     * TODO : delete agents.password at some point
     * TODO : once agents.password is deleted, remove update to the col in resetPassword() as well as agents::signinAgent()
     * TODO : forceRest() should probably be broken out to 2 functions
     *
     * Password expiration policy
     * 60 days age
     * remember last 3 passwords
     *
     * Database Tables
     * -----------------------
     * users_forgot_passwords
     *      idforgot_password     = mediumint(8)
     *      process_date          = datetime
     *      agent_id              = mediumint(8)
     *      active                = tinyint(1)
     *      hash                  = varchar(10)
     *
     * users_password_history
     *      history_id            = mediumint(8)
     *      agent_id              = mediumint(8)
     *      password_num          = tinyint(1)
     *      password              = varchar(255)
     *      created_on            = date
     *      expires_on            = date
     *
     * users
	 *		auth_type			  = enum('standard', 'remote', 'etc')
     *      password_num          = tinyint(1)
     *      reset_password        = tinyint(1)
     *      last_login            = datetime
     *      failed_login_attempts = smallint(2)
     *      temp_lockout          = datetime
     */

    private $objAgent;

    public function __construct($strUsername='', $arrPasswords=[], $intAgentID='')
    {
        global $objConnRead;

        // ------------------------ Config --------------------------
        $this->intFailedAttempts    = 5;
        $this->strLockoutTime       = "+10 minutes";
        $this->strPWExpiration      = "+60 days";

        $this->strUsersTable        = "users";
        $this->strHistoryTable      = "users_password_history";
        $this->strForgotPWTable     = "users_forgot_passwords";
        // ----------------------------------------------------------

        $this->intAgentID           = $intAgentID;
        $this->strUsername          = $strUsername;
        $this->arrPasswords         = $arrPasswords;

        $this->objAgent             = null;
        $this->strActiveHash        = null;
        $this->strPasswordNum       = null;

		$this->strException 		= "";

        $this->checkIfExists();
    }

    public function checkIfExists()
    {
        global $objConnRead;

		// check with email
        $sql_where = ["email" => $this->strUsername, "auth_type" => "standard"];

		// if email is blank, try ID
        if(empty($this->strUsername)) $sql_where = ["user_id" => $this->intAgentID, "auth_type" => "standard"];

		$objAgent = (object) $objConnRead->get("users", "*", ["AND" => $sql_where]);

        // set some basic variables pertaining to the agent
        if(empty($this->objAgent))   $this->objAgent        = $objAgent;
        if( ! $this->intAgentID)     $this->intAgentID      = $this->objAgent->user_id;
        if( ! $this->strUsername)    $this->strUsername     = $this->objAgent->email;
        if( ! $this->strPasswordNum) $this->strPasswordNum  = $this->objAgent->password_num;

        return $objAgent;
    }

    public function validateAndReturnUser()
    {
        global $objConnRead, $objConn;

		$arrWhere = [
			"{$this->strUsersTable}.active" 		=> 1,
			"{$this->strUsersTable}.auth_type" 		=> "standard"
		];

		// default to agent_id for identifier but check if username is available
        if( ! empty($this->strUsername)) $arrWhere["{$this->strUsersTable}.email"] = $this->strUsername;
		else $arrWhere["{$this->strUsersTable}`.user_id"] = $this->intAgentID;

		$this->objAgent = (object) $objConnRead->get(
			// table
			"{$this->strUsersTable}",
			// join
			[
				"[>]{$this->strHistoryTable}" => ["{$this->strUsersTable}.user_id" => "agent_id", "{$this->strUsersTable}.password_num" => "password_num"]
			],
			// select
			[
				"{$this->strUsersTable}.user_id",
				"{$this->strUsersTable}.company_id",
				"{$this->strUsersTable}.first_name",
				"{$this->strUsersTable}.last_name",
				"{$this->strUsersTable}.email",
				"{$this->strUsersTable}.ost_id",
				"{$this->strUsersTable}.phone_extension",
				"{$this->strUsersTable}.pbx_user_uuid",
				"{$this->strUsersTable}.reset_password",
				"{$this->strUsersTable}.failed_login_attempts",
				"{$this->strUsersTable}.temp_lockout",
				"{$this->strHistoryTable}.password(password1)",
				"{$this->strHistoryTable}.created_on",
				"{$this->strHistoryTable}.expires_on",
			],
			// where
			[
				"AND" => $arrWhere
			]
		);

		// check if agent was found
        if( ! $this->objAgent->user_id )
		{
			$this->strException = "User was not found.";
			throw new Exception($this->strException);
		}

		// if no password hash, act as locked account and force forgot password
		if( ! $this->objAgent->password1 )
		{
			$this->strException = "This account has been locked, please use the forgot password tool.";
			throw new Exception($this->strException);
		}

        $objPassword = new Password($this->arrPasswords, [0 => $this->objAgent->password1]);

        // check for temp lockout
        if( ! $this->checkLockout()) throw new Exception($this->strException);

        // failed login attempt
        if( ! $objPassword->verifyHash())
        {
            if( ! $this->forceReset()) $this->setLockout();
			$intLoginAttempt = $this->objAgent->failed_login_attempts + 1;
			// return json here for error reporting
			$this->strException = "You have used " . $intLoginAttempt . " attempts out of " . $this->intFailedAttempts . ". Your account will be locked out after " . $this->intFailedAttempts . " attempts.";
			throw new Exception($this->strException);
        }

        // check if account is flagged for password reset by admin
        if($this->objAgent->reset_password)
		{
			$strHash = $this->getActiveHash();
			if(empty($strHash)) $strHash = $this->createForgotPWHash();
			die(header("Location: reset_password.php?id={$this->getActiveHash()}"));
		}

        // check password age
        if(gmdate('Y-m-d', strtotime('today')) > $this->objAgent->expires_on)
        {
            $this->forceReset(true, false);
            die(header("Location: reset_password.php?id={$this->getActiveHash()}"));
        }

        $objConn->update(
			$this->strUsersTable,
			[
	            'last_login'            => gmdate('Y-m-d H:i:s', strtotime('now')),
	            'failed_login_attempts' => '0',
	            'temp_lockout'          => '0000-00-00 00:00:00'
        	],
			["user_id" => $this->intAgentID]
		);

        // check if hash needs rehashed due to crypto default change or cost change
        if($objPassword->needsRehashed())
        {
            $objConn->update(
				$this->strHistoryTable,
				['password' => $objPassword->rehash()],
				["AND" => [
					"agent_id" => $this->intAgentID,
					"password_num" => $this->strPasswordNum]
				]
			);
        }

        return $this->objAgent;
    }

    public function setLockout()
    {
        global $objConn;

        // if max failed attempts, set account to temporary lockout time period
        if($this->objAgent->failed_login_attempts >= $this->intFailedAttempts-1)
        {

            // check if account was already had
            // a temp lockout, if so force a reset
            $this->forceReset();

            $arrInsert = [
                'failed_login_attempts' => 0,
                'temp_lockout'          => gmdate('Y-m-d H:i:s', strtotime($this->strLockoutTime, strtotime('now')))
            ];
            $objConn->update($this->strUsersTable, $arrInsert, ["user_id" => $this->intAgentID]);

            session_destroy();
            $_SESSION['strTempLockoutMsg'] = "Too many failed attempts, please try again in {$this->strLockoutTime}.";
            return true;
        }

		$objConn->update($this->strUsersTable, ["failed_login_attempts[+]" => 1], ["user_id" => $this->intAgentID]);
    }

    // check temp lockout time remaining
    public function checkLockout()
    {
        $intLockout = strtotime($this->objAgent->temp_lockout);
        $intNow     = strtotime(gmdate('Y-m-d H:i:s', strtotime('now')));

        if($intLockout <= $intNow) return true;

        $this->strException = gmdate("i:s", $intLockout-$intNow) . " - remaining time on temporary lockout.";
        session_destroy();
        return false;
    }

    public function forceReset($blnBypass=false, $blnEmail=true, $intNum=1)
    {
        global $objConn;

        // bypass with $blnBypass=true;
        if($this->objAgent->failed_login_attempts >= $this->intFailedAttempts-1 && $this->objAgent->temp_lockout != '0000-00-00 00:00:00' || $blnBypass === true)
        {
            $arrInsert = [
                'failed_login_attempts' => 0,
                'temp_lockout'          => '0000-00-00 00:00:00',
                'reset_password'        => $intNum
            ];
            $objConn->update($this->strUsersTable, $arrInsert, ["user_id" => $this->intAgentID]);

            $this->sendPasswordRequest($blnEmail);
            $_SESSION['strTempLockoutMsg'] = "Please check your email for a password reset request.";

            return true;
        }

        return false;
    }

    // @ TODO - when reset from admin panel, do not override password in password history table
    public function resetPassword($intID='', $blnCheckHistory=true)
    {
        global $objConn;

        $objPassword = new Password($this->arrPasswords, $this->getArrHashHistory());
        $strNewHash  = $objPassword->hash();

        if( ! $strNewHash) return false;
        if($blnCheckHistory && !$objPassword->checkPasswordHistory()) return false;

        // update/insert password
        if($this->objAgent->password_num >= $objPassword->intMaxHistory)
        {
            // max password history reached
            // reset password number to 1
			$intPasswordNum = 1;

            $arrUpdate = [
                'password'      => $strNewHash,
                'created_on'    => gmdate('Y-m-d', strtotime("Today")),
                'expires_on'    => gmdate('Y-m-d', strtotime($this->strPWExpiration))
            ];

            $objConn->update($this->strHistoryTable, $arrUpdate, ["AND" => ["agent_id" => $this->intAgentID, "password_num" => $intPasswordNum]]);
        }
        else
        {
            $intPasswordNum   = $this->objAgent->password_num + 1;

            $arrData = [
                'agent_id'      => $this->intAgentID,
                'password'      => $strNewHash,
                'password_num'  => $intPasswordNum,
                'created_on'    => gmdate('Y-m-d', strtotime("Today")),
                'expires_on'    => gmdate('Y-m-d', strtotime($this->strPWExpiration))
            ];

            //Insert or Update
            if(array_key_exists($intPasswordNum, $this->getArrHashHistory()))
            {
                $objConn->update($this->strHistoryTable, $arrData, ["AND" => ["agent_id" => $this->intAgentID, "password_num" => $intPasswordNum]]);
            }
            else
            {
                $objConn->insert($this->strHistoryTable, $arrData);
            }
        }

        // update password
        $objConn->update($this->strUsersTable, ['reset_password' => 0, 'password_num' => $intPasswordNum], ["user_id" => $this->intAgentID]);
        if($intID) $objConn->update($this->strForgotPWTable, ['active' => 0], ["forgot_password_id" => $intID]);

        return true;
    }

    public function setPasswordNoValidation($intResetPW=0)
    {
        global $objConn;

        // create a new hash without any validation of complexity
        $objPassword = new Password($this->arrPasswords);
        $strNewHash  = $objPassword->hashNoValidation(); // @TODO should check if passwords match at least

        if( ! $strNewHash) return false;
        if( ! $this->strPasswordNum) $this->strPasswordNum = 1;
		else $this->strPasswordNum += 1;

        //Insert or Update
        if(array_key_exists($this->strPasswordNum, $this->getArrHashHistory()))
        {
            $objConn->update($this->strHistoryTable, ['password' => $strNewHash], ["AND" => ["agent_id" => $this->intAgentID, "password_num" => $this->strPasswordNum]]);
        }
        else
        {
            $arrData = [
                'agent_id'      => $this->intAgentID,
                'password'      => $strNewHash,
                'password_num'  => $this->strPasswordNum,
                'created_on'    => gmdate('Y-m-d', strtotime("Today")),
                'expires_on'    => gmdate('Y-m-d', strtotime($this->strPWExpiration))
            ];
            $objConn->insert($this->strHistoryTable, $arrData);
        }

        $objConn->update($this->strUsersTable, ['reset_password' => $intResetPW, 'password_num' => $this->strPasswordNum], ["user_id" => $this->intAgentID]);
        $objConn->update($this->strForgotPWTable, ['active' => 0], ["hash" => $this->getActiveHash()]);

        return true;
    }

	public function createForgotPWHash()
	{
		global $objConn;

		$strHash = substr( md5( date('Y-m-d H:i:s') . $this->objAgent->user_id ), 0, 10);

        $intInsert = $objConn->insert($this->strForgotPWTable, [
            "process_date"  => gmdate('Y-m-d H:i:s', strtotime("+3 days", strtotime('now'))),
            "agent_id"      => $this->objAgent->user_id,
            "hash"          => $strHash,
            "active"        => 1
        ]);

        if( ! $intInsert ) return false;

		return $strHash;
	}

    public function sendPasswordRequest($blnEmail=true)
    {
		$strHash = $this->createForgotPWHash();

		if($blnEmail)
		{
			$phpMailer = new PHPMailerChild('Password Reset Request', [$this->objAgent->email]);
			$phpMailer->sendResetPassword($this->objAgent->email, $strHash, $_SERVER['HTTP_HOST'] );
		}

        return true;
    }

    // verify password reset request is valid
    public function verifyPasswordRequest($intHash='')
    {
        global $objConnRead;

		$objPasswordRequest = (object) $objConnRead->get(
			$this->strForgotPWTable,
			["[>]{$this->strUsersTable}" => ["agent_id" => "user_id"]],
			[
				"{$this->strUsersTable}.password_num",
				"{$this->strForgotPWTable}.forgot_password_id",
				"{$this->strForgotPWTable}.agent_id"
			],
			[
				"AND" => [
					"#{$this->strForgotPWTable}.process_date[>=]" => "TIMESTAMP(UTC_TIMESTAMP())",
					"{$this->strForgotPWTable}.active" => 1,
					"{$this->strForgotPWTable}.hash" => $intHash
				]
			]
		);

        $this->intAgentID      = $objPasswordRequest->agent_id;
        $this->strPasswordNum  = $objPasswordRequest->password_num;

        return $objPasswordRequest;
    }

    // get password history
    public function getArrHashHistory()
    {
        global $objConnRead;

        $arrHashes = [];

		$arrPasswordHistory = $objConnRead->select(
			$this->strHistoryTable,
			["password_num", "password"],
			["agent_id" => $this->intAgentID]
		);

        foreach($arrPasswordHistory as $arrHash)
        {
            $arrHashes[$arrHash['password_num']] = $arrHash['password'];
        }

        return $arrHashes;
    }

    // retrieves forgot password hash
    public function getActiveHash()
    {
        global $objConnRead;

		$this->strActiveHash = $objConnRead->get($this->strForgotPWTable, "hash", [
			"AND" => [
				"agent_id" => $this->intAgentID,
				"active" => 1,
				"process_date[>=]" => gmdate("Y-m-d H:i:s")
			],
			"ORDER" => [
				"process_date" => "DESC"
			]
		]);

        return $this->strActiveHash;
    }

	public function getPasswordRequirements()
	{
		$objPassword = new Password();

		return [
			"upper" 		=> $objPassword->blnUpper,
			"lower" 		=> $objPassword->blnLower,
			"special" 		=> $objPassword->blnSpecial,
			"digit"			=> $objPassword->blnDigit,
			"repeat" 		=> $objPassword->blnRepeat,
			"minLength" 	=> $objPassword->intMinLen,
			"historyLength" => $objPassword->intMaxHistory
		];
	}
}
