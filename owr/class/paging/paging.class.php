<?php
class Paging 
{
	private $_intPerPage = 10; //class default of 10
	 
	private $_intTotalItems;  //Total item count
	private $_intCurrPage = '';
	private $_intLastPage = '';

	function paging($intCount = 0, $intCurrPage = 1, $intPerPage = 10, $arrSmartLoad = false)	
	{
		$this->setCount($intCount);

		if(!empty($arrSmartLoad) && empty($intCurrPage)) {
			preg_match('!/([^\.]+)\.[^\.]*$!', $_SERVER['HTTP_REFERER'], $arrMatch);

			for($i=0; $i < sizeof($arrSmartLoad); ++$i) {
				if($arrMatch[1] == $arrSmartLoad[$i]) {
					if($this->load()) {
						return;
					}
				}
			}
		}
		
		$this->setCurrPage($intCurrPage);
		$this->setPerPage($intPerPage);
		$this->setLastPage();
	}

	function setPerPage($intPerPage = null) 
	{	
		$this->_intPerPage = $intPerPage;
	}

	function setCount($intCount = null)
	{
		$this->_intTotalItems = $intCount;
	}

	function setCurrPage($intCurrPage = 1)
	{
		if($intCurrPage < 1) $intCurrPage = 1;
		$this->_intCurrPage = $intCurrPage;
	}

	function setLastPage($intLastPage = null)
	{
		if(empty($intLastPage)) {
			$intCount = $this->getCount();
			$intPerPage = $this->getPerPage();
			$intLastPage = ceil($intCount / $intPerPage);
			if(empty($intLastPage)) {
				$intLastPage = 1;
			}

			$this->_intLastPage = $intLastPage;
			return;
		}

		$this->_intLastPage = $intLastPage;
	}
	
	function getPerPage()			{ return $this->_intPerPage;			}
	function getCurrPage()			{ return $this->_intCurrPage;			}
	function getCount()			{ return $this->_intTotalItems;			}
	function getLastPage()			{ return $this->_intLastPage;			}
	
	function getLimit()
	{
		$intCurrPage = $this->getCurrPage();
		$intLastPage = $this->getLastPage();
		$intPerPage = $this->getPerPage();

		//prevent being too far into paging
		if($intCurrPage > $intLastPage) {
			$intCurrPage = $intLastPage;
			$this->setCurrPage($intCurrPage);
		}
			
		$intPage = $intCurrPage - 1;
		$intStart = $intPage * $intPerPage;
	
		if($intCurrPage > 0) {
			return "LIMIT $intStart,$intPerPage";
		}
		else {
			return "LIMIT 0,$intPerPage";
		}
	}

	function getStart()
	{
		$intCurrPage = $this->getCurrPage();
		$intLastPage = $this->getLastPage();
		$intPerPage = $this->getPerPage();

		//prevent being too far into paging
		if($intCurrPage > $intLastPage) {
			$intCurrPage = $intLastPage;
			$this->setCurrPage($intCurrPage);
		}
			
		$intPage = $intCurrPage - 1;
		return $intPage * $intPerPage;
	}

	function setPagination() 
	{
		$this->save();
		global $smarty;
		$smarty->assign('thePaging', $this);
	}

	function save()
	{
		$_SESSION['paging'] = serialize($this);
	}

	function load()
	{
		$thePaging = unserialize($_SESSION['user']['paging']);
		if(is_object($thePaging)) {
			$this->setCurrPage($thePaging->getCurrPage());
			$this->setPerPage($thePaging->getPerPage());
			$this->setLastPage();
			return true;
		}
		return false;
	}

	function getExistingQS()
	{
		$strQS = '';
		$strQS = preg_replace('!&?page=[^&]+!i', '', $_SERVER['QUERY_STRING']);
		if($strQS) $strQS .= '&';
		return $strQS;
	}
}