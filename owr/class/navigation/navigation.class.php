<?php

require_once CLASS_DIR . 'tag/tag.class.php';

class Navigation
{
	public static function set($strPrimary = '', $strSecondary = '')
	{
		global $smarty;

		if(!Tag::exists($strPrimary.'-'.$strSecondary, $_SESSION['user']->tags))
		{
			unset($_SESSION['last_page']);
		}

		if(is_object($smarty) && !empty($strPrimary)) $smarty->assign('strNavigationPrimary', $strPrimary);
		if(is_object($smarty) && !empty($strSecondary)) $smarty->assign('strNavigationSecondary', $strSecondary);
	}

	public static function setTitle($strTitle = '')
	{
		global $smarty;

		if(is_object($smarty) && !empty($strTitle)) $smarty->assign('strTitle', $strTitle);
	}

	public static function setBreadcrumb($arrBreadcrumbs = array())
	{
		global $smarty;

		if(is_object($smarty) && !empty($arrBreadcrumbs)) $smarty->assign('arrBreadcrumbs', $arrBreadcrumbs);
	}
}
