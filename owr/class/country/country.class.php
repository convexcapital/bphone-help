<?php
class Country
{
	CONST COUNTRY_CODE_US = 236;

	public function __construct($strID = '', $strISO2 = '')
	{
		$this->id = $strID;
		$this->iso2 = $strISO2;
	}

	public function getOne()
	{
		global $objConnRead;
		global $objMem;

		if(empty($this->id) && empty($this->iso2)) return array();

		$strKey = sha1("bp-Country-getOne-{$this->id}-{$this->iso2}");
		$arrReturn = $objMem->get($strKey);

		//if id for the object is not, set it to the results of this so we can use it later if needed
		if(!empty(@$strReturn['country_id']))
		{
			if(empty($this->id)) $this->id = $arrReturn['country_id'];
			return $arrReturn;
		}

		//if we are loading based on ID pull based on country id
		if($this->id)
		{
			$arrReturn = $objConnRead->get(
				'countries', '*', ['AND'=>['country_id'=>$this->id]]
			);
			$objMem->set($strKey, 30, null, $arrReturn);
			return $arrReturn;
		}

		//pull based on iso2 format
		$arrReturn = $objConnRead->get(
			'countries', '*', ['AND'=>['alpha2_code'=>strtoupper($this->iso2)]]
		);
		$this->id = $arrReturn['country_id'];
		$objMem->set($strKey, 30, null, $arrReturn);
		return $arrReturn;
	}

	public function load()
	{
		$this->loadFromPost($this->getOne());
	}

	public function getAll($blnFiltered = true)
	{
		global $objConnRead;
		$arrFilter = array();
		$arrFilter['order']['short_name'] = 'ASC';
		if($blnFiltered) $arrFilter['AND']['active'] = 1;
		$arrResult = $objConnRead->select('countries(c)', '*',$arrFilter);
		return $arrResult;
	}

	public function getAllAssoc($blnFiltered = true)
	{
		$arrReturn = array();
		$arrResult = $this->getAll($blnFiltered);
		foreach($arrResult as &$val)
		{
			$arrReturn[$val['country_id']] = $val['short_name'];
		}
		return $arrReturn;
	}

	public function getISOJson()
	{
		$arrResults = $this->getAll();
		$arrReturn = array();
		foreach($arrResults as &$key)
		{
			$arrReturn[] = $key['alpha2_code'];
		}
		return $arrReturn;
	}

	public function loadFromPost($arrPost = array())
	{
		if(empty($arrPost)) return false;

		foreach($arrPost as $key=>&$val)
		{
			if($key == 'counmtry_id' || $key == 'id') continue;
			if($key == 'company_id') continue;
			$this->{$key} = $val;
		}
	}

	public static function getUSA() { return 234; }
}
