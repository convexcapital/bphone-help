<?php
require_once CLASS_DIR . 'util/util.class.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'company/company.class.php';

class FusionCDRFormat
{
	public $objCDRFormatted;
	protected $objCDR;

	public function __construct($objCDR=[])
	{
		$this->objCDRFormatted = new stdClass();
		$this->objCDR = new stdClass();

		// set default unformatted CDR
		$this->objCDR = $objCDR;

		// set default unformatted values
		$this->objCDRFormatted->uuid			= $objCDR->uuid;
		$this->objCDRFormatted->direction 		= $objCDR->direction;
		$this->objCDRFormatted->cid 			= $objCDR->caller_id_name;
		$this->objCDRFormatted->source 			= $objCDR->caller_id_number;
		$this->objCDRFormatted->destination		= $objCDR->destination_number;
		$this->objCDRFormatted->datetime_start 	= $objCDR->start_epoch;
		$this->objCDRFormatted->tta 			= "";
		$this->objCDRFormatted->duration 		= $objCDR->duration;
		$this->objCDRFormatted->pdd 			= $objCDR->pdd_ms;
		$this->objCDRFormatted->mos 			= $objCDR->rtp_audio_in_mos;
		$this->objCDRFormatted->outcome 		= $objCDR->hangup_cause;
		$this->objCDRFormatted->has_recording	= 0;

		// format values
		$this->formatDestinationNumber();
		$this->formatSourceNumber();
		$this->formatCID();
		$this->formatDuration();
		$this->formatPDD();
		$this->formatTTA();
		$this->formatOutcome();
		$this->formatStartEpoch();
		$this->setHasRecording();
		$this->setDirectionIcon();
	}

	private function formatDestinationNumber()
	{
		try {
			$this->objCDRFormatted->destination = Util::formatPhoneNumber($this->objCDR->destination_number);
		} catch(Exception $e) {
			$this->objCDRFormatted->issues['destination'] = $e->getMessage();
		}
	}

	private function formatSourceNumber()
	{
		try {
			$this->objCDRFormatted->source = Util::formatPhoneNumber($this->objCDR->caller_id_number);
		} catch(Exception $e) {
			$this->objCDRFormatted->issues['source'] = $e->getMessage();
		}
	}

	private function formatCID()
	{
		try {
			$this->objCDRFormatted->cid = Util::formatPhoneNumber($this->objCDR->caller_id_name);
		} catch(Exception $e) {
			$this->objCDRFormatted->issues['cid'] = $e->getMessage();
		}
	}

	private function formatDuration()
	{
		$this->objCDRFormatted->duration = Util::seconds2human($this->objCDR->duration);
	}

	private function formatPDD()
	{
		$this->objCDRFormatted->pdd = number_format($this->objCDR->pdd_ms/1000,2)."s";
	}

	private function formatTTA()
	{
		$this->objCDRFormatted->tta = $this->objCDR->tta."s";
	}

	private function formatOutcome()
	{
		$this->objCDRFormatted->outcome = ucwords(strtolower(str_replace("_", " ", $this->objCDR->hangup_cause)));
	}

	private function formatStartEpoch()
	{
		$this->objCDRFormatted->datetime_start = timezone::convert_to_user_date(gmdate("Y-m-d H:i:s", $this->objCDRFormatted->datetime_start), Company::getTimeZone());
	}

	private function setHasRecording()
	{
		if($this->objCDR->recording_file) $this->objCDRFormatted->has_recording = 1;
	}

	private function setDirectionIcon()
	{
		if($this->objCDR->direction == 'inbound' || $this->objCDR->direction == 'local')
		{
			if ($this->objCDR->answer_stamp != '' && $this->objCDR->bridge_uuid != '')
			{
				$strCallResult = 'answered';
			}
			else if ($this->objCDR->answer_stamp != '' && $this->objCDR->bridge_uuid == '')
			{
				$strCallResult = 'voicemail';
			}
			else if ($this->objCDR->answer_stamp == '' && $this->objCDR->bridge_uuid == '' && $this->objCDR->sip_hangup_disposition != 'send_refuse')
			{
				$strCallResult = 'cancelled';
			}
			else
			{
				$strCallResult = 'failed';
			}
		}
		else if ($this->objCDR->direction == 'outbound')
		{
			if ($this->objCDR->answer_stamp != '' && $this->objCDR->bridge_uuid != '')
			{
				$strCallResult = 'answered';
			}
			else if ($this->objCDR->answer_stamp == '' && $this->objCDR->bridge_uuid != '')
			{
				$strCallResult = 'cancelled';
			}
			else
			{
				$strCallResult = 'failed';
			}
		}

		$strIcon = "icon_cdr_{$this->objCDR->direction}_{$strCallResult}.png";

		$arrIcons = [
			'icon_cdr_inbound_answered.png',
			'icon_cdr_inbound_cancelled.png',
			'icon_cdr_inbound_failed.png',
			'icon_cdr_inbound_voicemail.png',
			'icon_cdr_local_answered.png',
			'icon_cdr_local_cancelled.png',
			'icon_cdr_local_failed.png',
			'icon_cdr_local_voicemail.png',
			'icon_cdr_outbound_answered.png',
			'icon_cdr_outbound_cancelled.png',
			'icon_cdr_outbound_failed.png',
			'icon_cdr_outbound_voicemail.png'
		];

		if(in_array($strIcon, $arrIcons))
		{
			$this->objCDRFormatted->direction_icon = $strIcon;
			$this->objCDRFormatted->direction_text = "{$this->objCDR->direction}: {$strCallResult}";
		}
	}
}
