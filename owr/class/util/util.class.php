<?php

class util
{
    public function getGUID()
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    #sensitive data encryption
    public static function encrypt($strData = '')
    {
        return base64_encode(
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_256,
                md5(SENSATIVE_DATA_KEY),
                $strData,
                MCRYPT_MODE_CBC,
                md5(md5(SENSATIVE_DATA_KEY))
            )
        );
    }

    #sensative data decryption
    public static function decrypt($strData = '')
    {
        return rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_256,
                md5(SENSATIVE_DATA_KEY),
                base64_decode($strData),
                MCRYPT_MODE_CBC,
                md5(md5(SENSATIVE_DATA_KEY))
            ),
        "\0");
    }

    public static function seconds2human($ss)
    {
        $s = $ss % 60;
        $m = floor(($ss % 3600) / 60);
        $h = floor(($ss % 86400) / 3600);
        $d = floor(($ss % 2592000) / 86400);
        $M = floor($ss / 2592000);

        if (strlen($s) == '1') {
            $s = '0'.$s;
        }
        if (strlen($m) == '1') {
            $m = '0'.$m;
        }
        if (strlen($h) == '1') {
            $h = '0'.$h;
        }
        if (strlen($d) == '1') {
            $d = '0'.$d;
        }

        $strTime = "$d:$h:$m:$s";
        if ($d == '00') {
            $strTime = substr($strTime, 3);
            if ($h == '00') {
                $strTime = substr($strTime, 3);
            }
        }

        return $strTime;
    }

    public function human2seconds($hms)
    {
        list($h, $m, $s) = explode(':', $hms);
        $seconds = 0;
        $seconds += (intval($h) * 3600);
        $seconds += (intval($m) * 60);
        $seconds += (intval($s));

        return $seconds;
    }

    public static function cleanStrPhone($strPhone)
    {
		if(strlen($strPhone)>10) $strPhone = preg_replace('!^\+?1!', '', $strPhone);

        return preg_replace('![^0-9]!', '', $strPhone);
    }

    public static function validateGUID($strString = '')
    {
        return preg_match("/^(\{)?[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}(?(1)\})$/i", $strString);
    }

    public static function checkFileExistsRemote($strURL)
    {
        if (!$strURL) {
            return false;
        }

        $file_headers = @get_headers($strURL);
        if ($file_headers[0] == 'HTTP/1.1 200 OK') {
            return true;
        }

        return false;
    }

    public static function strToPhone($str)
    {
        $str = self::cleanStrPhone($str);

        if (!$str) {
            return '';
        }

        if (strlen($str) == '10') {
            return '('.substr($str, 0, 3).') '.substr($str, 3, 3).'-'.substr($str, 6);
        }

        if (strlen($str) == '7') {
            return substr($str, 0, 3).'-'.substr($str, 3);
        }
    }

    public static function sendRequest($strURL = '', $arrData = '', $strMethod = 'POST', $arrHeaders = array(), $arrCredentials = array())
    {
        if (!$strURL) {
            return false;
        }

        #if this is a GET request, append data to the url
        if ($strMethod == 'GET') {
            $strURL .= '?'.http_build_query($arrData, '', '&');
        }

        #init curl
        $ch = curl_init();

        #base curl options
        curl_setopt($ch, CURLOPT_URL, $strURL);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        #if there is data and the method is post, create post fields
        if ($strMethod == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            if (is_array($arrData)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arrData, '', '&'));
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $arrData);
            }
        }

        #if credentials are supplied, add via option
        if ($arrCredentials['username'] && $arrCredentials['password']) {
            curl_setopt($ch, CURLOPT_USERPWD, "{$arrCredentials['username']}:{$arrCredentials['password']}");
        }

        #if headers are provided, add via option
        if (count($arrHeaders)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $arrHeaders);
        }

        #create a fancy little array to show not just response, but error data, total time, etc
        $arrResponse['response'] = html_entity_decode(curl_exec($ch));
        $arrResponse['curl_error'] = curl_error($ch);
        $arrResponse['curl_errno'] = curl_errno($ch);

        $arrHeaders = curl_getinfo($ch);
        $arrResponse['total_time'] = $arrHeaders['total_time'];

        #clean up
        curl_close($ch);

        return $arrResponse;
    }

    public static function time_elapsed_string($datetime, $full = false)
    {
        $now = new DateTime();
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );

        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k.' '.$v.($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string).' ago' : 'just now';
    }

	public static function formatPhoneNumber($strPhoneNumber="")
	{
		// google number formatter
		$objPhoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

	    $strNumberProto = $objPhoneUtil->parse($strPhoneNumber, "US");

		if($objPhoneUtil->isValidNumber($strNumberProto))
		{
			return $objPhoneUtil->format($strNumberProto, \libphonenumber\PhoneNumberFormat::NATIONAL);
		}
		else
		{
			throw new Exception("The phone number provided was not a valid US number.");
		}
	}
}
