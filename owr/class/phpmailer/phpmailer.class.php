<?php
require_once INCLUDES_DIR . 'database.inc.php';
require_once INCLUDES_DIR . 'smarty.inc.php';

class PHPMailerChild extends PHPMailer
{
    // Set default variables for all new objects
    public $Username        = 'engage_mail';
    public $Password        = '6Bq@@d&3';
    public $Host            = 'smtp.sendgrid.net';
    public $Port            = 587;
    public $Mailer          = 'smtp';     // Alternative to isSMTP()
    public $SMTPAuth        = true;
    public $WordWrap        = 75;
    public $From            = 'no-reply@businessphone.com';
    public $FromName        = 'Customer Support';
    public $ReplyTo         = 'no-reply@businessphone.com';
    public $Subject         = '';
    public $CustomSendError = '';

    public function __construct($strSubject='', $arrEmailRecipients=[], $strFrom = "no-reply@businessphone.com")
    {
		$this->Subject 	= $strSubject;
		$this->From 	= $strFrom;
		$this->ReplyTo 	= $strFrom;

		// Loop through array of recipients
		for($i=0; $i<sizeof($arrEmailRecipients); ++$i)
		{
			$this->addOrEnqueueAnAddress('to', $arrEmailRecipients[$i]);
		}
    }

    public function sendResetPassword($strUserName = '', $strHash = '', $strServer)
	{
		global $objConnRead, $smarty;

		$strMsgBody = '';

		$this->addOrEnqueueAnAddress('to', $strUserName, $arrUserData->first_name . ' ' . $arrUserData->last_name);
		$this->Subject = "Reset your portal.businessphone.com password.";

		/* assign appropriate smarty variables before building the body */
		$smarty->assign('strUserName', $strUserName);
		$smarty->assign('strHash', $strHash);
		$smarty->assign('strServer', SERVER_NAME);

		$strMsgBody = $smarty->fetch('email/reset-password.tpl');

		/* make sure PHP Mailer knows the body is encoded in HTML */
		$this->msgHTML($strMsgBody);

		/* Attempt to send, return some sort of message -- this might
		be changed later to update the logs */
		if(!$this->send())
		{
			$this->CustomSendError = "Message failed to send.\n Username: ".$strUserName;
			return false;
			exit();
		}

		return true;
    }

	public function sendWelcomeEmail($arrData)
	{
		global $smarty;

		$strMsgBody = '';

		$this->addOrEnqueueAnAddress('to', $arrData['email'], $arrData['first_name'] . ' ' . $arrData['last_name']);
		$this->Subject = "Welcome to Business Phone.";

		/* assign appropriate smarty variables before building the body */
		$smarty->assign('strUsername', $arrData['email']);
		$smarty->assign('strPassword', $arrData['password']);
		$smarty->assign('strServer'	 , SERVER_NAME);

		$strMsgBody = $smarty->fetch('email/welcome-email.tpl');

		/* make sure PHP Mailer knows the body is encoded in HTML */
		$this->msgHTML($strMsgBody);

		/* Attempt to send, return some sort of message -- this might
		be changed later to update the logs */
		if(!$this->send())
		{
			$this->CustomSendError = "Message failed to send.\n Username: ". $arrData['email'];
			return false;
			exit();
		}

		return true;
	}
}
