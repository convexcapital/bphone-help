<?php
require_once CLASS_DIR . 'user/user.class.php';
require_once CLASS_DIR . 'util/util.class.php';

class Number
{
	public function __construct($strID = '')
	{
		$this->id = $strID;
	}

	public function load()
	{
		$this->loadFromPost($this->getOne());
	}

	public function getOne()
	{
		global $objConnRead;
		$arrNumber = $objConnRead->get(
			'numbers(n)',
			['[>]countries(c)'=>'country_id'],
			['n.number_id', 'n.number_e164', 'n.description', 'c.short_name', 'c.alpha2_code', 'c.alpha3_code', 'c.numeric_code'],
			[
				'AND'=>['n.company_id'=>User::getCompanyID(), 'number_id'=>$this->id]
			]
		);
		return $arrNumber;
	}

	public function getAll()
	{
		global $objConnRead;
		$arrNumbers = $objConnRead->select(
			'numbers(n)',
			['[>]countries(c)'=>'country_id'],
			['n.number_id', 'n.number_e164', 'n.description', 'c.short_name', 'c.alpha2_code', 'c.alpha3_code', 'c.numeric_code'],
			[
				'AND'=>['n.company_id'=>User::getCompanyID()],
				'ORDER'=>['n.number_e164'=>'ASC']
			]
		);
		return $arrNumbers;
	}

	public function save($blnDelete = false)
	{
		global $objConn;
		if($blnDelete)
		{
			if(empty($this->id)) return false;
			$objConn->delete('numbers', ['AND'=>[
				'company_id'=>User::getCompanyID(),
				'number_id'=>$this->id
			]]);
			return true;
		}

		$arrData = array();
		$arrData['number_e164'] = $this->getE164();
		$arrData['description'] = $this->description;
		$arrData['country_id'] = $this->country_id;
		if($this->id)
		{
			$objConn->update('numbers', $arrData, ['AND'=>[
				'company_id'=>User::getCompanyID(),
				'number_id'=>$this->id
			]]);
		}
		else
		{
			$arrData['company_id'] = User::getCompanyID();
			$objConn->insert('numbers', $arrData);
		}
	}

	public function getE164($strISO = '')
	{
		if(!empty($this->number_e164)) return $this->number_e164;
		if(empty($strISO))
		{
			$objCountry = new Country($this->country_id, $this->country_iso2);
			$objCountry->load();
			if(empty($this->country_id)) $this->country_id = $objCountry->id;

			#if we have issues for some reason, just assume US
			$strISO = !empty($objCountry->alpha2_code) ? $objCountry->alpha2_code : 'us';
		}

		$objPhoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
	    $strNumberProto = $objPhoneUtil->parse($this->number, strtoupper($strISO));
		return $objPhoneUtil->format($strNumberProto, \libphonenumber\PhoneNumberFormat::E164);
	}

	public static function format($strPhoneNumber = '', $strFormat = '', $strISO2 = 'us')
	{
		// google number formatter
		$objPhoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

	    $strNumberProto = $objPhoneUtil->parse($strPhoneNumber, strtoupper($strISO2));

		if($objPhoneUtil->isValidNumber($strNumberProto))
		{
			return $objPhoneUtil->format($strNumberProto, \libphonenumber\PhoneNumberFormat::NATIONAL);
		}
		else
		{
			throw new Exception("The phone number provided was not a valid US number.");
		}
	}

	public function loadFromPost($arrPost = array())
	{
		foreach($arrPost as $key=>&$val)
		{
			if($key == 'number_id' || $key == 'id') continue;
			if($key == 'company_id') continue;
			$this->{$key} = $val;
		}
	}

	public function isItADuplicate($strNumber = '')
	{
		global $objConnRead;
		$strNumber = preg_replace('![^0-9+]!', '', $strNumber);
		return $objConnRead->has('numbers', ['AND'=>[
			'number_e164'=>$strNumber,
			'number_id[!]'=>$this->id
		]]);
	}

	/*
		Return the regex version of a number for dialplan matching
		e.g.: ((\+?1)?6143796508)
	*/
	public function getRegex()
	{
		$strReturn = '';
		//get the country data so we can split out the number
		$objPhoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
	    $strNumberProto = $objPhoneUtil->parse($this->number_e164, strtoupper($this->alpha2_code));

		//get the FULL number information
		$strInt = $objPhoneUtil->format($strNumberProto, \libphonenumber\PhoneNumberFormat::E164);


		//find the difference to get JUST the country code
		$strCountryCode = $strNumberProto->getCountryCode();
		$strReturn = str_replace("+{$strCountryCode}", "(\+?{$strCountryCode})?", $strInt);

		return "({$strReturn})";
	}

	public function lookupNumberAPI($strNumber = '')
	{
		$arrResponse = Util::sendRequest(
			"https://lookups.twilio.com/v1/PhoneNumbers/{$strNumber}",
			['Type'=>'carrier'],
			'GET',
			null,
			['username'=>TWILIO_SID, 'password'=>TWILIO_TOKEN]
		);
		return json_decode($arrResponse['response']);
	}
}
