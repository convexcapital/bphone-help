<?php

require_once CLASS_DIR . 'location/location.class.php';
require_once CLASS_DIR . 'skill/skill.class.php';

class Filter
{

	private $arrFilters;

	public function __construct($arrFilters)
	{
		global $smarty;

	    $this->setArrFilters($arrFilters);

	    foreach($arrFilters as $filter)
	    {
	      $this->$filter = true;
	      $smarty->assign($filter,true);
	    }

	    $this->buildFilters();
	    $smarty->assign('blnFilters',true);
	}

	public function setArrFilters($arrFilters)
	{
		$this->arrFilters = $arrFilters;
	}

	public function getArrFilters()
	{
		return $this->arrFilters;
	}

	public function buildFilters()
	{
		global $smarty;

		if($this->blnLocations) $smarty->assign('arrLocations',Location::getAll());
		if($this->blnSkills) 	$smarty->assign('arrSkills', Skill::getAll());
	}

}
