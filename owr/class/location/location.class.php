<?php

require_once CLASS_DIR.'user/user.class.php';
require_once CLASS_DIR.'util/util.class.php';

class Location
{
    public static function getCount($strCompanyID = '', $strSmartyKey = '')
    {
        global $smarty;
        global $objMem;

		if(empty($strCompanyID)) {
            $strCompanyID = Company::getID();
        }

        $strKey = sha1("location-count-{$strCompanyID}");
		$arrLocations = $objMem->get($strKey);

        if(empty($arrLocations))
        {
            $arrLocations = self::getAll($strCompanyID);

            $objMem->set($strKey, $arrLocations, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, count($arrLocations));

        return count($arrLocations);
    }

    public static function getAll($strCompanyID = '', $strSmartyKey = '')
    {
        global $smarty;
        global $objConnRead;

        if (empty($strCompanyID)) {
            $strCompanyID = Company::getID();
        }

        $arrData = $objConnRead->select('locations(l)', [
            'l.location_id(location_id)',
			'l.location_uuid',
            'l.company_id',
            'l.number',
            'l.address1',
            'l.address2',
            'l.city',
            'l.state',
            'l.zip',
            'l.phone1',
            'l.phone2',
            'l.isp_name',
            'l.isp_type',
            'l.isp_phone1',
            'l.mb_down',
            'l.mb_up',
        ], [
            'AND' => [
                'l.company_id' => $strCompanyID,
            ],
        ]);

        for ($i = 0; $i < sizeof($arrData); ++$i) {
            $arrData[$i] = (object) $arrData[$i];
            $objData = $arrData[$i];

            $objData->phone1 = util::strToPhone($objData->phone1);
            $objData->phone2 = util::strToPhone($objData->phone2);
            $objData->isp_phone1 = util::strToPhone($objData->isp_phone1);
        }

        if (!empty($strSmartyKey) && is_object($smarty)) {
            $smarty->assign($strSmartyKey, $arrData);
        }

        return $arrData;
    }

	public static function getAllTagsInputJSON()
	{
		global $objConnRead;

        if (empty($strCompanyID)) {
            $strCompanyID = Company::getID();
        }

        $arrData = $objConnRead->select('locations(l)', [
            'l.location_uuid(uuid)',
			'l.number(name)'
        ], [
            'AND' => [
                'l.company_id' => $strCompanyID,
            ],
        ]);

        return json_encode($arrData);
	}

	public static function getSelectedTagsInputJSON($arrUUID=[])
	{
		global $objConnRead;

		if(empty($arrUUID)) return false;

        if (empty($strCompanyID)) {
            $strCompanyID = Company::getID();
        }

        $arrData = $objConnRead->select('locations(l)', [
            'l.location_uuid(uuid)',
			'l.number(name)'
        ], [
            'AND' => [
                'l.company_id' => $strCompanyID,
				'l.location_uuid' => $arrUUID
            ],
        ]);

        return json_encode($arrData);
	}

    public static function getOne($strLocationID = '', $strSmartyKey = '')
    {
        global $smarty;
        global $objConnRead;

        $objData = (object)$objConnRead->get('locations(l)', [
            'l.location_id(location_id)',
            'l.company_id',
            'l.number',
            'l.address1',
            'l.address2',
            'l.city',
            'l.state',
            'l.zip',
            'l.phone1',
            'l.phone2',
            'l.isp_name',
            'l.isp_type',
            'l.isp_phone1',
            'l.mb_down',
            'l.mb_up',
        ], [
            'AND' => [
                'l.location_id' => $strLocationID,
                'l.company_id' => Company::getID(),
            ],
        ]);

        $objData->phone1 = util::strToPhone($objData->phone1);
        $objData->phone2 = util::strToPhone($objData->phone2);
        $objData->isp_phone1 = util::strToPhone($objData->isp_phone1);

        if (!empty($strSmartyKey) && is_object($smarty)) {
            $smarty->assign($strSmartyKey, $objData);
        }

        return $objData;
    }

    public static function getOneByUUID($strUUID = '', $strSmartyKey = '')
    {
        global $smarty;
        global $objConnRead;

        if(empty($strUUID)) return false;

        $objData = (object)$objConnRead->get('locations(l)', '*', [
            'AND' => [
                'l.location_uuid' => $strUUID,
            ],
        ]);

        if(!$objData->location_id) return false;

        if (!empty($strSmartyKey) && is_object($smarty)) {
            $smarty->assign($strSmartyKey, $objData);
        }

        return $objData;
    }

    public static function getOneByNumber($strNumber = '', $strSmartyKey = '')
    {
        global $smarty;
        global $objConnRead;

        if(empty($strNumber)) return false;

        $objData = (object)$objConnRead->get('locations(l)', '*', [
            'AND' => [
                'l.number' => $strNumber,
            ],
        ]);

        if(!$objData->location_id) return false;

        if (!empty($strSmartyKey) && is_object($smarty)) {
            $smarty->assign($strSmartyKey, $objData);
        }

        return $objData;
    }

    public static function save($strLocationID = '', $arrData = array())
    {
        if(empty($arrData)) return false;

        global $objConn;

        $arrData['phone1'] = util::cleanStrPhone($arrData['phone1']);
        $arrData['phone2'] = util::cleanStrPhone($arrData['phone2']);

        if(empty($strLocationID))
        {
            $strLocationID = $objConn->insert('locations', $arrData);
        }else{
            $objConn->update('locations', $arrData, [
                'AND' => [
                    'location_id' => $strLocationID,
                ],
            ]);
        }

        if($objConn->error()[2]) throw new exception($objConn->error()[2]);

        return $strLocationID;
    }

    public static function deleteNotInArray($strCompanyID = '', $arrData = array())
    {
        if(empty($arrData)) return true;

        if(empty($strCompanyID)) return false;

        global $objConn;

        $arrData = $objConn->delete('locations', [
            'AND' => [
                'company_id'        =>  $strCompanyID,
                'OR'                => [
                    'location_uuid[!]'  => $arrData,
                    'location_uuid'     => NULL,
            	]
            ],
        ]);

        if($objConn->error()[2]) return false;

        return true;
    }
}
