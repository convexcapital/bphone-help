<?php
require_once CLASS_DIR . 'authuser/authuser.class.php';
require_once CLASS_DIR . 'company/company.class.php';
require_once CLASS_DIR . 'tag/tag.class.php';
require_once CLASS_DIR . 'ticket/ticket.class.php';

class User
{
	function __construct(){}

	function login($strUsername='', $strPassword='')
	{
		global $smarty, $objConn;

		// attempt to use new password hashing
		$AuthUser = new AuthUser($strUsername, [0 => $strPassword]);

		try{
			$objUser = $AuthUser->validateAndReturnUser();
		}catch(Exception $e){
			$smarty->assign('strMessage', $e->getMessage());
			return false;
		}

		if(!empty($objUser->user_id))
		{
			$_SESSION['logged_in'] = true;

			unset($objUser->password1);
			$_SESSION['user'] = $objUser;
			$_SESSION['user']->tags = Tag::getAllByUser($_SESSION['user']->user_id);
			#$_SESSION['user']->tenants = json_decode($_SESSION['user']->tenants);
			$_SESSION['user']->tenants[] = $objData->company_id;
			$_SESSION['company'] = Company::getOne($_SESSION['user']->company_id);
			$_SESSION['company']->domain_uuids = Company::getAllDomainUUIDs($_SESSION['user']->company_id);

			if(empty(Ticket::getUserID())) Ticket::getUserID(self::getEmail());
			if(empty(Ticket::getCompanyID())) Ticket::getCompanyID(Company::getName());

			$objConn->update(
				'users',
				['datetime_last_login' => gmdate('Y-m-d H:i:s')],
				['user_id' => $_SESSION['user']->user_id]
			);

			if(!empty($_SESSION['last_page'])) die(header("location: {$_SESSION['last_page']}"));

			return true;
		}

		return false;
	}

	function isLoggedIn()
	{
		global $smarty;

		if(!$_SESSION['logged_in'])
		{
			$smarty->display('login.html');
			die();
		}
	}

	function logout() { session_destroy(); }

	public static function getOne($strUserID = '', $strSmartyKey = '')
	{
		global $smarty;
		global $objMem;
		global $objConnRead;

		if(empty($strUserID)) return false;

		$strKey = sha1("user-{$strUserID}");
		$objMem->delete($strKey);
		$objUser = $objMem->get($strKey);

		if(empty($objUser->user_id))
		{
			$objData = (object)$objConnRead->get('users', '*', [
				'AND' => [
			        'user_id' 		=>$strUserID,
			        'company_id' 	=>Company::getID()
			    ]
			]);

			if(!empty($objData->user_id))
			{
				$objMem->set($strKey, $objData, null, 60*1);
			}
		}

		if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $objData);

		return $objData;
	}

	public static function getAll($strCompanyID = '', $strSmartyKey = '')
	{
		global $smarty;
		global $objMem;
		global $objConnRead;

		if(empty($strCompanyID)) $strCompanyID = Company::getID();

		$strKey = sha1("users-{$strCompanyID}");
		$objMem->delete($strKey);
		$arrData = $objMem->get($strKey);

		if(empty($arrData))
		{
			$arrData = $objConnRead->select('users', '*', [
				'AND' => [
			        'company_id'     =>	$strCompanyID,
			    ]
			]);

			for($i = 0; $i < sizeof($arrData); ++$i)
			{
				$objUser = (object)$arrData[$i];
				$objUser->name = "{$objUser->first_name} {$objUser->last_name}";
				$arrData[$i] = $objUser;
			}

			$objMem->set($strKey, $arrData, null, 60*1);
		}

		if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrData);

		return $arrData;
	}

	public static function getCount($strCompanyID = '', $strSmartyKey = '')
    {
        global $smarty;
        global $objMem;

		if(empty($strCompanyID)) {
            $strCompanyID = Company::getID();
        }

        $strKey = sha1("user-count-{$strCompanyID}");
		$arrUsers = $objMem->get($strKey);

        if(empty($arrUsers))
        {
            $arrUsers = self::getAll($strCompanyID);

            $objMem->set($strKey, $arrUsers, null, 60*1);
        }

        if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, count($arrUsers));

        return count($arrUsers);
    }

	public static function save($strUserID = '', $arrData = array())
    {
		if(empty($arrData)) return false;

		global $objConn;

		// get company id
		$strCompanyID = Company::getID();

		// if we are trying to update password, verify both passwords match
		if(!empty($arrData['password']) && !empty($arrData['confirm_password']) && $arrData['password'] != $arrData['confirm_password'])
		{
			throw new exception('passwords do not match');
		}

		// format data we are sending to database
		$arrPost = [
			'first_name' 		=> $arrData['first_name'],
			'last_name' 		=> $arrData['last_name'],
			'email' 			=> $arrData['email'],
			'pbx_user_uuid' 	=> $arrData['pbx_user_uuid']
		];

		// create new user
		if(empty($strUserID))
		{
			// need company for new users
			$arrPost['company_id'] = $strCompanyID;

			// insert
			$objConn->insert('users', $arrPost);

			// get user id
			$strUserID = $objConn->id();
		}
		// update user
		else
		{
			$objConn->update('users', $arrPost, [
				'AND' => [
					'company_id' 	=> $strCompanyID,
					'user_id' 		=> $strUserID
				]
			]);
		}

		// verify no db issues
		if($objConn->error()[2]) throw new exception($objConn->error()[2]);

		// update password
		if(!empty($arrData['password']) && !empty($arrData['confirm_password']) && $arrData['password'] == $arrData['confirm_password'])
		{
			// instantiate authuser class
			$AuthUser = new AuthUser("", [$arrData['password'], $arrData['confirm_password']], $strUserID);

			// reset password
			if( ! $AuthUser->setPasswordNoValidation($arrData['welcome_email'] ? 1 : 0)) throw new exception('failed to update password');
		}

		if($arrData['welcome_email']) self::sendWelcomeEmail($arrData);

		return $strUserID;

    }

	public function delete($intAgentID)
	{
		if(empty($intAgentID)) return false;

		global $objConn;

		$objConn->delete('users',[
			'AND' => [
				'user_id' => $intAgentID
			]
		]);

		if($objConn->error()[2]) return false;

        return true;
	}


	public function getPBXUsers($arrDomainUUIDs = array())
    {
		require_once INCLUDES_DIR . 'fusion.database.inc.php';
        global $objFusionConnRead;

		global $objMem;
		if(empty($arrDomainUUIDs)) $arrDomainUUIDs = self::getDomainUUIDs();
		$arrWhere['AND']['u.domain_uuid'] = $arrDomainUUIDs;
		$arrWhere['ORDER']['u.domain_uuid'] = 'ASC';
		$objResult = $objFusionConnRead->select(
			'v_users(u)',
			['[>]v_contact_users(cu)'=>['u.user_uuid'=>'user_uuid'],'[>]v_contacts(c)'=>['cu.contact_uuid'=>'contact_uuid']],
			['u.user_uuid', 'u.username', 'c.contact_name_given', 'c.contact_name_family'],
			$arrWhere
		);

		return $objResult;
    }

	public function getPBXUsersAssoc()
	{
		$arrUsers = $this->getPBXUsers();
		$arrReturn = array();
		foreach($arrUsers as &$val)
		{
			$strUsername = trim("{$val['contact_name_given']} {$val['contact_name_family']}");
			if(empty($strUsername)) $strUsername = $val['username'];

			$arrReturn[$val['user_uuid']] = $strUsername;
			unset($strUsername);
		}

		asort($arrReturn);

		return $arrReturn;
	}

	public function getUserIDFromHash($strHash="")
	{
		// Only used to supply agent_id to the reset_password.php page
	   global $objConnRead;
	   return $objConnRead->get("users_forgot_passwords", "agent_id", ["AND" => ["hash" => $strHash, "active" => 1]]);
	}

	public function sendWelcomeEmail($arrData)
	{
		$phpMailer = new PHPMailerChild('Business Phone Welcome Email', [$arrData['email']]);
		$phpMailer->sendWelcomeEmail($arrData);
	}

	public static function getID() { return $_SESSION['user']->user_id; }
	public static function getEmail() { return $_SESSION['user']->email; }
	public static function getName() { return "{$_SESSION['user']->first_name} {$_SESSION['user']->last_name}"; }
	public static function getPhoneNumber() { return $_SESSION['user']->phone_number; }
	public static function getPhoneExtension() { return $_SESSION['user']->phone_extension; }
	public static function getIPAddress() { return $_SERVER['REMOTE_ADDR']; }
	public static function getTags() { return $_SESSION['user']->tags; }
	public static function getDomainUUIDs() { return $_SESSION['company']->domain_uuids; }
	public static function getCompanyID() { return $_SESSION['company']->company_id; }
}
