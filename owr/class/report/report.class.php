<?php
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'util/util.class.php';

use Carbon\Carbon;

class Report
{
	public function __construct($objParams)
	{
		// obj params
		$this->objParams = $objParams;

		// get timezone offset
		$this->strCompanyTimezone = Company::getTimezone();
		$this->objCarbon  = Carbon::today($this->strCompanyTimezone);
		$this->strOffset  = $this->objCarbon->format("P");

		// report date for past 7 days
		$this->strTodayDate = $this->objCarbon->toDateString() . ' 23:59:59';
		$this->strSevenDaysDate = $this->objCarbon->subDays(7)->toDateString() . ' 00:00:00';

		// get Company realms and domain_uuids
		$arrDomains = Company::getAllDomains();
		for($i=0; $i<sizeof($arrDomains); ++$i)
		{
			$this->arrRealms[] = $arrDomains[$i]['domain_name'];
			$this->arrDomainUUIDs[] = $arrDomains[$i]['domain_uuid'];
		}
	}

	public function getTotalsHUD($strQueueName)
	{
		global $objMem;
		require_once CLASS_DIR . 'database/elasticsearch.class.php';

		// limit by domain
		$objQuery->query->bool->must[]->match->domain_uuid = $this->objParams->arrParams['domains'][0];

		// limit by cc_queue
		$objQuery->query->bool->must[]->match->cc_queue = $strQueueName;

		// limit by day
		$strToday 		= Carbon::today($this->strCompanyTimezone)->toDateString();
		$strTomorrow 	= Carbon::tomorrow($this->strCompanyTimezone)->toDateString();
		$objQuery->query->bool->must[]->range->start_epoch = [
			"gte" => strtotime(timezone::convert_to_server_date($strToday, $this->strCompanyTimezone)),
			"lte" => strtotime(timezone::convert_to_server_date($strTomorrow, $this->strCompanyTimezone))
		];

		// Take away sign-in/sign-out calls with # or * at the start of the destination_number
		$objQuery->query->bool->must_not[]->regexp->destination_number = "[#*].*";

		// make sure cc_agent is present
		$objQuery->query->bool->must_not[]->missing->field = "cc_agent";

		// check memcache
		$strMemKey = sha1("getCCAgentsHUD-{$strQueueName}");
		$arrShouldExtensions = $objMem->get($strMemKey);

		// get from fusion and add to memcache
		if(empty($arrShouldExtensions))
		{
			$arrCCAgentData = $this->getCCAgents();
			for($i=0; $i<sizeof($arrCCAgentData); ++$i)
			{
				$strAgent = $arrCCAgentData[$i]['agent_name'] . "@" . $arrCCAgentData[$i]['domain_name'];
				$arrShouldExtensions[]->match->cc_agent = $strAgent;
			}
		}

		// set to memcache
		if($arrShouldExtensions) $objMem->set($strMemKey, $arrShouldExtensions, null, 60*5);

		// filter by this queue's call center agents
		$objQuery->query->bool->must[]->bool->should[] = $arrShouldExtensions;

		// aggregate by direction
		$objQuery->aggs->total_outbound->filter->terms->direction = ['outbound'];
		$objQuery->aggs->total_inbound->filter->term = ['direction' => 'inbound'];

		$objQuery->aggs->total_duration->sum->field = "duration";

		// do not need any documents back
		$objQuery->size = 0;

		// query ES
		$objResults = ElasticSearch::query(ES_INDEX."/cdr", $objQuery, "_search", "GET", false)->aggregations;

		// set array data for view
		$intTotalCalls = $objResults->total_inbound->doc_count + $objResults->total_outbound->doc_count;
		$intTotalInbound = $objResults->total_inbound->doc_count;
		$intTotalOutbound = $objResults->total_outbound->doc_count;

		$arrTotals['total_calls'] 		= $intTotalCalls ? $intTotalCalls : 0;
		$arrTotals['total_inbound'] 	= $intTotalInbound ? $intTotalInbound : 0;
		$arrTotals['total_outbound'] 	= $intTotalOutbound ? $intTotalOutbound : 0;
		$arrTotals['total_talktime']	= util::seconds2human($objResults->total_duration->value);

		return $arrTotals;
	}

	public function getCCAgents()
	{
		require_once INCLUDES_DIR . 'fusion.database.inc.php';
		global $objFusionConnRead;

		$arrCCAgents = $objFusionConnRead->select(
			"v_call_center_agents(cca)",
			[
				"[>]v_domains(d)" => "domain_uuid",
				"[>]v_extensions(e)" => "extension_uuid"
			],
			[
				"cca.call_center_agent_uuid",
				"cca.extension_uuid",
				"d.domain_description",
				"cca.agent_name",
				"d.domain_name",
				"e.extension",
				"cca.agent_status"
			],
			[
				"cca.domain_uuid" => $this->objParams->arrParams['domains']
			]
		);

		return $arrCCAgents;
	}

	public function getRegistrationChartData()
	{
		require_once INCLUDES_DIR . 'freeswitch.database.inc.php';
		require_once INCLUDES_DIR . 'fusion.database.inc.php';

		global $objFreeswitchConnRead, $objFusionConnRead;

		$intFreeswitchRegistrations = $objFreeswitchConnRead->count(
			"registrations",
			[
				"AND" => [
					"realm" => $this->arrRealms,
					"expires[>]" => strtotime("now")
				]
			]
		);

		$intFusionRegistrations = $objFusionConnRead->count(
			"location",
			[
				"AND" => [
					"domain" => $this->arrRealms,
					"expires[>]" => Carbon::createFromTimestamp(strtotime("now"), $this->strCompanyTimezone)->toDateTimeString()
				]
			]
		);

		$intDevices = $objFusionConnRead->count(
			"v_devices",
			[
				"AND" => [
					"domain_uuid" => $this->arrDomainUUIDs,
					"device_enabled" => "true"
				]
			]
		);

		$intDisabled = $objFusionConnRead->count(
			"v_devices",
			[
				"AND" => [
					"domain_uuid" => $this->arrDomainUUIDs,
					"device_enabled" => "false"
				]
			]
		);

		// prevent negative unregistrations
		$intUnregistered = $intDevices - ($intFreeswitchRegistrations + $intFusionRegistrations);
		if ($intUnregistered < 0) $intUnregistered = 0;

		return [
			"registered" 	=> ($intFreeswitchRegistrations + $intFusionRegistrations),
			"unregistered" 	=> $intUnregistered,
			"disabled"		=> $intDisabled
		];
	}

	public function getCallVolumeChartData()
	{
		require_once CLASS_DIR . 'database/elasticsearch.class.php';

		$objQuery = new stdClass();
		$objQuery->size = 0;

		//filter by date range
		$objQuery->query->bool->filter->and[]->range->start_epoch = (object) [
			'lte'	 => timezone::convert_to_server_date($this->strTodayDate, $this->strCompanyTimezone),
		    'gte'    => timezone::convert_to_server_date($this->strSevenDaysDate, $this->strCompanyTimezone),
		    "format" => "yyyy-MM-dd HH:mm:ss"
		];

		// find difference in days
		$intDateDiff = strtotime($this->objParams->arrParams['date_range']['end_date']) - strtotime($this->objParams->arrParams['date_range']['start_date']);
		$intDateDiff = floor($intDateDiff/(60*60*24));

		$objQuery->aggs->call_volume->date_histogram = [
			"field"     => "start_epoch",
			"interval"  => "day",
			"format"    => "MM/dd/yyyy",
			"time_zone" => $this->strOffset
		];

		$objQuery->aggs->call_volume->aggs->call_type->terms->field = "direction";

		$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);

		// parse calls by hour
		for($i=0; $i<sizeof($results->aggregations->call_volume->buckets); ++$i)
		{
			$arrData['call_volume']['time'][]  = $results->aggregations->call_volume->buckets[$i]->key_as_string;

			$arrData['call_volume']['inbound'][$i] 		= 0;
			$arrData['call_volume']['outbound'][$i] 	= 0;
			$arrData['call_volume']['local'][$i]	 	= 0;

			foreach($results->aggregations->call_volume->buckets[$i]->call_type->buckets as $objCallType)
			{
				$arrData['call_volume'][$objCallType->key][$i] = $objCallType->doc_count;
			}
		}

		// json encode them for use in the bar chart
		$arrData['call_volume']['time'] 	= json_encode($arrData['call_volume']['time']);
		$arrData['call_volume']['inbound'] 	= json_encode($arrData['call_volume']['inbound']);
		$arrData['call_volume']['outbound'] = json_encode($arrData['call_volume']['outbound']);
		$arrData['call_volume']['local'] 	= json_encode($arrData['call_volume']['local']);

		return $arrData;
	}

	public function billing($arrParams)
	{
		$objQuery = new stdClass();

		/**
		 * Date Range Filter
		 */
		$objQuery->query->bool->must[]->range->start_epoch = [
			"gte" => strtotime($arrParams['date_range']['start_date']),
			"lte" => strtotime($arrParams['date_range']['end_date'])
		];

		// aggregations code

		// $objQuery->aggs->accountcode->terms->field = "domain_name";

		$objQuery->aggs->accountcode->terms->script = [
			"inline" => "if(!doc['accountcode'].value){ doc['domain_name'].value } else { doc['accountcode'].value }"
		];

		$objQuery->aggs->accountcode->aggs->direction->terms->field = "direction";

		$objQuery->aggs->accountcode->aggs->direction->aggs->total->scripted_metric = [
				"init_script" 		=> "_agg['transactions'] = []",
				"map_script"		=> "
					if (doc['billsec'].value < 30 && doc['billsec'].value > 0)
					{
						_agg.transactions.add(30)
					}
					else if (doc['billsec'].value > 30 && (doc['billsec'].value % 6) == 0)
					{
						_agg.transactions.add(doc['billsec'].value)
					}
					else if (doc['billsec'].value > 30 && (doc['billsec'].value % 6) != 0)
					{
						_agg.transactions.add(doc['billsec'].value + (6 - (doc['billsec'].value % 6)))
					}
					else
					{
						_agg.transactions.add(doc['billsec'].value)
					}
				",
				"combine_script"	=> "total = 0; for (t in _agg.transactions) { total += t }; return total",
				"reduce_script" 	=> "total = 0; for (a in _aggs) { total += a }; return total"
			];

		// Filtered Results
		$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);

		$arrResults = [];

		foreach($results->aggregations->accountcode->buckets as $objAccounts)
		{
			$intTotal = 0;
			foreach($objAccounts->direction->buckets as $objDirection)
			{
				$arrResults[$objAccounts->key][$objDirection->key] = $objDirection->total->value;

				$intTotal = $intTotal + $objDirection->total->value;
			}

			$arrResults[$objAccounts->key]['total_sec'] = $intTotal;
		}

		return $arrResults;

	}
}
