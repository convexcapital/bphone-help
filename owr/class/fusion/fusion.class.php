<?php

require_once INCLUDES_DIR . 'fusion.database.inc.php';
require_once CLASS_DIR . 'event_socket/event_socket.class.php';

class Fusion {
    public static function getServers()
    {
		global $smarty, $objMem, $objFusionConnRead;

		$strKey = sha1("fusion-servers");
        $objMem->delete($strKey);
		$arrData = $objMem->get($strKey);

		if(empty($arrData))
		{
			$arrData = $objFusionConnRead->select('v_default_settings(ds)',['default_setting_value(server)'], [
				'AND' => [
			        'ds.default_setting_category' => 'servers',
			    ],
                'ORDER' =>  'ds.default_setting_value'
			]);

			$objMem->set($strKey, $arrData, null, 60*10);
		}

		if(!empty($strSmartyKey) && is_object($smarty)) $smarty->assign($strSmartyKey, $arrData);

		return $arrData;
    }

    public static function getConnection($arrCreds = array())
	{
		$strAddress = $arrCreds['event_socket_ip_address'] ? $arrCreds['event_socket_ip_address'] : '127.0.0.1';
		$intPort = $arrCreds['event_socket_port'] ? $arrCreds['event_socket_port'] : '8021';
		$strPassword = $arrCreds['event_socket_password'] ? $arrCreds['event_socket_password'] : '2ET9bwc5CuKN9M';
		return self::event_socket_create($strAddress, $intPort, $strPassword);
	}

    public static function event_socket_create($host, $port, $password) {
    	$esl = new event_socket;
    	if ($esl->connect($host, $port, $password)) {
    		return $esl->reset_fp();
    	}
    	return false;
    }

    public static function eventSocketMulti($varCommand = '')
	{
		$arrResponse = array();
		$arrSwitches = self::getServers();

		if(!empty($arrSwitches))
		{
            for($i = 0; $i < sizeof($arrSwitches); ++$i)
            {
                $arrSwitch = $arrSwitches[$i];

				$fp = self::getConnection(array('event_socket_ip_address'=>$arrSwitch['server']));
				if(is_array($varCommand))
				{
					foreach($varCommand as &$strCommand)
					{
						$arrResponse[$arrSwitch['server']][] = event_socket_request($fp, $strCommand);
					}
				}
				else {
					$arrResponse[$arrSwitch['server']] = event_socket_request($fp, $varCommand);
				}
			}
		}
		return $arrResponse;
	}
}
