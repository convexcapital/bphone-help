<?php
error_reporting(E_ERROR);
ini_set("display_errors", 1);

//make all system dates in UTC
date_default_timezone_set('UTC');

session_start();
$_SESSION['current_file'] = $_SERVER['SCRIPT_NAME'];
$_SERVER['DOCUMENT_ROOT']  =  rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/'; # make consistent with trailing slash regardless of setup

DEFINE('ROOT', $_SERVER['DOCUMENT_ROOT']);
DEFINE('CLASS_DIR', ROOT . '../owr/class/');
DEFINE('INCLUDES_DIR', ROOT . '../owr/includes/');
DEFINE('TEMPLATE_DIR', ROOT . '../owr/template/');
DEFINE('TEMPLATE_C_DIR', ROOT . '../owr/template/template_c/');
DEFINE('TEMP_FILES', ROOT . '../temporary/');
DEFINE('PLUGIN_DIR', ROOT . '../owr/plugins/');
DEFINE('VENDOR_DIR', ROOT . '../vendor/');
DEFINE('CRON_DIR', ROOT . '../owr/cron/');

DEFINE('AWS_CRED_ID', 'AKIAJNGWVPCGIFCP24XA');
DEFINE('AWS_CRED_KEY', 'zZmHO0c7pL765p0x67J5EQi5LIRQi6Th9n4JMj5d');
DEFINE('AWS_CRED_BUCKET', 'engage-asr-storage');
DEFINE('AWS_CRED_ENC_KEY_ID', '4bec24b4-1ca8-41f8-86af-60fcdc787a18');

//probably not working because of load balancer protocol delivery
if(@$_SERVER['HTTPS'] == 'on') $strProtocol = 'https://';
else $strProtocol = 'http://';
$strServer = $strProtocol . $_SERVER['HTTP_HOST'];
DEFINE('SERVER_NAME', $strServer);

DEFINE('OSTICKET_API_KEY', 'B7E452F8657DEAB397C03388FE266CA6');

//used in number class
DEFINE('TWILIO_SID', 'ACf85fac7857cdcf3e4e70f72c9477dbd9');
DEFINE('TWILIO_TOKEN', '73dc12399ae4e0042abaadb572b36fa6');

DEFINE('ES_INDEX', 'bphone');

############# COMPOSER AUTOLOADER
require_once VENDOR_DIR . 'autoload.php';
#################################

require_once INCLUDES_DIR . "smarty.inc.php";
require_once INCLUDES_DIR . "database.inc.php";
require_once INCLUDES_DIR . "memcache.inc.php";
require_once CLASS_DIR . "user/user.class.php";

require_once CLASS_DIR . 'params/params.class.php';
$objParams = new Params();
