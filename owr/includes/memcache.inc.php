<?php
global $objMem;
$objMem = new Memcache();
if (ENVIRONMENT_LEVEL == 'DEVELOPMENT')
{
	$objMem->addServer('localhost', 11211);
}
elseif (ENVIRONMENT_LEVEL == 'STAGING')
{
	$objMem->addServer('prod-cache-01.engagertc.net', 11211);
	$objMem->addServer('prod-cache-02.engagertc.net', 11211);
}
else
{
	$objMem->addServer('prod-cache-01.engagertc.net', 11211);
	$objMem->addServer('prod-cache-02.engagertc.net', 11211);
}
