<?php
global $smarty;
$smarty = new Smarty();
/* setup smarty dirs */
$smarty->template_dir = TEMPLATE_DIR;
$smarty->compile_dir = TEMPLATE_C_DIR;

$arrPluginsDir = $smarty->getPluginsDir();
$arrPluginsDir[] = PLUGIN_DIR . '/smarty/';
$smarty->setPluginsDir($arrPluginsDir);

DEFINE('DISPLAY_DATETIME_1', '%m/%d/%y %l:%M %p');
DEFINE('DISPLAY_DATE_1', '%m/%d/%Y');
DEFINE('DISPLAY_TIME_1', '%l:%M %P');