<?php
/*
* Smarty plugin
* -------------------------------------------------------------
* File: modifier.format_phone.php
* Type: modifier
* Name: format_phone
* Purpose: format a 10-digit phone number
* -------------------------------------------------------------
*/


function smarty_modifier_format_phone($number, $format="(%s) %s-%s") {
	$number = preg_replace("/\D/","",$number);
	if (strlen($number) != 10) return $number;
		$number = sprintf(
		$format,
		substr($number,0,3),
		substr($number,3,3),
		substr($number,6,4)
	);
	return $number;
}