<?php
/*
* Smarty plugin
* -------------------------------------------------------------
* File: modifier.format_seconds.php
* Type: modifier
* Name: format_seconds
* Purpose: seconds2human function in Util class
* -------------------------------------------------------------
*/


function smarty_modifier_format_seconds($ss) {
	$s = $ss%60;
	$m = floor(($ss%3600)/60);
	$h = floor(($ss%86400)/3600);
	$d = floor(($ss%2592000)/86400);
	$M = floor($ss/2592000);

	if(strlen($s)=="1") $s="0".$s;
	if(strlen($m)=="1") $m="0".$m;
	if(strlen($h)=="1") $h="0".$h;
	if(strlen($d)=="1") $d="0".$d;

	$strTime = "$d:$h:$m:$s";
	if($d == "00"){
		$strTime = substr($strTime, 3);
	}

	return $strTime;
}