<?php

namespace Facebook\WebDriver;

require_once('vendor/autoload.php');

use PHPUnit\Framework\TestCase;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;

class webTest extends TestCase
{
	protected $webDriver;
	protected $baseUrl 		= "http://dmcdaniel.help.bphone.io/";
	protected $host 		= "http://localhost:4444/wd/hub";
	protected $username 	= "dmcdaniel@iqventures.com";
	protected $strErrorText = "You have used 1 attempts out of 5. Your account will be locked out after 5 attempts.";
	protected $urlJson 		= "/testing/selenium/urls.json";

	public function setUp()
	{
		$objOptions = new ChromeOptions();

		$objOptions->addArguments(array(
		  '--window-size=1600,1600',
		));

		$desired_capabilities = DesiredCapabilities::chrome();
		$desired_capabilities->setCapability(ChromeOptions::CAPABILITY, $objOptions);

		$this->webDriver = RemoteWebDriver::create($this->host, $desired_capabilities);
		$this->webDriver->manage()->window()->maximize();
		$this->webDriver->get($this->baseUrl);
	}

	public function testSystem()
	{
		$this->failed_login();
		$this->successful_login();
		$this->validate_dashboard_charts();
		$this->validate_pages();
		$this->logout();
	}

	public function failed_login()
	{
		$strSignInText = strtolower($this->webDriver->findElement(WebDriverBy::className('panel-title'))->getText());

		$this->assertTrue($strSignInText === "please sign in");

		// Assert failed login here
		$username = $this->webDriver->findElement(WebDriverBy::name('email'));
		$username->click();

		$this->webDriver->getKeyboard()->sendKeys($this->username);

		$password = $this->webDriver->findElement(WebDriverBy::name('password'));
		$password->click();

		$this->webDriver->getKeyboard()->sendKeys('1234');

		$this->webDriver->getKeyboard()->pressKey(WebDriverKeys::ENTER);

		sleep(4);

		$strErrorText = $this->webDriver->findElement(WebDriverBy::className('help-block'))->getText();

		$this->assertTrue($strErrorText === $this->strErrorText);

		// test User was not found.
		$username = $this->webDriver->findElement(WebDriverBy::name('email'));
		$username->click();

		$this->webDriver->getKeyboard()->sendKeys('test@test1234974.com');

		$password = $this->webDriver->findElement(WebDriverBy::name('password'));
		$password->click();

		$this->webDriver->getKeyboard()->sendKeys('1234');

		$this->webDriver->getKeyboard()->pressKey(WebDriverKeys::ENTER);

		sleep(4);

		$strErrorText = $this->webDriver->findElement(WebDriverBy::className('help-block'))->getText();

		$this->assertTrue($strErrorText === "User was not found.");
	}

	public function successful_login()
	{
		$this->webDriver->getKeyboard()->sendKeys($this->username);

		$password = $this->webDriver->findElement(WebDriverBy::name('password'));
		$password->click();

		$this->webDriver->getKeyboard()->sendKeys('VLocis4ME!');

		$this->webDriver->getKeyboard()->pressKey(WebDriverKeys::ENTER);

		$intLocations 	= $this->webDriver->findElement(
			WebDriverBy::cssSelector('.panel-green:first-child .panel-heading .row .col-xs-9.text-right .huge'))->getText();
		$intUsers 		= $this->webDriver->findElement(
			WebDriverBy::cssSelector('.panel-red:first-child .panel-heading .row .col-xs-9.text-right .huge'))->getText();

		$this->assertTrue($intLocations >= 3);
		$this->assertTrue($intUsers 	>= 3);

		sleep(2);
	}

	public function validate_dashboard_charts()
	{
		// validate bar chart
		$arrBarrResults = $this->webDriver->executeScript("return barData.labels");

		$this->assertTrue(is_array($arrBarrResults));

		// validate pie chart
		$arrPieResults = $this->webDriver->executeScript("return pieData.labels");

		$this->assertTrue(is_array($arrPieResults));
	}

	public function validate_pages()
	{
		$strObject = json_decode(file_get_contents(getcwd().$this->urlJson), true)['urls'];

		foreach($strObject as $arrUrl)
		{
			$this->goToUrl($arrUrl);
		}
	}

	public function goToUrl($url)
	{
		$this->webDriver->navigate()->to($this->baseUrl . $url);
		$logo = $this->webDriver->findElement(WebDriverBy::cssSelector(".side-header a img"));
		$this->assertTrue(count($logo) > 0);
		sleep(1);
	}

	public function logout()
	{
		$this->webDriver->findElement(WebDriverBy::cssSelector('.fa-sign-out'))->click();
	}

}
