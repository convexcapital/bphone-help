<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'database/elasticsearch.class.php';
require_once CLASS_DIR . 'filter/filter.class.php';
require_once CLASS_DIR . 'location/location.class.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'paging/paging.class.php';
require_once CLASS_DIR . 'skill/skill.class.php';
require_once CLASS_DIR . 'util/fusion_cdr_format.class.php';
require_once CLASS_DIR . 'util/util.class.php';

Navigation::set('call_center','production_report');

/**
 * Params
 */
if($_POST)
{
	$objParams->setParams([
		'date_range' => [
 			'start_date'     	=> timezone::convert_to_server_date($_POST['start_date'] . ' 00:00:00', Company::getTimeZone()),
 			'end_date'       	=> timezone::convert_to_server_date($_POST['end_date'] . ' 23:59:59', Company::getTimeZone()),
            'user_timezone'  	=> Company::getTimeZone()
 		],
		'page' => [
			'size'				=> $_POST['size'],
			'from'				=> $_POST['page']
		],
		'sort' => [
			'field'				=> $_POST['field'],
			'order'				=> $_POST['order']
		],
		'cc_queue'				=> ''
	]);

	if($_POST['blnSkills']) $objParams->setParams(['cc_queue' => $_POST['skills']]);
}

// get page parameters
$arrParams = $objParams->getAllParams();

/**
 * ElasticSearch Query
 */
$objQuery = new stdClass();
// Result Size
$objQuery->size = 0;

// Paging
$objQuery->from = ($arrParams['page']['from'] - 1) * $arrParams['page']['size'];

// Sorting
$objQuery->sort->{$arrParams['sort']['field']}->order = $arrParams['sort']['order'];

/**
 * Take away sign-in/sign-out calls with # or * at the start of the destination_number
 */
$objQuery->query->bool->must_not[]->regexp->destination_number = "[#*].*";


/**
 * Date Range Filter
 */
$objQuery->query->bool->must[]->range->start_epoch = [
	"gte" => strtotime($arrParams['date_range']['start_date']),
	"lte" => strtotime($arrParams['date_range']['end_date'])
];

$objQuery->query->bool->must_not[]->term->sip_hangup_disposition = "send_refuse";

$objQuery->query->bool->must_not[]->missing = [
		"field"			=> "cc_agent",
		"existence"		=> true,
		"null_value"	=> false
];

/**
 * Skills Filter
 */
if(!empty($arrParams['cc_queue']))
{
	for($i=0; $i<sizeof($arrParams['cc_queue']); ++$i)
	{
		$arrShouldSkills[]->match->cc_queue = $arrParams['cc_queue'][$i];
	}
	$objQuery->query->bool->must[]->bool->should[] = $arrShouldSkills;
}

// aggregations code
$objQuery->aggs->date->date_histogram = [
		"field"  	=> "start_epoch",
		"interval"	=> "day"
];

$objQuery->aggs->date->aggs->by_agent->terms = [
	'field' => "cc_agent",
	'size'	=> 2000
];

// agg by cc_extension here
$objQuery->aggs->date->aggs->by_agent->aggs->extension->terms->field = "cc_extension";
// cc_queue  == skill
$objQuery->aggs->date->aggs->by_agent->aggs->extension->aggs->skill->terms->field = "cc_queue";

$objQuery->aggs->date->aggs->by_agent->aggs->extension->aggs->skill->aggs->direction_count->terms->field = "direction";

// Filtered Results
$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);

$arrData = [];
// Format Results For CDR View
for($i=0; $i<sizeof($results->aggregations->date->buckets); ++$i)
{
	$date = date("Y-m-d", strtotime($results->aggregations->date->buckets[$i]->key_as_string));

	foreach($results->aggregations->date->buckets[$i]->by_agent->buckets as $objAgent)
	{
		foreach($objAgent->extension->buckets as $objExtension)
		{
			foreach($objExtension->skill->buckets as $objSkill)
			{
				$arrData[$objAgent->key . '-' . $date][$objSkill->key] = [
					"agent"			=> explode('@',$objAgent->key)[0],
					"extension"		=> $objExtension->key,
					"inbound"		=> 0,
					"outbound"		=> 0,
					"call_group"	=> explode('@',$objSkill->key)[0],
					"date"			=> $date, true
				];
				foreach($objSkill->direction_count->buckets as $objDirection)
				{
					$arrData[$objAgent->key . '-' . $date][$objSkill->key][$objDirection->key] = $objDirection->doc_count;
				}
			}
		}
	}
}

asort($arrData);

$objFilter = new Filter([
	'blnDates',
	'blnSkills'
]);

$arrPageTitle = [
	'Reporting'	=> '',
	'Production Reporting Records' => '/report_production.php'
];

$smarty->assign('arrData', $arrData);
$smarty->assign('arrParams', $arrParams);
$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->assign('blnFilter',true);
$smarty->display('report-production.html');
