<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'database/elasticsearch.class.php';
require_once CLASS_DIR . 'filter/filter.class.php';
require_once CLASS_DIR . 'location/location.class.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'util/util.class.php';
require_once CLASS_DIR . 'report/report.class.php';

Navigation::set('call_center','cc_location_summary');

/**
 * Params
 */
if($_POST)
{
	$objParams->setParams([
		'date_range' => [
			'start_date'     	=> timezone::convert_to_server_date($_POST['start_date'] . ' 00:00:00', Company::getTimeZone()),
			'end_date'       	=> timezone::convert_to_server_date($_POST['end_date'] . ' 23:59:59', Company::getTimeZone()),
	    	'user_timezone'  	=> Company::getTimeZone()
		],
		'locations' 			=> isset($_POST['locations']) ? $_POST['locations'] : "",
		'page' => [
			'size'				=> $_POST['size'],
			'from'				=> $_POST['page']
		],
		'sort' => [
			'field'				=> $_POST['field'],
			'order'				=> $_POST['order']
		],
		'phone' => [
			'type'				=> $_POST['phone_type'],
			'number'			=> $_POST['phone_number']
		],
		'domains'				=> $_POST['domains'] ? $_POST['domains'] : Company::getDomainUUIDs(),
		'cc_queue'				=> $_POST['skills'],
		'call_group'			=> $_POST['call_group']
	]);
}

 // get page parameters
 $arrParams = $objParams->getAllParams();

/**
 * ElasticSearch Query
 */
$objQuery = new stdClass();
// Set Default Size To 0, Results Not Needed
$objQuery->size = 0;

/**
 * Take away sign-in/sign-out calls with # or * at the start of the destination_number
 */
$objQuery->query->bool->must_not[]->regexp->destination_number = "[#*].*";

/**
 * Date Range Filter
 */
$objQuery->query->bool->must[]->range->start_epoch = [
	"gte" => strtotime($arrParams['date_range']['start_date']),
	"lte" => strtotime($arrParams['date_range']['end_date'])
];

/**
 * Location UUID Filter
 */
if(!empty($arrParams['locations']))
{
	for($i=0; $i<sizeof($arrParams['locations']); ++$i)
	{
		$arrShould[]->match->location_uuid = $arrParams['locations'][$i];
	}
	$objQuery->query->bool->must[]->bool->should[] = $arrShould;
}

/**
 * Call Group Filter
 */
if(!empty($arrParams['call_group']))
{
	$objQuery->query->bool->must[]->match->call_group = $arrParams['call_group'];
}

/**
 * Skills Filter
 */
if(!empty($arrParams['cc_queue']))
{
	for($i=0; $i<sizeof($arrParams['cc_queue']); ++$i)
	{
		$arrShouldSkills[]->match->cc_queue = $arrParams['cc_queue'][$i];
	}
	$objQuery->query->bool->must[]->bool->should[] = $arrShouldSkills;
}

/*
 * CC Agent Filter - based on cc_agent
 */
$objReport = new Report($objParams);
$arrCCAgentData = $objReport->getCCAgents();
for($i=0; $i<sizeof($arrCCAgentData); ++$i)
{
	$strAgent = $arrCCAgentData[$i]['agent_name'] . "@" . $arrCCAgentData[$i]['domain_name'];

	if(empty($arrParams['cc_agent'])) $arrShouldExtensions[]->match->cc_agent = $strAgent;

	// keep array for formatting data after ES query
	$arrCCAgents[$strAgent] = $arrCCAgentData[$i]['agent_name'];
}

// cc_agent Filter
if($arrParams['cc_agent'])
{
	for($i=0; $i<sizeof($arrParams['cc_agent']); ++$i)
	{
		$arrShouldExtensions[]->match->cc_agent = $arrParams['cc_agent'][$i];
	}
}

$objQuery->query->bool->must[]->bool->should[] = $arrShouldExtensions;

/**
 * By Location Aggregations
 */
$objQuery->aggs->by_location->terms = [
	'field' => 'location_uuid',
	'size' => 1000000
];

/**
 * Total Inbound and Average
 */
$objQuery->aggs->by_location->aggs->total_inbound->filter->term = ['direction' => 'inbound'];
$objQuery->aggs->by_location->aggs->total_inbound->aggs->avg_duration->avg = ['field' => 'duration'];

/**
 * Total Outbound and Average
 */
$objQuery->aggs->by_location->aggs->total_outbound->filter->terms->direction = ['outbound'];
$objQuery->aggs->by_location->aggs->total_outbound->aggs->avg_duration->avg = ['field' => 'duration'];

$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);
$arrData = $results->aggregations->by_location->buckets;

/**
 * Format average calls per day
 */
$date1 = new DateTime($arrParams['date_range']['start_date']);
$date2 = new DateTime($arrParams['date_range']['end_date']);
$diff  = $date2->diff($date1)->format("%a");
for($i=0; $i<sizeof($arrData); ++$i)
{
    $arrData[$i]->total_calls = $arrData[$i]->total_inbound->doc_count + $arrData[$i]->total_outbound->doc_count;
    $arrData[$i]->avg_calls_day = ceil($arrData[$i]->total_calls / $diff);

	// get location name
	$objLocation = Location::getOneByUUID($arrData[$i]->key);
	if($objLocation->number) $arrData[$i]->location_name = $objLocation->number;
}

$objFilter = new Filter([
	'blnDates',
	'blnLocations',
	'blnDomains',
	'blnSkills',
	'blnCallGroup'
]);

$arrPageTitle = [
	'Call Center'	=> '',
	'Location Summary' => '/report_cc_location_summary.php'
];

$smarty->assign('arrDomains', Company::getAllDomains());
$smarty->assign('arrLocationsSet',$objParams->arrParams['locations']);
$smarty->assign('arrData', $arrData);
$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->assign('blnFilter',true);

$smarty->assign('arrParams', $arrParams);
$smarty->display('report-cc-location-summary.html');
