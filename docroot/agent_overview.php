<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';

require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'filter/filter.class.php';
require_once CLASS_DIR . 'report/report.class.php';

Navigation::set('call_center','agent_overview');

/**
 * Params
 */
if($_POST)
{
	$objParams->setParams([
		'domains'				=> $_POST['domains'] ? $_POST['domains'] : Company::getDomainUUIDs(),
	]);
}


$objFilter = new Filter([
	'blnDomains',
]);

$objReport = new Report($objParams);
$smarty->assign('arrAgents', $objReport->getCCAgents());
$smarty->assign('arrDomains', Company::getAllDomains());
$smarty->assign('blnFilter',true);
$smarty->display('agent-overview.html');
