<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR.'number/number.class.php';

Navigation::set('numbers');

$objNumbers = new Number();
$smarty->assign('arrNumbers', $objNumbers->getAll());

$arrPageTitle = [
	'Manage Numbers' => '/manage_numbers.php'
];

$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->display('manage-numbers.html');
