<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR.'location/location.class.php';

Navigation::set('locations');


if($_POST)
{
    /*
    We are not going to let anyone edit locations through the UI for now - Steven @ 11/16/16
    try
    {
        $_GET['id'] = Location::save($_GET['id'], $_POST);
        $smarty->assign('strSuccess', "Location ({$_POST['number']}) updated successfully!");
        Location::getAll(Company::getID(), 'arrLocations');
        die($smarty->display('manage-locations.html'));
    }

    catch(Exception $e)
    {
        $smarty->assign('strError', $e->getMessage());
    }
    */
}

Location::getOne($_GET['id'], 'objLocation');

if($_GET['id']) $strID = '?id=' . $_GET['id'];

$arrPageTitle = [
	'Manage Locations' 	=> '/manage_locations.php',
	'Edit Location'		=> '/edit_location.php' . $strID
];

$smarty->assign('arrPageTitle',$arrPageTitle);

$smarty->display('edit-location.html');
