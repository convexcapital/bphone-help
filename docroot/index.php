<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'ticket/ticket.class.php';
require_once CLASS_DIR . 'location/location.class.php';
require_once CLASS_DIR . 'report/report.class.php';

use Carbon\Carbon;

Navigation::set('dashboard');

$objUser = new User();

if($_GET['action'] == 'logout')
{
	$objUser->logout();
	die(header('Location: /'));
}

// attempt login
if($_POST && !$_SESSION['logged_in']) $objUser->login($_POST['email'], $_POST['password']);

// verify login
$objUser->isLoggedIn();

Ticket::getCount('', 'intOpenTickets');
Location::getCount('', 'intLocations');
User::getCount('', 'intUsers');

$arrPageTitle = [
	'Dashboard' => '/'
];

$objReport = new Report();
$smarty->assign('arrRegistrationData', $objReport->getRegistrationChartData());
$smarty->assign('arrCallVolumeData', $objReport->getCallVolumeChartData());
$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->display('dashboard.html');
