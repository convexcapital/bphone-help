<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR.'location/location.class.php';

Navigation::set('locations');

Location::getAll(Company::getID(), 'arrLocations');

$arrPageTitle = [
	'Manage Locations' => '/manage_locations.php'
];

$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->display('manage-locations.html');
