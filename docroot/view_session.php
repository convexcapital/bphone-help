<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';

Navigation::set('admin','view_session');

$arrPageTitle = [
	'View Session' => ''
];

$smarty->assign('arrPageTitle',$arrPageTitle);

$smarty->display('view-session.html');
