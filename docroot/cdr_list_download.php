<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'database/elasticsearch.class.php';
require_once CLASS_DIR . 'location/location.class.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'util/fusion_cdr_format.class.php';
require_once CLASS_DIR . 'util/util.class.php';

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=cdr_list.csv');



if($_POST)
{
	$objParams->setParams([
		'date_range' => [
 			'start_date'     	=> timezone::convert_to_server_date($_POST['start_date'] . ' 00:00:00', Company::getTimeZone()),
 			'end_date'       	=> timezone::convert_to_server_date($_POST['end_date'] . ' 23:59:59', Company::getTimeZone()),
            'user_timezone'  	=> Company::getTimeZone()
 		],
		'locations' 			=> isset($_POST['locations']) ? $_POST['locations'] : "",
		'page' => [
			'size'				=> $_POST['size'],
			'from'				=> $_POST['page']
		],
		'sort' => [
			'field'				=> $_POST['field'],
			'order'				=> $_POST['order']
		],
		'phone' => [
			'type'				=> $_POST['phone_type'],
			'number'			=> $_POST['phone_number']
		]
	]);
}

$objParams->arrParams['page']['from'] = 1;
$objParams->arrParams['page']['size'] = 10000;

// get page parameters
$arrParams = $objParams->getAllParams();

/**
 * ElasticSearch Query
 */
$objQuery = new stdClass();
// Result Size
$objQuery->size = $arrParams['page']['size'];

// Paging
$objQuery->from = ($arrParams['page']['from'] - 1) * $arrParams['page']['size'];

// Sorting
$objQuery->sort->{$arrParams['sort']['field']}->order = $arrParams['sort']['order'];

/**
 * Date Range Filter
 */
$objQuery->query->bool->must[]->range->start_epoch = [
	"gte" => strtotime($arrParams['date_range']['start_date']),
	"lte" => strtotime($arrParams['date_range']['end_date'])
];

/**
 * Phone Number Filter
 * (source OR destination) = (phone number)
 */
if($arrParams['phone']['number'])
{
	if(strlen(preg_replace('![^0-9]!','',$arrParams['phone']['number'])) == 10)
	{
		$objQuery->query->bool->should[]->wildcard->destination_number = '*' . preg_replace('![^0-9]!','',$arrParams['phone']['number']);
		$objQuery->query->bool->should[]->wildcard->caller_id_number   = '*' . preg_replace('![^0-9]!','',$arrParams['phone']['number']);
		$objQuery->query->bool->minimum_should_match = 1;
	}
	else
	{
		$objQuery->query->bool->should[]->term->destination_number = preg_replace('![^0-9]!','',$arrParams['phone']['number']);
		$objQuery->query->bool->should[]->term->caller_id_number   = preg_replace('![^0-9]!','',$arrParams['phone']['number']);
		$objQuery->query->bool->minimum_should_match = 1;
	}
}

/**
 * Direction Filter
 * (Inbound/Outbound)
 */
if($arrParams['phone']['type'])
{
	$objQuery->query->bool->must[]->match->direction = $arrParams['phone']['type'];
}

/**
 * Location UUID Filter
 */
if(!empty($arrParams['locations']))
{
	for($i=0; $i<sizeof($arrParams['locations']); ++$i)
	{
		$arrShouldLocations[]->match->location_uuid = $arrParams['locations'][$i];
	}
	$objQuery->query->bool->must[]->bool->should[] = $arrShouldLocations;
}

/**
 * Account Code Filter
 */
if(!empty($arrParams['accountcodes']))
{
	for($i=0; $i<sizeof($arrParams['accountcodes']); ++$i)
	{
		$arrShouldAccountCodes[]->match->accountcode = $arrParams['accountcodes'][$i];
	}
	$objQuery->query->bool->must[]->bool->should[] = $arrShouldAccountCodes;
}

/**
 * Extensuion UUID Filter
 */
if(!empty($arrParams['extensions']))
{
	for($i=0; $i<sizeof($arrParams['extensions']); ++$i)
	{
		$arrShouldExtensions[]->match->extension_uuid = $arrParams['extensions'][$i];
	}
	$objQuery->query->bool->must[]->bool->should[] = $arrShouldExtensions;
}

// Filtered Results
$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);

// Format Results For CDR View
for($i=0; $i<sizeof($results->hits->hits); ++$i)
{
	$objCDR = new FusionCDRFormat($results->hits->hits[$i]->_source);
	$arrData[$i] = $objCDR->objCDRFormatted;
}

// add to csv
$objOutput = fopen('php://output', 'w');

fputcsv($objOutput, array('CID Name', 'Source', 'Destination','DateTime Start','TTA','Duration','PDD','MOS','Hangup Cause'));

foreach($arrData as $objData)
{
	$arrDataNew = (array) $objData;
	unset($arrDataNew['uuid']);
	unset($arrDataNew['direction']);
	unset($arrDataNew['issues']);
	unset($arrDataNew['direction_icon']);
	unset($arrDataNew['direction_text']);

	fputcsv($objOutput, (array) $arrDataNew);
}
