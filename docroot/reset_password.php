<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once CLASS_DIR . 'authuser/authuser.class.php';

// if($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'http') die(header("location: https://{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}"));
// if( ! session_id() ) session_start();

$smarty->assign('signed_out', true);
$smarty->assign('strHeaderMsg', 'Reset your password.');

$objUser = new User(false);

$AuthUser = new AuthUser('', [], $objUser->getUserIDFromHash($_GET['id']));
$objPasswordRequest = $AuthUser->verifyPasswordRequest($_GET['id']);

$arrRequirements = $AuthUser->getPasswordRequirements();

if( ! $objPasswordRequest->agent_id) die(header("Location: forgot_password.php"));

if($_POST)
{
    $arrPasswords = [
        0 => $_POST['password1'],
        1 => $_POST['password2']
    ];

    $AuthUser->arrPasswords = $arrPasswords;
    $AuthUser->intAgentID   = $objPasswordRequest->agent_id;

    if($AuthUser->resetPassword($objPasswordRequest->forgot_password_id, $objPasswordRequest->password_num))
    {
		// unset temp lockout message on signin page if auto-login failed
		unset($_SESSION['strTempLockoutMsg']);

		// attempt to login
		$objUser->login($AuthUser->strUsername, $AuthUser->arrPasswords[0]);

        //redirect
        $_SESSION['reset_password'] = false;
        die(header("location: /"));
    }
}

#append general password info
$strMessage .= "<ul class='help'><li class='li-header'>Password must contain all of the following</li>";
if($arrRequirements['upper']) $strMessage .= "<li class='text-danger' id='pwUpper'> - at least 1 uppercase character (A-Z) </li>";
if($arrRequirements['lower']) $strMessage .= "<li class='text-danger' id='pwLower'> - at least 1 lowercase character (a-z) </li>";
if($arrRequirements['digit']) $strMessage .= "<li class='text-danger' id='pwDigit'> - at least 1 digit (0-9) </li>";
if($arrRequirements['special']) $strMessage .= "<li class='text-danger' id='pwSpecial'> - at least 1 special character (punctuation, spaces) </li>";
if($arrRequirements['repeat']) $strMessage .= "<li class='text-danger' id='pwIdentical'> - no more than 2 identical characters in a row (e.g. 111 )</li>";
$strMessage .= "
<li class='text-danger' id='pwLength'> - at least {$arrRequirements['minLength']} characters long </li>
<li class='li-footer'>and cannot match previous {$arrRequirements['historyLength']} password(s)</li>
</ul>";

$smarty->assign('strMessage', $strMessage);
$smarty->display('reset_password.html');
