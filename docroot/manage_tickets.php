<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR .'location/location.class.php';
require_once CLASS_DIR .'ticket/ticket.class.php';

Navigation::set('tickets');

Location::getAll(Company::getID(), 'arrLocations');
Ticket::getAll($_POST, 'arrTickets');
Ticket::getAllStatuses('arrStatuses');
Ticket::getAllPriorities('arrPriorities');

$arrPageTitle = [
	'Manage Tickets' => '/manage_tickets.php'
];

$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->display('manage-tickets.html');
