// filters



$(document).ready(function() {

	$("#filter, #filtersmall").click(function()  {


		if(($(window).width() <= 992) || (navigator.userAgent.match(/iPad/i)) && (navigator.userAgent.match(/iPad/i)!== null)){


				$('body').css('overflow-y', 'hidden');
				$(".filter").fadeIn(function(){
				$(".filter-content").animate({
				"right": "0%"
				},300);

			}

		);

			} else {


				$('body').css('overflow-y', 'hidden');
				$(".filter").fadeIn(function(){
				$(".filter-content").animate({
				"right": "0%"
				},300);

			}

		);

			}
		});

});

$(document).ready(function() {

	$(".filter-back, .filter-close, #close-filters").click(function()  {
		if(($(window).width() <= 992) || (navigator.userAgent.match(/iPad/i)) && (navigator.userAgent.match(/iPad/i)!== null)){
				$('body').css('overflow-y', 'visible');
				$(".filter-content").animate({
				"right": "-100%"
				},300, function(){
					$(".filter").fadeOut(100);
			}
		);
			} else {
				$('body').css('overflow-y', 'visible');
				$(".filter-content").animate({
				"right": "-50%"
				},300, function(){
					$(".filter").fadeOut(100);
				}
		);
			}
		});
});


$(document).ready(function() {
   $('.custom-select').css('color','#999');
   $('.custom-select').change(function() {
      var current = $('.custom-select').val();
      if (current !== 'null') {
          $('.custom-select').css('color','black');
      } else {
          $('.custom-select').css('color','#999');
      }
   });
});


$(document).ready(function(){
	$(".yourchkboxes").click(function() {
		$("#check-expand").toggle();
	});
});


$(document).ready(function(){
	$('.expand').click(function(){

		target_num = $(this).attr('id').split('-')[1];
		content_id = '#expandable-'.concat(target_num);
		target_icon = ("#expand-"+target_num);
		//$(target_icon).find('i').toggleClass('fa fa-angle-down fa fa-angle-up');


		if ($(this).hasClass('active')) {
			$(target_icon).removeClass("active");
			$(content_id).slideToggle(200);
		} else {
			$(".filter-box-content").hide();
			$(".filter-box-heading").removeClass("active");
			$(target_icon).addClass("active");
			$(content_id).slideToggle(200);
		}

	});

	$(".checkbox").click(function(e) {
		e.stopPropagation();
	});

});



$(document).ready(function () {


	$('#start-date').datepicker({
		format: "dd/mm/yyyy"
	});
	$('#end-date').datepicker({
		format: "dd/mm/yyyy"
	});



	$('.input-search').focus(function() {
		$(this).css("color", "white");
	});



	$(document).ready(function(){
		$('#save-new-search').prop('disabled',true);
		$('.input-search').keyup(function(){
			$('#save-new-search').prop('disabled', this.value == "" ? true : false);
		})
	});


	$("#apply-filters").click(function() {
		$(".attention").animate({
			left: '0%',
			width: '100%',
		}, 300);
		$(".filter-content").hide();
	});



	$("#cancel").click(function() {
		$(".attention").animate({
			left: '-100%',
			width: '100%',
		}, 300);
		$(".filter-content").show();
	});



	$("#save-changes").click(function(){
		$(".att2").show();
		$(".att1").hide();
		$(".action2").show();
		$(".action1").hide();
	});



	// save search
	$("#save-search").click(function(e) {
		e.preventDefault();
		$.ajax({

			beforeSend: function(xhr, opts) {
				$('.attention-center').hide();
				$(".loader").show();

			},
			success: function() {

			},
			complete: function() {
				//remove loading gif
			}
		});
	});

});




$(".checkbox").change(function(){
	ok = $(this).closest('.filter-box-heading');
	$(ok).toggleClass("blue");

	ok2 = $(this).closest('.checkbox');
	$(ok2).toggleClass("selected");
});




if ($('input[type="checkbox"]').is(':checked')){
	ok = $('input[type="checkbox"]:checked').closest('.filter-box-heading');
	$(ok).toggleClass("blue");
}



// link from table

$('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
});



$(function () {
  $('[data-toggle="tooltip"]').tooltip({container:'body'})
});

$(function(){ // DOM ready

  // ::: TAGS BOX

  $("#tags input").on({
    focusout : function() {
      var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters
      if(txt) $("<span/>", {text:txt.toLowerCase(), insertBefore:this});
      this.value = "";
    },
    keyup : function(ev) {
      // if: comma|enter (delimit more keyCodes with | pipe)
      if(/(188|13)/.test(ev.which)) $(this).focusout();
    }
  });
  $('#tags').on('click', 'span', function() {
    if(confirm("Remove "+ $(this).text() +"?")) $(this).remove();
  });

});




$( document.body ).on( 'click', '.dropdown-menu li', function( event ) {


   var $target = $( event.currentTarget );

   $target.closest( '.btn-group' )
      .find( '[data-bind="label"]' ).text( $target.text() )
         .end()
      .children( '.dropdown-toggle' ).dropdown( 'toggle' );

	$(".btn-select").css('color', '#5d5d5d');

   return false;



});


// upload link

$("#upload_link").on('click', function(e) {
    e.preventDefault();
    $("#upload:hidden").trigger('click');
});



$(".agent-details li.active").click(function() {

	$(".agent-details li").toggleClass("sub");

});

$(".step-details li.active").click(function() {

	$(".step-details li").toggleClass("sub");

});



$(".custom-tab li.active").click(function(e) {

	$(".custom-tab li").toggleClass("sub");

	e.preventDefault();

});






$(window).bind("load resize", function() {

	if($(window).width() <= 992){
		$(".main-content").addClass("full-content");
		$(".fixed-header").removeClass("fixed-header-wrapped");
		$(".sidebar").addClass("sidebar-hide");
    }

	if($(window).width() >= 992){
		$(".main-content").removeClass("full-content");
		$(".fixed-header").addClass("fixed-header-wrapped");
		$(".sidebar").removeClass("sidebar-hide");
		$(".sidebar").css("width","");
    }


});




// nav toggle

$(".sidebar-toggle").click(function() {


	if($(document).width() <= 992){
		$(".sidebar").css("width", "100%");
	}

	if($(document).width() >= 992){

		$(".sidebar").toggleClass("sidebar-hide");

		$(".main-content").toggleClass("full-content");

		$(".fixed-header").toggleClass("fixed-header-wrapped");

	}

});



// show-modal
$(".show-modal").click(function() {
	$(".modal-main").css("display","block");
});

// close-modal
$(".close-modal").click(function() {
	$(".modal-main").css("display","none");
});


// mobile-toggle
$(".mobile-toggle").click(function() {
	$(".sidebar").css("width","0");
});



$(".sidebar-menu > li.has-sub > a ").click(function() {
	// variable for readability
	var parent = $(this).closest('li.has-sub')

	// Closes li.sub-menu if opening new li.sub-menu
	$(this).closest('.has-sub').siblings().removeClass('active');

	// toggle actives
	if(parent.hasClass('active'))
	{
		parent.removeClass('active');
	}
	else
	{
		parent.addClass('active');
	}
});

$(document).ready(function () {
	$('#reset-filters').click(function(){
		$('#tableFilters input:checkbox').removeAttr('checked');;
		$('#tableFilters').submit();
	});

	/* Filter Code */
	$('select[name="phone_type"]').change(function(){
		enable($(this));
	});

	$('input[name="phone_number"]').keyup(function(){
		enable($(this));
	});

	$('.filter-box-content .custom-checkbox input[type="checkbox"]').click(function(){
		enable($(this));
	});

	function enable(obj)
	{
		$(obj).parents('.filter-box-content').prev().children('.filter-checkbox').children('label').children('input').prop('checked',true);
	}
});
