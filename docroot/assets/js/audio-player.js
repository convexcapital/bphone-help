var objAudioPlayer;
$(function() {
	$.audioPlayer = function(objSettings) {
		this.objSettings = objSettings;
		this.blnPlaying = false;
		this.blnMute = false;
		this.intPercentLoaded = 0;
	};

	$.audioPlayer.prototype.initPlayer = function(type, dualChannel) {
		var options = {
			container: '#wavesurfer',
			barWidth: 3,
			normalize: true,
			fillParent: true,
			backend: 'WebAudio',
			waveColor: '#a0001a',
			height: 100
		}

		if(dualChannel == true)
		{
			options.splitChannels = true;
			options.height = 70;
		}

		this.wavesurfer = WaveSurfer.create(options);
		this.wavesurfer.load(objAudioPlayer.objSettings.strAudioURL);

		$('.percent-loaded div').css('width', '0%');
		$('.percent-loaded').fadeIn('fast');

		this.wavesurfer.on('loading', function (intPercent) {
			if(intPercent % 5 == 0)
			{
				$('.percent-loaded div').css('width', intPercent + '%');
			}
		});
	}

	$.audioPlayer.prototype.setCurrentTime = function(intDuration, objElement) {
		if(intDuration == null) var intDuration = objAudioPlayer.wavesurfer.getCurrentTime();
		objElement.text(this.formatTime(intDuration) + ' |');
	}

	$.audioPlayer.prototype.setDuration = function(objElement) {
		var intDuration = objAudioPlayer.wavesurfer.getDuration();
		objElement.text(this.formatTime(intDuration));
	}

	$.audioPlayer.prototype.formatTime = function(intSeconds) {
		var date = new Date(null);
		date.setSeconds(intSeconds); // specify value for SECONDS here
		return date.toISOString().substr(11, 8);
	}

	$.audioPlayer.prototype.handlePlayAndPause = function(objElement) {
		this.wavesurfer.on('play', function () {
			$('.control-play i').removeClass('fa-play').addClass('fa-pause');
			objAudioPlayer.blnPlaying = true;
		});
		this.wavesurfer.on('pause', function () {
			$('.control-play i').removeClass('fa-pause').addClass('fa-play');
			objAudioPlayer.blnPlaying = false;
		});
		this.wavesurfer.on('finish', function () {
			$('.control-play i').removeClass('fa-pause').addClass('fa-play');
			objAudioPlayer.blnPlaying = false;
		});
		objElement.unbind();
		objElement.click(function() {
			objAudioPlayer.wavesurfer.playPause();
		});
	}

	$.audioPlayer.prototype.handleStartOver = function(objElement) {
		objElement.unbind();
		objElement.click(function() {
			objAudioPlayer.wavesurfer.play(0);
		});
	}

	$.audioPlayer.prototype.unloadPlayer = function() {
		if(this.wavesurfer !== undefined)
		{
			this.wavesurfer.destroy();
		}
	}
})
