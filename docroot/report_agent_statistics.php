<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'database/elasticsearch.class.php';
require_once CLASS_DIR . 'filter/filter.class.php';
require_once CLASS_DIR . 'location/location.class.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'paging/paging.class.php';
require_once CLASS_DIR . 'util/fusion_cdr_format.class.php';
require_once CLASS_DIR . 'util/util.class.php';
require_once CLASS_DIR . 'domain/domain.class.php';
require_once CLASS_DIR . 'report/report.class.php';

Navigation::set('call_center','agent_statistics');

/**
 * Params
 */
if($_POST)
{
	$objParams->setParams([
		'date_range' => [
 			'start_date'     	=> timezone::convert_to_server_date($_POST['start_date'] . ' 00:00:00', Company::getTimeZone()),
 			'end_date'       	=> timezone::convert_to_server_date($_POST['end_date'] . ' 23:59:59', Company::getTimeZone()),
            'user_timezone'  	=> Company::getTimeZone()
 		],
		'cc_agent'				=> $_POST['agents'] ? $_POST['agents'] : ''
	]);
}

// get page parameters
$arrParams = $objParams->getAllParams();

/**
 * ElasticSearch Query
 */
$objQuery = new stdClass();
$objQuery->size = 0;

/**
 * Date Range Filter
 */
$objQuery->query->bool->must[]->range->start_epoch = [
	"gte" => strtotime($arrParams['date_range']['start_date']),
	"lte" => strtotime($arrParams['date_range']['end_date'])
];

/*
 * CC Agent Filter - based on cc_agent
 */
$objReport = new Report($objParams);
$arrCCAgentData = $objReport->getCCAgents();
for($i=0; $i<sizeof($arrCCAgentData); ++$i)
{
	$strAgent = $arrCCAgentData[$i]['agent_name'] . "@" . $arrCCAgentData[$i]['domain_name'];

	if(empty($arrParams['cc_agent'])) $arrShouldExtensions[]->match->cc_agent = $strAgent;

	// keep array for formatting data after ES query
	$arrCCAgents[$strAgent] = $arrCCAgentData[$i]['agent_name'];
}

// cc_agent Filter
if($arrParams['cc_agent'])
{
	for($i=0; $i<sizeof($arrParams['cc_agent']); ++$i)
	{
		$arrShouldExtensions[]->match->cc_agent = $arrParams['cc_agent'][$i];
	}
}

$objQuery->query->bool->must[]->bool->should[] = $arrShouldExtensions;

// by agent
$objQuery->aggs->by_agent->terms = [
	"field" => "cc_agent",
	"size" => 10000
];

// total talk time
$objQuery->aggs->by_agent->aggs->total_talk_time->sum->field = "duration";

// total dispositions
$objQuery->aggs->by_agent->aggs->dispositions->terms->field = "hangup_cause";

// outbound
$objQuery->aggs->by_agent->aggs->total_outbound->filter->terms->direction = ['outbound', 'local'];
$objQuery->aggs->by_agent->aggs->total_outbound->aggs->talk_time->sum->field = "duration";

// inbound
$objQuery->aggs->by_agent->aggs->total_inbound->filter->term = ['direction' => 'inbound'];
$objQuery->aggs->by_agent->aggs->total_inbound->aggs->talk_time->sum->field = "duration";

// Filtered Results
$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);

// format aggregations for view
for($i=0; $i<sizeof($results->aggregations->by_agent->buckets); ++$i)
{
	$objAgg = $results->aggregations->by_agent->buckets[$i];

	// new row with extension_uuid
	$arrData[$i] = new stdClass();
	$arrData[$i]->cc_agent = $objAgg->key;
	$arrData[$i]->caller_id_name = explode("@", $objAgg->key)[0];

	// totals
	$arrData[$i]->total->calls 					= $objAgg->doc_count ? $objAgg->doc_count : 0;
	$arrData[$i]->total->talk_time 				= $objAgg->total_talk_time->value;

	// total inbound
	$arrData[$i]->total_inbound->calls 			= $objAgg->total_inbound->doc_count ? $objAgg->total_inbound->doc_count : 0;
	$arrData[$i]->total_inbound->talk_time 		= $objAgg->total_inbound->talk_time->value;

	// total outbound
	$arrData[$i]->total_outbound->calls 		= $objAgg->total_outbound->doc_count ? $objAgg->total_outbound->doc_count : 0;
	$arrData[$i]->total_outbound->talk_time 	= $objAgg->total_outbound->talk_time->value;

	// loop for missed and busy calls
	$arrData[$i]->no_answer 	= 0;
	$arrData[$i]->rejected 		= 0;

	foreach($objAgg->dispositions->buckets as $objDisposition)
	{
		if($objDisposition->key == "no_answer") $arrData[$i]->no_answer = $objDisposition->doc_count;
		if($objDisposition->key == "user_busy") $arrData[$i]->rejected = $objDisposition->doc_count;
	}
}

// Total Results In Date Range
$objTotal = new stdClass();
$objTotal->query->bool->must[]->bool->should[] = $arrShouldDomains;

$objTotal->query->bool->must[]->range->start_epoch = [
	"gte" => strtotime($arrParams['date_range']['start_date']),
	"lte" => strtotime($arrParams['date_range']['end_date'])
];
$objTotal = ElasticSearch::query(ES_INDEX."/cdr", $objTotal, '_count');

/**
 * Build Paging Object
 */
$objPaging = new Paging($results->hits->total, $arrParams['page']['from'], $arrParams['page']['size']);
$objPaging->setPagination();

// Build Table Stats
$intBoxTitleCount 		= number_format($results->hits->total);
$intBoxTitleTotalCount 	= number_format($objTotal->count);
$intBoxTitlePercentage 	= round((($results->hits->total / $objTotal->count)*100), 2);

$objFilter = new Filter([
	'blnDates',
	'blnAgents'
]);

$arrPageTitle = [
	'Reporting'	=> '',
	'Agent Statistics' => '/report_agent_statistics.php'
];

asort($arrCCAgents);

$smarty->assign("strTableStats", "{$intBoxTitleCount} records of {$intBoxTitleTotalCount} ({$intBoxTitlePercentage}%)");
$smarty->assign('arrCCAgents', $arrCCAgents);
$smarty->assign('arrData', $arrData);
$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->assign('blnFilter',true);
$smarty->assign('arrParams', $arrParams);
$smarty->display('report-agent-statistics.html');
