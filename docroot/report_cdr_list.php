<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'database/elasticsearch.class.php';
require_once CLASS_DIR . 'filter/filter.class.php';
require_once CLASS_DIR . 'location/location.class.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'paging/paging.class.php';
require_once CLASS_DIR . 'util/fusion_cdr_format.class.php';
require_once CLASS_DIR . 'util/util.class.php';

Navigation::set('reporting','call_detail_records');

/**
 * Params
 */
if($_POST)
{
	$objParams->setParams([
		'date_range' => [
 			'start_date'     	=> timezone::convert_to_server_date($_POST['start_date'] . ' 00:00:00', Company::getTimeZone()),
 			'end_date'       	=> timezone::convert_to_server_date($_POST['end_date'] . ' 23:59:59', Company::getTimeZone()),
            'user_timezone'  	=> Company::getTimeZone()
 		],
		'page' => [
			'size'				=> $_POST['size'],
			'from'				=> $_POST['page']
		],
		'sort' => [
			'field'				=> $_POST['field'],
			'order'				=> $_POST['order']
		],
		'location'				=> '',
		'phone'	=> [
			'type'				=> '',
			'number'			=> '',
		]
	]);

	if($_POST['blnDirection'] || $_POST['blnNumber'])
	{
		$objParams->setParams([
			'phone'		=> [
				'type'   => $_POST['phone_type'],
				'number' => $_POST['phone_number']
			]
		]);
	}

	if($_POST['blnLocations']) 	$objParams->setParams(['locations' 	=> isset($_POST['locations']) ? $_POST['locations'] : ""]);
}

// get page parameters
$arrParams = $objParams->getAllParams();

/**
 * ElasticSearch Query
 */
$objQuery = new stdClass();
// Result Size
$objQuery->size = $arrParams['page']['size'];

// Paging
$objQuery->from = ($arrParams['page']['from'] - 1) * $arrParams['page']['size'];

// Sorting
$objQuery->sort->{$arrParams['sort']['field']}->order = $arrParams['sort']['order'];

/**
 * Date Range Filter
 */
$objQuery->query->bool->must[]->range->start_epoch = [
	"gte" => strtotime($arrParams['date_range']['start_date']),
	"lte" => strtotime($arrParams['date_range']['end_date'])
];

/**
 * Phone Number Filter
 * (source OR destination) = (phone number)
 */
if($arrParams['phone']['number'])
{
	if(strlen(preg_replace('![^0-9]!','',$arrParams['phone']['number'])) == 10)
	{
		$objQuery->query->bool->should[]->wildcard->destination_number = '*' . preg_replace('![^0-9]!','',$arrParams['phone']['number']);
		$objQuery->query->bool->should[]->wildcard->caller_id_number   = '*' . preg_replace('![^0-9]!','',$arrParams['phone']['number']);
		$objQuery->query->bool->minimum_should_match = 1;
	}
	else
	{
		$objQuery->query->bool->should[]->term->destination_number = preg_replace('![^0-9]!','',$arrParams['phone']['number']);
		$objQuery->query->bool->should[]->term->caller_id_number   = preg_replace('![^0-9]!','',$arrParams['phone']['number']);
		$objQuery->query->bool->minimum_should_match = 1;
	}
}

/**
 * Direction Filter
 * (Inbound/Outbound)
 */
if($arrParams['phone']['type'])
{
	$objQuery->query->bool->must[]->match->direction = $arrParams['phone']['type'];
}

/**
 * Location UUID Filter
 */
if(!empty($arrParams['locations']))
{
	for($i=0; $i<sizeof($arrParams['locations']); ++$i)
	{
		$arrShouldLocations[]->match->location_uuid = $arrParams['locations'][$i];
	}
	$objQuery->query->bool->must[]->bool->should[] = $arrShouldLocations;
}

/**
 * Account Code Filter
 */
if(!empty($arrParams['accountcodes']))
{
	for($i=0; $i<sizeof($arrParams['accountcodes']); ++$i)
	{
		$arrShouldAccountCodes[]->match->accountcode = $arrParams['accountcodes'][$i];
	}
	$objQuery->query->bool->must[]->bool->should[] = $arrShouldAccountCodes;
}

/**
 * Extensuion UUID Filter
 */
if(!empty($arrParams['extensions']))
{
	for($i=0; $i<sizeof($arrParams['extensions']); ++$i)
	{
		$arrShouldExtensions[]->match->extension_uuid = $arrParams['extensions'][$i];
	}
	$objQuery->query->bool->must[]->bool->should[] = $arrShouldExtensions;
}

// Filtered Results
$results = ElasticSearch::query(ES_INDEX."/cdr", $objQuery);

// Total Results In Date Range
$objTotal = new stdClass();
$objTotal->query->bool->must[]->range->start_epoch = [
	"gte" => strtotime($arrParams['date_range']['start_date']),
	"lte" => strtotime($arrParams['date_range']['end_date'])
];
$objTotal = ElasticSearch::query(ES_INDEX."/cdr", $objTotal, '_count');




// Format Results For CDR View
for($i=0; $i<sizeof($results->hits->hits); ++$i)
{
	$objCDR = new FusionCDRFormat($results->hits->hits[$i]->_source);
	$arrData[$i] = $objCDR->objCDRFormatted;
}

/**
 * Build Paging Object
 */
$objPaging = new Paging($results->hits->total, $arrParams['page']['from'], $arrParams['page']['size']);
$objPaging->setPagination();

// Build Table Stats
$intBoxTitleCount 		= number_format($results->hits->total);
$intBoxTitleTotalCount 	= number_format($objTotal->count);
$intBoxTitlePercentage 	= round((($results->hits->total / $objTotal->count)*100), 2);

$objFilter = new Filter([
	'blnDates',
	'blnLocations',
	'blnNumbers'
]);

$arrPageTitle = [
	'Reporting'	=> '',
	'CDR List' => '/report_cdr_list.php'
];

$smarty->assign('arrLocations',Location::getAll());
$smarty->assign("strTableStats", "Showing {$intBoxTitleCount} records of {$intBoxTitleTotalCount} ({$intBoxTitlePercentage}%)");
$smarty->assign('arrData', $arrData);
$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->assign('blnFilter',true);
$smarty->assign('arrParams', $arrParams);
$smarty->display('report-cdr-list.html');
