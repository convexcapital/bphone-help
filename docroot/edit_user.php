<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';

Navigation::set('users');

if($_POST)
{
    try
    {
        $_GET['id'] = User::save($_GET['id'], $_POST);
        $smarty->assign('strSuccess', "User ({$_POST['email']}) updated successfully!");
        User::getAll(Company::getID(), 'arrUsers');
        die(header('location: edit_user.php?id='.$_GET['id']));
    }
    catch(Exception $e)
    {
        $smarty->assign('strError', $e->getMessage());
    }
}

User::getOne($_GET['id'], 'objUser');

if($_GET['id']) $strID = '?id=' . $_GET['id'];

$arrPageTitle = [
	'Manage Users' => '/manage_users.php',
	'Edit User'	   => '/edit_user.php' . $strID
];

$objUser = new User();
$smarty->assign('arrPBXUsers', $objUser->getPBXUsersAssoc());

$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->assign('arrSave',['blnSave' => true,'save_name' => 'Save User', 'blnDelete' => true, 'delete_name' => 'Delete User']);
$smarty->display('edit-user.html');
