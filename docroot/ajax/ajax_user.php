<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';

if($_GET['action'] = 'delete')
{
	$intUserID = $_POST['user_id'];

	if($objUser->delete($intUserID))
	{
		$arrSuccess = ["status" => true];
		return json_encode($arrSuccess);
	}
	else {
		$arrError = ["status" => false, "error" => "error"];
		return json_encode($arrError);
	}
}
