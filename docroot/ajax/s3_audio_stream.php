<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'rds.database.inc.php';
require_once CLASS_DIR . 'aws/s3.class.php';

$objUser = new User();
$objUser->isLoggedIn();

global $objRDSConnRead;

$strRecordingURL = $objRDSConnRead->get(
	"v_xml_cdr",
	"recording_file",
	[
		"AND" => [
			"uuid" => $_GET['uuid'],
			"domain_uuid" => $objParams->arrParams['domains']
		]
	]
);

$strRecordingURL = str_replace("archive/", "", $strRecordingURL);

$objS3 = new BphoneS3();
$objS3->streamAudio($strRecordingURL);
