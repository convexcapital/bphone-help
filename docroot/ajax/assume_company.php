<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . "company/company.class.php";
require_once CLASS_DIR . "tag/tag.class.php";

$objUser = new User();

if($_POST['id'] && Tag::exists('super', User::getTags()))
{
    $_SESSION['company'] = Company::getOne($_POST['id']);
}

// reset params to the new company
$_POST['resetParams'] = true;
require_once CLASS_DIR . 'params/params.class.php';
$objParams = new Params();
