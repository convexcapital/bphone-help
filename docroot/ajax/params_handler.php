<?php
// reset objParams in session so a clean object is created
$_POST['resetParams'] = true;
if( $_POST['temp_param'] || $_POST['add_param'] ) $_POST['resetParams'] = false;

// instantiate objParams - happens in file_structure.inc.php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';

if ( $_POST['temp_param'] )
{
	$objSessionParams = unserialize($_SESSION['objParams']);

	// loop through post and apply params if allowed
	foreach($_POST as $key => $val)
	{
		// check if param is allowed
		if( ! array_key_exists($key, $objParams->arrParams) ) continue;

		// create a hash of the value to verify later if value has changed
		$objSessionParams->arrTempParams['id'][$key] = md5(serialize($val));

		$objSessionParams->arrTempParams[$key] = $val;
	}

	// set param in session so it gets created next page load
	$_SESSION['objParams'] = serialize($objSessionParams);
}
else
{
	$objParams->setParams($_POST);
}
