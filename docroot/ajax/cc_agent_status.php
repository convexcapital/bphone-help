<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once CLASS_DIR . 'fusion/fusion.class.php';
require_once INCLUDES_DIR . 'fusion.database.inc.php';

// new handler
$objHandler = new Handler();

// get agent
$objHandler->getAgent($_POST['cc_agent_uuid']);

// make sure we have an agent
if(empty($objHandler->agent_uuid)) $objHandler->respond("No agent found!");

// try to toggle the agent status  -- Available <--> Logged Out
if(!$objHandler->toggleAgentStatus()) $objHandler->respond("Agent Status Toggle Failed!");

// success
$objHandler->respond("Success", true);


/*
 * Handler for this script
 */
class Handler
{
	public function __contruct()
	{
		$this->agent_uuid = "";
		$this->agent_name = "";
		$this->agent_status = "";
	}

	public function getAgent($strID="")
	{
		if(empty($strID)) return;

		global $objFusionConnRead;
		$arrAgent = $objFusionConnRead->get(
			"v_call_center_agents(cca)",
			[
				"[>]v_domains(d)" => "domain_uuid"
			],
			[
				"cca.call_center_agent_uuid",
				"cca.agent_name",
				"d.domain_name",
				"cca.agent_status"
			],
			[
				"call_center_agent_uuid" => $strID
			]
		);

		if(empty($arrAgent['agent_name']) || empty($arrAgent['domain_name'])) return;

		$this->agent_uuid 	= $arrAgent['call_center_agent_uuid'];
		$this->agent_name  	= $arrAgent['agent_name']."@".$arrAgent['domain_name'];
		$this->agent_status = $arrAgent['agent_status'];
	}

	public function toggleAgentStatus()
	{
		switch ($this->agent_status) {
		    case "Available":
		        $this->agent_status = "Logged Out";
		        break;
		    case "Logged Out":
		        $this->agent_status = "Available";
		        break;
			default:
				$this->agent_status = "Logged Out";
		}

		// setup connection
		$strServer = Fusion::getServers()[rand(0,sizeof(Fusion::getServers()) - 1)]['server'];
		$fp = Fusion::getConnection(['event_socket_ip_address'=>$strServer]);

		// make sure we have a connection
		if(!$fp) return false;

		// send api call
		$response = event_socket_request($fp, "api callcenter_config agent set status {$this->agent_name} '{$this->agent_status}'");

		// check response
		if(strpos($response, "ERR")) return false;

		global $objFusionConn;
		$objFusionConn->update(
			"v_call_center_agents",
			["agent_status" => $this->agent_status],
			["call_center_agent_uuid" => $this->agent_uuid]
		);
		return true;
	}

	public function respond($strMessage="", $blnSuccess=false)
	{
		die(json_encode([
			'success' => $blnSuccess,
			'message' => $strMessage,
			'agent_status' => $this->agent_status
		]));
	}
}
