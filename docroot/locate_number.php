<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR.'number/number.class.php';

Navigation::set('admin', 'locate_number');

require_once INCLUDES_DIR . 'auth.inc.php';

if($_GET['number'])
{
	$objNumber = new Number();
	$objResult = $objNumber->lookupNumberAPI(preg_replace('![^0-9]!', '', $_GET['number']));
	$smarty->assign('objNumber', $objResult);
}

$arrPageTitle = [
	'Locate Number' => '/locate_number.php'
];
$smarty->assign('arrPageTitle',$arrPageTitle);

$smarty->assign('arrSave',['blnSave' => true,'save_name' => 'Locate Number']);

$smarty->display('locate-number.html');
