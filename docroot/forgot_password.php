<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';

require_once CLASS_DIR . 'authuser/authuser.class.php';

if($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'http') die(header("location: https://{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}"));
$smarty->assign('strHeaderMsg', 'Request password reset.');

try{
	if( ! $_POST ) throw new Exception('');

	$AuthUser = new AuthUser($_POST['username']);
	$objAgent = $AuthUser->checkIfExists();

	if( ! $objAgent->user_id ) 			throw new Exception('agent not found.');
	if( ! $AuthUser->sendPasswordRequest()) throw new Exception('password request failed.');

	$smarty->assign('strMessage', 'Please check your email for a password reset request');
}
catch(Exception $e)
{
	$strMessage  = "Please enter your username/email";
	$strMessage .= "<br/>".$e->getMessage();
	$smarty->assign('strMessage', $strMessage);
}

$smarty->assign('signed_out', "false");
$smarty->display('forgot_password.html');
