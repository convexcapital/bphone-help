<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';

Navigation::set('users');

User::getAll(Company::getID(), 'arrUsers');

$arrPageTitle = [
	'Manage Users' => '/manage_users.php'
];

$smarty->assign('arrPageTitle',$arrPageTitle);

$smarty->display('manage-users.html');
