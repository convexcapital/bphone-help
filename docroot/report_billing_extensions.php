<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once INCLUDES_DIR . 'fusion.database.inc.php';

Navigation::set('admin','report_billing_extensions');

global $objFusionConnRead;

$arrData = $objFusionConnRead->select(
	"v_extensions(e)",
	["[>]v_domains(d)" => "domain_uuid"],
	[
		"d.domain_name",
		"d.domain_description",
		"e.extension",
		"e.effective_caller_id_name",
		"e.outbound_caller_id_name",
		"e.outbound_caller_id_number",
		"e.call_group"
	],
	[
		"ORDER" => "domain_name"
	]
);

$smarty->assign('arrPageTitle', [
	'Reporting'	=> '',
	'Billing - Extensions' => '/report_billing_extensions.php'
]);

$smarty->assign('arrData', $arrData);

$smarty->display('report-billing-extensions.html');
