<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once CLASS_DIR . 'event_socket/event_socket.class.php';
require_once CLASS_DIR . 'fusion/fusion.class.php';
require_once CLASS_DIR . 'util/util.class.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';

//https://iqventures.fpbx.esip.io/app/call_center_active/call_center_active_inc.php?queue_name=Test&id=123

if($_GET['id'] != '4340729038ef432caad2fe92d47edeae') die('unauthorized');

//get the queue_name and set it as a variable
$strQueueName = $_GET['queue_name'];
$arrQueueName = explode('@', $strQueueName);
$strQueue = $arrQueueName[0];
$strDomain = $arrQueueName[1];

// create an event socket connection
$strServer = Fusion::getServers()[rand(0,sizeof(Fusion::getServers()) - 1)]['server'];
$fp = event_socket_create($strServer, '8021', '2ET9bwc5CuKN9M');

if(!$fp) die('Connection to Event Socket failed.');

$smarty->assign('strNow', timezone::convert_to_user_date(date('Y-m-d H:i:s'), $_GET['time_zone'], 'm/d/y h:i A'));
$smarty->assign('strQueue', $strQueue);
$smarty->assign('strDomain', $strDomain);
$smarty->assign('arrQueueStatus', $arrQueueStatus);
$smarty->assign('arrAgents', $arrAgents);
$smarty->assign('arrCallData', $arrCallData);

$smarty->display('call_center_hud/index.html');
