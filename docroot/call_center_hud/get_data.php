	<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'freeswitch.database.inc.php';
require_once INCLUDES_DIR . 'memcache.inc.php';
require_once CLASS_DIR . 'event_socket/event_socket.class.php';
require_once CLASS_DIR . 'fusion/fusion.class.php';
require_once CLASS_DIR . 'util/util.class.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'report/report.class.php';
require_once CLASS_DIR . 'company/company.class.php';
require_once CLASS_DIR . 'domain/domain.class.php';


if($_GET['id'] != '4340729038ef432caad2fe92d47edeae')
{
	die('unauthorized');
}

//get the queue_name and set it as a variable
$strQueueName = $_GET['queue_name'];
$arrQueueName = explode('@', $strQueueName);
$strQueue = $arrQueueName[0];
$strDomain = $arrQueueName[1];

//convert the string to a named array
function str_to_named_array($tmp_str, $tmp_delimiter) {
	$tmp_array = explode ("\n", $tmp_str);
	$result = '';
	if (trim(strtoupper($tmp_array[0])) != "+OK") {
		$tmp_field_name_array = explode ($tmp_delimiter, $tmp_array[0]);
		$x = 0;
		if (isset($tmp_array)) foreach ($tmp_array as $row) {
			if ($x > 0) {
				$tmp_field_value_array = explode ($tmp_delimiter, $tmp_array[$x]);
				$y = 0;
				if (isset($tmp_field_value_array)) foreach ($tmp_field_value_array as $tmp_value) {
					$tmp_name = $tmp_field_name_array[$y];
					if (trim(strtoupper($tmp_value)) != "+OK") {
						$result[$x][$tmp_name] = $tmp_value;
					}
					$y++;
				}
			}
			$x++;
		}
		unset($row);
	}
	return $result;
}

//create an event socket connection
$strServer = Fusion::getServers()[rand(0,sizeof(Fusion::getServers()) - 1)]['server'];
$fp = Fusion::getConnection(['event_socket_ip_address' => $strServer]);

if(!$fp)
{
	die('Connection to Event Socket failed.');
}else{

	#default queue statuses
	$arrQueueStatus['waiting'] = 0;
	$arrQueueStatus['trying'] = 0;
	$arrQueueStatus['answered_now'] = 0;
	$arrQueueStatus['answered_total'] = 0;
	$arrQueueStatus['abandoned'] = 0;
	$arrQueueStatus['talk_time'] = 0;
	$arrQueueStatus['inbound'] = 0;

	//send the event socket command and get the response
	//callcenter_config queue list tiers [queue_name] |
	$switch_cmd = 'callcenter_config queue list tiers '.$strQueueName;
	$strKey = md5($switch_cmd);
	$arrAgents = $objMem->get($strKey);
	asort($arrAgents);

	if(empty($arrAgents))
	{
		$event_socket_str = trim(event_socket_request($fp, 'api '.$switch_cmd));
		$arrAgents = str_to_named_array($event_socket_str, '|');
		$arrAgents = array_values($arrAgents);
		$objMem->set($strKey, $arrAgents, null, 3);
	}

	//send the event socket command and get the response
	//callcenter_config queue list agents [queue_name] [status] |
	$switch_cmd = 'callcenter_config queue list agents '.$strQueueName;
	$strKey = md5($switch_cmd);
	$arrAgentsData = $objMem->get($strKey);
	if(empty($arrAgentsData))
	{
		$event_socket_str = trim(event_socket_request($fp, 'api '.$switch_cmd));
		$arrAgentsData = str_to_named_array($event_socket_str, '|');
		$arrAgentsData = array_values($arrAgentsData);
		$objMem->set($strKey, $arrAgentsData, null, 3);
	}

	for($i = 0; $i < sizeof($arrAgents); ++$i)
	{
		$arrAgent = $arrAgents[$i];
		$arrAgent['tier_state'] = $arrAgent['state'];

		for($x = 0; $x < sizeof($arrAgentsData); ++$x)
		{
			$arrAgentData = $arrAgentsData[$x];

			if ($arrAgent['agent'] == $arrAgentData['name'])
			{
				$arrAgentData['name'] = str_replace("@{$strDomain}", '',$arrAgentData['name']);
				$arrAgentData['domain'] = $strDomain;

				$last_offered_call_seconds = time() - $arrAgentData['last_offered_call'];
				$last_offered_call_length_hour = floor($last_offered_call_seconds/3600);
				$last_offered_call_length_min = floor($last_offered_call_seconds/60 - ($last_offered_call_length_hour * 60));
				$last_offered_call_length_sec = $last_offered_call_seconds - (($last_offered_call_length_hour * 3600) + ($last_offered_call_length_min * 60));
				$last_offered_call_length_min = sprintf("%02d", $last_offered_call_length_min);
				$last_offered_call_length_sec = sprintf("%02d", $last_offered_call_length_sec);
				$arrAgentData['last_offered_call_length'] = $last_offered_call_length_hour.':'.$last_offered_call_length_min.':'.$last_offered_call_length_sec;

				$last_status_change_seconds = time() - $arrAgentData['last_status_change'];
				$last_status_change_length_hour = floor($last_status_change_seconds/3600);
				$last_status_change_length_min = floor($last_status_change_seconds/60 - ($last_status_change_length_hour * 60));
				$last_status_change_length_sec = $last_status_change_seconds - (($last_status_change_length_hour * 3600) + ($last_status_change_length_min * 60));
				$last_status_change_length_min = sprintf("%02d", $last_status_change_length_min);
				$last_status_change_length_sec = sprintf("%02d", $last_status_change_length_sec);
				$arrAgentData['last_status_change_length'] = $last_status_change_length_hour.':'.$last_status_change_length_min.':'.$last_status_change_length_sec;

				$arrAgents[$i] = array_merge($arrAgent, $arrAgentData);

				// if the agent status is logged out, do not count the calls
				if($arrAgents[$i]['status'] != "Logged Out") $arrQueueStatus['answered_total'] += $arrAgents[$i]['calls_answered'];

				// getting total_talktime from report::getTotalsHUD() function from ES
				// $arrQueueStatus['talk_time'] += $arrAgents[$i]['talk_time'];
			}
		}
	}

	$arrQueueStatus['talk_time'] = util::seconds2human($arrQueueStatus['talk_time']);

	//get the queue list
	//send the event socket command and get the response
	//callcenter_config queue list members [queue_name]
	$switch_cmd = 'callcenter_config queue list members '.$strQueueName;
	$strKey = md5($switch_cmd);
	$arrCallData = $objMem->get($strKey);
	if(empty($arrCallData))
	{
		$event_socket_str = trim(event_socket_request($fp, 'api '.$switch_cmd));
		$arrCallData = str_to_named_array($event_socket_str, '|');
		$arrCallData = array_values($arrCallData);
		$objMem->set($strKey, $arrCallData, null, 3);
	}

	for($i = 0; $i < sizeof($arrCallData); ++$i)
	{
		$arrRow = $arrCallData[$i];
		$arrQueueStatus['waiting'] 		+= ($arrRow['state'] == "Waiting") 		? 1 : 0;
		$arrQueueStatus['waiting'] 		+= ($arrRow['state'] == "Trying") 		? 1 : 0;
		$arrQueueStatus['answered_now'] += ($arrRow['state'] == "Answered") 	? 1 : 0;
		$arrQueueStatus['inbound']		+= ($arrRow['state'] != "Abandoned") 	? 1 : 0;
	}

	for($i = 0; $i < sizeof($arrCallData); ++$i)
	{
		$arrCallRow = $arrCallData[$i];

		$intEpoch = $arrCallRow['rejoined_epoch'] ? $arrCallRow['rejoined_epoch'] : $arrCallRow['joined_epoch'];

		$joined_seconds = time() - $intEpoch;
		$joined_length_hour = floor($joined_seconds/3600);
		$joined_length_min = floor($joined_seconds/60 - ($joined_length_hour * 60));
		$joined_length_sec = $joined_seconds - (($joined_length_hour * 3600) + ($joined_length_min * 60));
		$joined_length_min = sprintf("%02d", $joined_length_min);
		$joined_length_sec = sprintf("%02d", $joined_length_sec);
		$arrCallData[$i]['joined_length'] = $joined_length_hour.':'.$joined_length_min.':'.$joined_length_sec;

		if($arrCallRow['state'] == 'Trying' || $arrCallRow['state'] == 'Waiting')
		{
			if(empty($arrQueueStatus['first_waiting'])){
				$arrQueueStatus['first_waiting'] = util::seconds2human($joined_seconds);
				continue;
			}

			if(empty($arrQueueStatus['second_waiting'])){
				$arrQueueStatus['second_waiting'] = util::seconds2human($joined_seconds);
				continue;
			}

			if(empty($arrQueueStatus['third_waiting'])){
				$arrQueueStatus['third_waiting'] = util::seconds2human($joined_seconds);
				continue;
			}
		}
	}

	if(empty($arrQueueStatus['first_waiting'])){
		$arrQueueStatus['first_waiting'] = 'n/a';
	}

	if(empty($arrQueueStatus['second_waiting'])){
		$arrQueueStatus['second_waiting'] = 'n/a';
	}

	if(empty($arrQueueStatus['third_waiting'])){
		$arrQueueStatus['third_waiting'] = 'n/a';
	}

	/**
	 * Gets
	 * @var Params
	 */
	global $objFreeswitchConnRead;

	$objParams = new Params();

	$objCompany = new Company();

	$objDomain = new Domain();

	// get domain UUID

	$objParams->setParams([
		'domains'	=>  [$objDomain->getUUIDFromFusion($strDomain)]
	]);

	$objReport = new Report($objParams);
	$arrAgentsNew = $objReport->getCCAgents();

	foreach($arrAgentsNew as $arrAgent)
	{
		if($arrAgent['extension']) $arrAgentList[] = $arrAgent['extension'];
	}

	$arrOutbound = $objFreeswitchConnRead->select(
			"agents(a)",
		[
			"[>]channels(c)" 	=> "uuid"
		],
			"*",
		[
			"a.status[!]" 		=> "Logged Out",
			"a.state[!]" 		=> "Waiting",
			"c.direction[=]" 	=> "outbound",
			"c.callee_num"		=> $arrAgentList
		]
	);

	$strStartDate = strtotime(date('Y-m-d 00:00:00'));
	$strEndDate   = strtotime(date('Y-m-d 23:59:59'));

	// get abandoned here
	$arrAbandoned = $objFreeswitchConnRead->select(
			"members",
			"*",
			[
				"state[=]" 	=> "Abandoned",
				"queue[=]"	=> $strQueueName,
				"abandoned_epoch[<>]" => [$strStartDate,$strEndDate]
			]
	);

	$arrQueueStatus['abandoned'] = sizeof($arrAbandoned);

	$arrResponse['now'] 			= timezone::convert_to_user_date(date('Y-m-d H:i:s'), $_GET['time_zone'], 'm/d/y h:i A');
	$arrResponse['queue_status'] 	= $arrQueueStatus;
	$arrResponse['agents'] 			= $arrAgents;
	$arrResponse['queue_status']['outbound']	= sizeof($arrOutbound);
	$arrResponse['totals'] = $objReport->getTotalsHUD($strQueueName);

	echo json_encode($arrResponse);



}
