<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR.'location/location.class.php';
require_once CLASS_DIR.'ticket/ticket.class.php';
require_once CLASS_DIR.'util/util.class.php';

Navigation::set('tickets');

if($_POST)
{
    try
    {
        $_GET['id'] = Ticket::save($_GET['id'], $_POST);

		$objOSTUser = (object) $objOSTConnRead->get( 'ticket', [ "[>]user" => ["ticket.user_id" => "id"] ], '*', [ 'ticket.number' =>  $_GET['id'] ] );

		if($objOSTUser->org_id == 0)
		{
			// update organization id
			$objOSTConn->update( 'user', ['org_id' => Ticket::getCompanyID()],  ["id" => $objOSTUser->user_id]);
		}

        $smarty->assign('strSuccess', "Ticket (<a href='/edit_ticket.php?id={$_GET['id']}'>{$_GET['id']}</a>) updated successfully!");
        Ticket::getAll('', 'arrTickets');
        die(header('location: edit_ticket.php?id='.$_GET['id']));
    }

    catch(Exception $e)
    {
        $smarty->assign('strError', $e->getMessage());
    }
}

Location::getAll('', 'arrLocations');
Ticket::getOne($_GET['id'], 'objTicket');
Ticket::getEntriesAndEvents($_GET['id'], 'arrComments');
Ticket::getAllHelpTopics('arrHelpTopics');
Ticket::getAllStatuses('arrStatuses');

if($_GET['id']) $strID = '?id=' . $_GET['id'];

$arrPageTitle = [
	'Manage Tickets' => '/manage_tickets.php',
	'Edit Ticket'	 => '/edit_ticket.php' . $strID
];

$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->assign('arrSave',['blnSave' => true, 'save_name' => 'Submit Ticket', 'blnDelete' => false, 'delete_name' => 'Delete Ticket']);
$smarty->display('edit-ticket.html');
