<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'database/elasticsearch.class.php';
require_once CLASS_DIR . 'filter/filter.class.php';
require_once CLASS_DIR . 'location/location.class.php';
require_once CLASS_DIR . 'timezone/timezone.class.php';
require_once CLASS_DIR . 'paging/paging.class.php';
require_once CLASS_DIR . 'report/report.class.php';
require_once CLASS_DIR . 'util/fusion_cdr_format.class.php';
require_once CLASS_DIR . 'util/util.class.php';

Navigation::set('admin','report_billing');

/**
 * Params
 */
if($_POST)
{
	$objParams->setParams([
		'date_range' => [
 			'start_date'     	=> timezone::convert_to_server_date($_POST['start_date'] . ' 00:00:00', Company::getTimeZone()),
 			'end_date'       	=> timezone::convert_to_server_date($_POST['end_date'] . ' 23:59:59', Company::getTimeZone()),
            'user_timezone'  	=> Company::getTimeZone()
 		],
		'page' => [
			'size'				=> $_POST['size'],
			'from'				=> $_POST['page']
		],
		'sort' => [
			'field'				=> $_POST['field'],
			'order'				=> $_POST['order']
		],
		'phone' => [
			'type'				=> $_POST['phone_type'],
			'number'			=> $_POST['phone_number']
		],
		'location'				=> '',
	]);

	if($_POST['blnLocations']) $objParams->setParams(['locations' => isset($_POST['locations']) ? $_POST['locations'] : ""]);
}

// get page parameters
$arrParams = $objParams->getAllParams();

$objReport = new Report();

$objFilter = new Filter([
	'blnDates',
	'blnLocations'
]);

$arrPageTitle = [
	'Reporting'	=> '',
	'Billing' => '/report_billing.php'
];

$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->assign('arrLocations',Location::getAll());
$smarty->assign('arrLocationsSet',$objParams->arrParams['locations']);
$smarty->assign('arrData', $objReport->billing($arrParams));
$smarty->assign('arrParams', $arrParams);
$smarty->assign('blnFilter',true);
$smarty->display('report-billing.html');
