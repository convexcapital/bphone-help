<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/../owr/includes/file_structure.inc.php';
require_once INCLUDES_DIR . 'auth.inc.php';
require_once CLASS_DIR . 'navigation/navigation.class.php';
require_once CLASS_DIR . 'number/number.class.php';
require_once CLASS_DIR . 'country/country.class.php';

Navigation::set('numbers');

require_once INCLUDES_DIR . 'auth.inc.php';
$objNumber = new Number($_GET['id']);

if($_GET['action'] == 'validate')
{
	echo $objNumber->isItADuplicate($_POST['number']) ? 'true' : 'false';
	die();
}

if($_POST)
{
	$objNumber->loadFromPost($_POST);
    $objNumber->save();
	die(header('location: ./manage_numbers.php'));
}

$smarty->assign('arrNumber', $objNumber->getOne());

$objCountry = new Country();
$smarty->assign('arrCountries', $objCountry->getAllAssoc());
$smarty->assign('strCountries', json_encode($objCountry->getISOJson()));
if($_GET['id']) $strID = '?id=' . $_GET['id'];

$arrPageTitle = [
	'Manage Numbers' 	=> '/manage_numbers.php',
	'Edit Number'		=> '/edit_number.php' . $strID
];

$smarty->assign('arrSave',['blnSave' => true,'save_name' => 'Save Number', 'blnDelete' => false, 'delete_name' => 'Delete Number']);

$smarty->assign('arrPageTitle',$arrPageTitle);
$smarty->display('edit-number.html');
